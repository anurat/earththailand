<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
	
		$cms_image = $this->cms_model->get_cms_image();
		$data['slideshow_list'] = $cms_image->get_images( 'slideshow', 1 );
		
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$data['thai_link_list'] = $cms_homepage->get_homepages( 'thai_link' );
		$data['regional_link_list'] = $cms_homepage->get_homepages( 'regional_link' );
		$data['global_link_list'] = $cms_homepage->get_homepages( 'global_link' );
		$data['news_slideshow_list'] = $cms_homepage->get_homepages( 'news_slideshow' );
		$data['document_list'] = $cms_homepage->get_homepages( 'document' );
		$data['article_list'] = $cms_homepage->get_homepages( 'article' );

		$data['page_count'] = file_get_contents( APPPATH .'views/homepage/page_count.txt' );

		$data['title'] = 'EARTH Thailand';
		$data['content'] = 'homepage';
		$this->load->view( 'template', $data );
	}
	
	/**
	 * Show pollutions page
	 *
	 */
	public function pollutions() {
	
		$cms_pollution = $this->cms_model->get_cms_pollution();
		$data['pollution_list'] = $cms_pollution->get_pollution_list( 'enabled', 0, 100000 );
	
		$data['title'] = 'Pollution Hotspots';
		$data['content'] = 'pollutions';
		$this->load->view( 'template', $data );
	}

	/**
	 * Show article list page
	 *
	 * @param int $index
	 */
	public function articles( $index=0 ) {
	
		// set the number of items per page
		$no = 10;
	
		$cms_article = $this->cms_model->get_cms_article();
		$data['article_list'] = $cms_article->get_article_list( 'enabled', $index, $no );
		$count = $cms_article->count_article_list( 'enabled' );

		$this->load->library('pagination');
		$config['base_url'] = site_url( "/articles" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['num_links'] = 4;
		$config['uri_segment'] = 2;
		$config['first_link'] = '< First';
		$config['last_link'] = 'Last >';
		$this->pagination->initialize( $config );
		$data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $data['article_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);

		$data['title'] = 'News';
		$data['content'] = 'articles';
		$this->load->view( 'template', $data );
	}

	/**
	 * Show document list page
	 *
	 * @param int $index
	 */
	public function documents( $index = 0 ) {
	
		// set the number of items per page
		$no = 10;
	
		$cms_document = $this->cms_model->get_cms_document();
		$data['document_list'] = $cms_document->get_document_list( 'enabled', $index, $no );
		$count = $cms_document->count_document_list( 'enabled' );

		$this->load->library('pagination');
		$config['base_url'] = site_url( "/documents" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['num_links'] = 4;
		$config['uri_segment'] = 2;
		$config['first_link'] = '< First';
		$config['last_link'] = 'Last >';
		$this->pagination->initialize( $config );
		$data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $data['document_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);

		$data['title'] = 'Publications';
		$data['content'] = 'documents';
		$this->load->view( 'template', $data );
	}

	/**
	 * Show gallery list page
	 *
	 * @param int $index
	 */
	public function gallery_list( $index = 0 ) {
	
		// set the number of items per page
		$no = 10;
	
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$data['gallery_list'] = $cms_gallery->get_gallery_list( 'enabled', $index, $no );
		$count = $cms_gallery->count_gallery_list( 'enabled' );

		$this->load->library('pagination');
		$config['base_url'] = site_url( "/gallery_list" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['num_links'] = 4;
		$config['uri_segment'] = 2;
		$config['first_link'] = '< First';
		$config['last_link'] = 'Last >';
		$this->pagination->initialize( $config );
		$data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $data['gallery_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
	
		$data['title'] = 'Gallery';
		$data['content'] = 'galleries';
		$this->load->view( 'template', $data );
	}

	/**
	 * Show multimedia list page
	 *
	 * @param int $index
	 */
	public function multimedia_list( $index=0 ) {
	
		// set the number of items per page
		$no = 10;
	
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$data['multimedia_list'] = $cms_multimedia->get_multimedia_list( 'enabled', $index, $no );
		$count = $cms_multimedia->count_multimedia_list( 'enabled' );

		$this->load->library('pagination');
		$config['base_url'] = site_url( "/multimedia_list" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['num_links'] = 4;
		$config['uri_segment'] = 2;
		$config['first_link'] = '< Firt';
		$config['last_link'] = 'Last >';
		$this->pagination->initialize( $config );
		$data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $data['multimedia_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
	
		$data['title'] = 'Multimedia';
		$data['content'] = 'multimedia_list';
		$this->load->view( 'template', $data );
	}
	
	/**
	 * Show calendar page
	 *
	 */
	public function events( $year=null, $month=null ) {
		
		$data['year'] = ( empty( $year ) )? date('Y'): $year;
		$data['month'] = ( empty( $month ) )? date('n'): $month;
		$start_date = mktime( 0, 0, 0, $data['month'], 1, $data['year'] );
		$end_date = mktime( 0, 0, 0, $data['month'] +1, 1, $data['year'] ) -1;
		
		
		$cms_calendar = $this->cms_model->get_cms_calendar();
		$data['events'] = $cms_calendar->get_events( $start_date, $end_date );
		
		$data['title'] = 'Calendar';
		$data['content'] = 'events';
		$this->load->view( 'template', $data );
	}

	/**
	 * Show tag page
	 *
	 */
	public function tags() {
	
		$cms_tag = $this->cms_model->get_cms_tag();
		
		$search = $this->input->post( 'search' );
		if( !empty( $search ) ) {
			$tags = $cms_tag->search( $search );
		}
		else {
			$tags = $cms_tag->get_tag_list();
		}
		
		$data['tags'] = $tags;
	
		$data['title'] = 'tags';
		$data['content'] = 'tags';
		$this->load->view( 'template', $data );
	}
	
	/**
	 * Show search page
	 *
	 */
	public function search() {
	
		$search_submit = $this->input->post( 'search_submit' );
		$data['text-search'] = $this->input->post( 'text-search' );
		$data['search'] = $this->input->post( 'search' );
		$data['article_check'] = $this->input->post( 'article_check' );
		$data['document_check'] = $this->input->post( 'document_check' );
		$data['gallery_check'] = $this->input->post( 'gallery_check' );
		$data['multimedia_check'] = $this->input->post( 'multimedia_check' );
		$data['tag_check'] = $this->input->post( 'tag_check' );
		
		// search from the menu, not from the search form in the page
		if( empty( $search_submit ) ) {
			$data['article_check'] = 'on';
			$data['document_check'] = 'on';
			$data['gallery_check'] = 'on';
			$data['multimedia_check'] = 'on';
			$data['tag_check'] = 'on';
		}
		
		// set search value
		if( empty( $data['search'] ) ) {
			$data['search'] = $data['text-search'];
		}
		
		// search
		/*
		$cms_search = $this->cms_model->get_cms_search()
		$cms_search->search( $data['article_check'], $data['document_check'], $data['gallery_check'],
				$data['multimedia_check'], $data['tag_check'] );
		*/
		if( !empty( $data['search'] ) ) {
			if( $data['article_check'] == 'on' ) {
				$cms_article = $this->cms_model->get_cms_article();
				$data['articles'] = $cms_article->search( $data['search'] );
			}
			if( $data['document_check'] == 'on' ) {
				$cms_document = $this->cms_model->get_cms_document();
				$data['documents'] = $cms_document->search( $data['search'] );
			}
			if( $data['gallery_check'] == 'on' ) {
				$cms_gallery = $this->cms_model->get_cms_gallery();
				$data['galleries'] = $cms_gallery->search( $data['search'] );
			}
			if( $data['multimedia_check'] == 'on' ) {
				$cms_multimedia = $this->cms_model->get_cms_multimedia();
				$data['multimedia'] = $cms_multimedia->search( $data['search'] );
			}
			if( $data['tag_check'] == 'on' ) {
				$cms_tag = $this->cms_model->get_cms_tag();
				$data['tags'] = $cms_tag->search( $data['search'] );
			}
		}
		
		
		$data['title'] = 'Search';
		$data['content'] = 'search';
		$this->load->view( 'template', $data );
	}
	
	/**
	 * Cont the total number of pages and saved in views/homepage/page_count.txt
	 *
	 */
	public function page_count() {

		$this->load->library( 'piwik' );
		$actions = $this->piwik->actions( 'range', '2016-04-01,today' );
		file_put_contents( APPPATH .'views/homepage/page_count.txt', $actions['value'] );
	}
	
	public function transfer() {
	
		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_article = $this->cms_model->get_cms_article();
		$cms_document = $this->cms_model->get_cms_document();
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$cms_pollution = $this->cms_model->get_cms_pollution();
		$cms_calendar = $this->cms_model->get_cms_calendar();
		
		$article_list = $cms_article->get_article_list( 'all' );
		foreach( $article_list as $article ) {
			$item_list = $cms_item_list->add_item_list( 'article', $article->get_article_id() );
			$item_list->set_order( $article->get_order() );
			$item_list->save();
		}
		
		$document_list = $cms_document->get_document_list( 'all' );
		foreach( $document_list as $document ) {
			$item_list = $cms_item_list->add_item_list( 'document', $document->get_document_id() );
			$item_list->set_order( $document->get_order() );
			$item_list->save();
		}

		$gallery_list = $cms_gallery->get_gallery_list( 'all' );
		foreach( $gallery_list as $gallery ) {
			$item_list = $cms_item_list->add_item_list( 'gallery', $gallery->get_gallery_id() );
			$item_list->set_order( $gallery->get_order() );
			$item_list->save();
		}

		$multimedia_list = $cms_multimedia->get_multimedia_list( 'all' );
		foreach( $multimedia_list as $multimedia ) {
			$item_list = $cms_item_list->add_item_list( 'multimedia', $multimedia->get_multimedia_id() );
			$item_list->set_order( $multimedia->get_order() );
			$item_list->save();
		}

		$pollution_list = $cms_pollution->get_pollution_list( 'all' );
		foreach( $pollution_list as $pollution ) {
			$item_list = $cms_item_list->add_item_list( 'pollution', $pollution->get_pollution_id() );
			$item_list->set_order( $pollution->get_order() );
			$item_list->save();
		}

		$calendar_list = $cms_calendar->get_calendar_list( 'all' );
		foreach( $calendar_list as $calendar ) {
			$item_list = $cms_item_list->add_item_list( 'calendar', $calendar->get_calendar_id() );
			$item_list->set_order( $calendar->get_order() );
			$item_list->save();
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */