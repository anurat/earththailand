<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Show a page
	 *
	 * @param string $page_type
	 * @param string $id
	 *
	 */
	public function show( $page_type, $id ) {
	
		$data['content'] = $page_type;
		$data['id'] = $id;
		$this->load->view( 'template', $data );
	}
	
	/**
	 * Show a category page
	 *
	 * @param int $id
	 */
	public function category( $id ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		
		$data['category'] = $cms_category->get_category( $id );
		$data['category_path'] = $cms_category->get_category_path( $id );
		$data['images'] = $cms_category->get_images( $id );
		$data['cat_links'] = $cms_category->get_cat_links_by_category( $id );
		
		$data['title'] = $data['category']->get_title();
		$data['content'] = 'category';
	
		$this->load->view( 'template', $data );
	}
    
    /**
     * Show a pollution type page
     *
     */
    public function pollution_type() {
        
		$data['title'] = 'ข้อมูลกรณีพื้นที่มลพิษ';
		$data['content'] = 'pollution_type';
	
		$this->load->view( 'template', $data );
    }

    /**
     * Show a pollution list page
     *
     * @param int $type
     * @param int $index
     */
    public function pollution_list( $type, $index=0 ) {
        
		// set the number of items per page
		$no = 10;
	
        $cms_pollution = $this->cms_model->get_cms_pollution();
        $data['pollutions'] = $cms_pollution->get_pollutions_by_type( $type, $index, $no );
		$count = $cms_pollution->count_pollutions_by_type( $type );
       
		$this->load->library('pagination');
		$config['base_url'] = site_url( "/pollution_list/{$type}" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['num_links'] = 5;
		$config['uri_segment'] = 3;
		$config['first_link'] = '< หน้าแรกสุด';
		$config['last_link'] = 'หน้าสุดท้าย >';
		$this->pagination->initialize( $config );
		$data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $data['pollutions'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);

        $data['title'] = 'ข้อมูลกรณีพื้นที่มลพิษ';
        $data['content'] = 'pollution_list';

        $this->load->view( 'template', $data );
    }

	/**
	 * Show an article
	 *
	 * @param int $id
	 */
	public function article( $id ) {
	
		$cms_article = $this->cms_model->get_cms_article();
		$cms_image = $this->cms_model->get_cms_image();
		
		$data['article'] = $cms_article->get_article( $id );
		$data['images'] = $cms_image->get_images( 'article', $id );
		$data['title'] = $data['article']->get_title();
		$data['content'] = 'article';
	
		$this->load->view( 'template', $data );
	}

	/**
	 * Show a document page
	 *
	 * @param int $id
	 */
	public function document( $id ) {
	
		$cms_document = $this->cms_model->get_cms_document();
		$cms_image = $this->cms_model->get_cms_image();
		
		$data['document'] = $cms_document->get_document( $id );
		$data['images'] = $cms_image->get_images( 'document', $id );
		$data['title'] = $data['document']->get_title();
		$data['content'] = 'document';
	
		$this->load->view( 'template', $data );
	}

	/**
	 * Show a gallery page
	 *
	 * @param int $id
	 */
	public function gallery( $id ) {
	
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$cms_image = $this->cms_model->get_cms_image();

		$data['gallery'] = $cms_gallery->get_gallery( $id );
		$data['images'] = $cms_image->get_images( 'gallery', $id );
		$data['title'] = $data['gallery']->get_title();
		$data['content'] = 'gallery';
	
		$this->load->view( 'template', $data );
	}

	/**
	 * Show a multimedia page
	 *
	 * @param int $id
	 */
	public function multimedia( $id ) {
	
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		
		$data['multimedia'] = $cms_multimedia->get_multimedia( $id );
		$data['title'] = $data['multimedia']->get_title();
		$data['content'] = 'multimedia';
	
		$this->load->view( 'template', $data );
	}

	/**
	 * Show a tag page
	 *
	 * @param int $id
	 */
	public function tag( $id ) {
	
		$cms_tag = $this->cms_model->get_cms_tag();
		
		$data['tag'] = $cms_tag->get_tag( $id );
		$data['title'] = $data['tag']->get_name();
		$data['tag_links'] = $cms_tag->get_tag_links( $id );
		$data['content'] = 'tag';
	
		$this->load->view( 'template', $data );
	}

	/**
	 * Show a pollution page
	 *
	 * @param int $id
	 */
	public function pollution( $id ) {
	
		$cms_pollution = $this->cms_model->get_cms_pollution();
		
		$data['pollution'] = $cms_pollution->get_pollution( $id );
		$data['title'] = $data['pollution']->get_title();
		$data['content'] = 'pollution';
	
		$this->load->view( 'template', $data );
	}

	/**
	 * Show a calendar page
	 *
	 * @param int $id
	 */
	public function calendar( $id ) {
	
		$cms_calendar = $this->cms_model->get_cms_calendar();
		
		$data['calendar'] = $cms_calendar->get_calendar( $id );
		$data['title'] = $data['calendar']->get_title();
		$data['content'] = 'calendar_page';
	
		$this->load->view( 'template', $data );
	}

}

/* End of file page.php */
/* Location: ./application/controllers/page.php */