<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin2 extends CI_Controller {

	private $data;

	public function __construct() {
		parent::__construct();
		
		$cms_signin = $this->cms_model->get_cms_signin();
		$this->data = $cms_signin->get_session_data();
		if( empty( $this->data['language'] ) ) {
			$this->data['language'] = $this->config->item('language');
		}
		$this->lang->load( 'menu', $this->data['language'] );
		$this->lang->load( 'header', $this->data['language'] );
	}
	
	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
	
		if( empty( $this->data['userid'] ) ) {
			redirect( 'admin/signin' );
		}
		
		redirect( 'admin/dashboard' );
	}
	
}

/* End of file admin2.php */
/* Location: ./application/controllers/admin2.php */