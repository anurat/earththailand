<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH .'controllers/admin/admin_ci.php';

class Homepage2 extends Admin_CI {

	public function __construct() {
		parent::__construct();
		
	}
	
	/**
	 * Show header image page
	 *
	 */
	public function header_image() {	
		$this->data['content'] = 'admin/homepage/header_image';
		$this->data['title'] = $this->lang->line( 'menu_header_image', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show link page
	 *
	 */
	public function link() {

		$cms_homepage = $this->cms_model->get_cms_homepage();
		$this->data['thai_link_list'] = $cms_homepage->get_homepages( 'thai_link' );
		$this->data['regional_link_list'] = $cms_homepage->get_homepages( 'regional_link' );
		$this->data['global_link_list'] = $cms_homepage->get_homepages( 'global_link' );

		$this->data['content'] = 'admin/homepage/link';
		$this->data['title'] = $this->lang->line( 'menu_link', $this->data['language'] );
		$this->lang->load( 'link', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Add link
	 *
	 * @param string $type
	 */
	public function add_link( $type ) {
		
		$this->link_page( $type );
		
		$link_name = $this->input->post( 'link_name' );
		if( !empty( $link_name ) ) {
			redirect( "admin/homepage/link" );
		}
	}
	
	/**
	 * Update link
	 *
	 * @param string $type
	 * @param int $homepage_id
	 *
	 */
	public function update_link( $type, $homepage_id ) {
	
		$this->link_page( $type, $homepage_id );
		
		$link_name = $this->input->post( 'link_name' );
		if( !empty( $link_name ) ) {
			redirect( "admin/homepage/link" );
		}
	}
	
	/**
	 * Remove link
	 *
	 * @param int $homepage_id
	 */
	public function remove_link( $homepage_id ) {
	
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$cms_homepage->delete_homepage( $homepage_id );
		redirect( "admin/homepage/link" );
	}
	
	/**
	 * Show slideshow page
	 *
	 */
	public function slideshow() {
		$this->data['content'] = 'admin/homepage/slideshow';
		$this->data['title'] = $this->lang->line( 'menu_slideshow', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Show news slideshow page
	 *
	 */
	public function news_slideshow() {
	
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$this->data['homepage_list'] = $cms_homepage->get_homepages( 'news_slideshow' );

		$this->data['content'] = 'admin/homepage/news_slideshow';
		$this->data['title'] = $this->lang->line( 'menu_news_slideshow', $this->data['language'] );
		$this->lang->load( 'news_slideshow', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Add news slideshow
	 *
	 *
	 */
	public function add_news_slideshow() {
	
		$cms_category = $this->cms_model->get_cms_category();
		$cms_homepage = $this->cms_model->get_cms_homepage();
		
		$content_type = $this->input->post('content_type');
		if( !empty( $content_type ) ) {
			$content_title = $this->input->post('content_title');
			
			$table = '';
			$table_id = 0;
			$obj = null;
			switch( $content_type ) {
			case Cms_category::MULTIMEDIA_CAT: 
				$table = 'multimedia'; 
				$cms_multimedia = $this->cms_model->get_cms_multimedia();
				$obj = $cms_multimedia->get_multimedia_by_title( $content_title );
				$table_id = $obj->get_multimedia_id();
				break;
			case Cms_category::GALLERY_CAT: 
				$table = 'gallery';
				$cms_gallery = $this->cms_model->get_cms_gallery();
				$obj = $cms_gallery->get_gallery_by_title( $content_title );
				$table_id = $obj->get_gallery_id();
				break;
			case Cms_category::DOCUMENT_CAT: 
				$table = 'document';
				$cms_document = $this->cms_model->get_cms_document();
				$obj = $cms_document->get_document_by_title( $content_title );
				$table_id = $obj->get_document_id();
				break;
			case Cms_category::ARTICLE_CAT: 
				$table = 'article';
				$cms_article = $this->cms_model->get_cms_article();
				$obj = $cms_article->get_article_by_title( $content_title );
				$table_id = $obj->get_article_id();
				break;
			}
			
			if( !empty( $obj ) ) {
				$cms_homepage->add_homepage( 'news_slideshow', $table_id, 
						$obj->get_list_title(), $obj->get_list_desc(), $obj->get_list_img(), 
						$table, "/{$table}/{$table_id}" );
				redirect( "admin/homepage/news_slideshow" );
			}
			else {
				$this->data['error'] = 'Unable to find content.';
			}
		}
	
		$this->data['cat_list'] = $cms_category->get_main_categories();
	
		$this->data['content'] = 'admin/homepage/add_news_slideshow';
		$this->data['title'] = $this->lang->line( 'menu_add_news_slideshow', $this->data['language'] );
		$this->lang->load( 'news_slideshow', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Update news slideshow
	 *
	 * @param int $homepage_id
	 *
	 */
	public function update_news_slideshow( $homepage_id ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		$cms_homepage = $this->cms_model->get_cms_homepage();
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$description = $this->input->post( 'editor1' );
			$image = $this->input->post( 'news_slideshow_img' );
			$ns = $cms_homepage->get_homepage( $homepage_id );
			$cms_homepage->update_homepage( $homepage_id, $ns->get_table(), $ns->get_table_id(),
					$title, $description, $image, $ns->get_link_name(), $ns->get_link_url() );
			redirect( "admin/homepage/news_slideshow" );
		}
		
		
		$this->data['cat_list'] = $cms_category->get_main_categories();
		$this->data['news_slideshow'] = $cms_homepage->get_homepage( $homepage_id );
	
		$this->data['content'] = 'admin/homepage/update_news_slideshow';
		$this->data['title'] = $this->lang->line( 'menu_update_news_slideshow', $this->data['language'] )
			.'-' .$this->data['news_slideshow']->get_title();
		$this->lang->load( 'article', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Remove news slideshow
	 *
	 * @param int $homepage_id
	 */
	public function remove_news_slideshow( $homepage_id ) {
	
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$cms_homepage->delete_homepage( $homepage_id );
		redirect( "admin/homepage/news_slideshow" );
	}
	
	/**
	 * Show documents page
	 *
	 */
	public function documents() {
		
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$this->data['homepage_list'] = $cms_homepage->get_homepages( 'document' );
		
		$this->data['content'] = 'admin/homepage/documents';
		$this->data['title'] = $this->lang->line( 'menu_documents', $this->data['language'] );
		$this->lang->load( 'document', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Update a document item in homepage
	 *
	 * @param int $document_id
	 *
	 */
	public function update_document( $document_id ) {

		$this->update_homepage( 'document', $document_id );
		
		$title = $this->input->post( 'title' );
		if( !empty( $title ) ) {
			redirect( 'admin/homepage/documents' );
		}
	}
	
	/**
	 * Remove a document from homepage
	 *
	 * @param int $homepage_id
	 *
	 */
	public function remove_document( $homepage_id ) {

		$cms_homepage = $this->cms_model->get_cms_homepage();
		$cms_homepage->delete_homepage( $homepage_id );
		
		redirect( "admin/homepage/documents" );
	}

	/**
	 * Show articles page
	 *
	 */
	public function articles() {
	
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$this->data['homepage_list'] = $cms_homepage->get_homepages( 'article' );
	
		$this->data['content'] = 'admin/homepage/articles';
		$this->data['title'] = $this->lang->line( 'menu_articles', $this->data['language'] );
		$this->lang->load( 'article', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Update an article item in homepage
	 *
	 * @param int $article_id
	 *
	 */
	public function update_article( $article_id ) {

		$this->update_homepage( 'article', $article_id );
		
		$title = $this->input->post( 'title' );
		if( !empty( $title ) ) {
			redirect( 'admin/homepage/articles' );
		}
	}
	
	/**
	 * Remove an article from homepage
	 *
	 * @param int $homepage_id
	 *
	 */
	public function remove_article( $homepage_id ) {

		$cms_homepage = $this->cms_model->get_cms_homepage();
		$cms_homepage->delete_homepage( $homepage_id );
		
		redirect( "admin/homepage/articles" );
	}

	/**
	 * Show tags page
	 *
	 * @param string $query
	 */
	public function tags( $query=null ) {
	
		$search = urldecode( $query );
		$cms_tag = $this->cms_model->get_cms_tag();
		if( !empty( $query ) ) {
			$tags = $cms_tag->search( $search );
		}
		else {
			$tags = $cms_tag->get_tag_list();
		}
		
		$this->data['search'] = $search;
		$this->data['tags'] = $tags;
	
		$this->data['content'] = 'admin/homepage/tags';
		$this->data['title'] = $this->lang->line( 'menu_tags', $this->data['language'] );
		$this->lang->load( 'tag', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show tag detail
	 *
	 * @param int $tag_id
	 *
	 */
	public function tag( $tag_id ) {
	
		$cms_tag = $this->cms_model->get_cms_tag();
		$this->data['tag'] = $cms_tag->get_tag( $tag_id );
		$this->data['tag_links'] = $cms_tag->get_tag_links( $tag_id );
	
		$this->data['content'] = 'admin/homepage/tag';
		$this->data['title'] = $this->lang->line( 'menu_tag_detail', $this->data['language'] )
			.'-' .$this->data['tag']->get_name();
		$this->lang->load( 'tag', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Remove tag
	 *
	 * @param int $tag_id
	 *
	 */
	public function remove_tag( $tag_id ) {
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tag( $tag_id );
		
		redirect( "/admin/homepage/tags" );
	}

	/**
	 * Remove tag link
	 *
	 * @param int $tag_link_id
	 *
	 */
	public function remove_tag_link( $tag_link_id ) {
		$cms_tag = $this->cms_model->get_cms_tag();
		$tag_id = $cms_tag->get_tag_link( $tag_link_id )->get_tag_id();
		$cms_tag->delete_tag_link( $tag_link_id );
		
		redirect( "/admin/homepage/tag/" .$tag_id );
	}

	/**
	 * Show footer page
	 *
	 */
	public function footer() {
	
		$filepath = APPPATH .'views/footer.php';
	
		$editor1 = $this->input->post( 'editor1' );
		if( !empty( $editor1 ) ) {
			$no = file_put_contents( $filepath, $editor1 );
			if( $no ) {
				$this->data['result'] = 'Save file successfully.';
			}
			else {
				$this->data['result'] = 'Error saving file.';
			}
		}
	
		$this->data['footer'] = file_get_contents( $filepath );
	
		$this->data['content'] = 'admin/homepage/footer';
		$this->data['title'] = $this->lang->line( 'menu_footer', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

}

/* End of file homepage.php */
/* Location: ./application/controllers/admin/homepage.php */