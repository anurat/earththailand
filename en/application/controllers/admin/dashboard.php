<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $data;

	public function __construct() {
		parent::__construct();

		$cms_signin = $this->cms_model->get_cms_signin();
		$this->data = $cms_signin->get_session_data();
		if( empty( $this->data['language'] ) ) {
			$this->data['language'] = $this->config->item('language');
		}
		$this->lang->load( 'menu', $this->data['language'] );
		$this->lang->load( 'header', $this->data['language'] );
		
		if( empty( $this->data['userid'] ) ) {
			redirect( '/admin/signin' );
		}
	}
	
	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
	
		$cms_article = $this->cms_model->get_cms_article();
		$cms_document = $this->cms_model->get_cms_document();
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_pollution = $this->cms_model->get_cms_pollution();
		$cms_calendar = $this->cms_model->get_cms_calendar();
		
		$this->data['article_no'] = $cms_article->get_article_no();
		$this->data['document_no'] = $cms_document->get_document_no();
		$this->data['gallery_no'] = $cms_gallery->get_gallery_no();
		$this->data['multimedia_no'] = $cms_multimedia->get_multimedia_no();
		$this->data['pollution_no'] = $cms_pollution->get_pollution_no();
		$this->data['tag_no'] = $cms_tag->get_tag_no();
		$this->data['calendar_no'] = $cms_calendar->get_calendar_no();
		
		$this->data['content'] = 'admin/dashboard';
		$this->lang->load( 'dashboard', $this->data['language'] );
		$this->data['title'] = $this->lang->line('dashboard_dashboard');
		$this->load->view( 'admin_template', $this->data );
	}
	
}

/* End of file dashboard.php */
/* Location: ./application/controllers/admin/dashboard.php */