<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin extends CI_Controller {

	private $data;

	public function __construct() {
		parent::__construct();
		
		$cms_signin = $this->cms_model->get_cms_signin();
		$this->data = $cms_signin->get_session_data();
		if( empty( $this->data['language'] ) ) {
			$this->data['language'] = $this->config->item('language');
		}
		$this->lang->load( 'menu', $this->data['language'] );
		$this->lang->load( 'header', $this->data['language'] );
	}
	
	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {
	
		$email = $this->input->post( 'email' );
		if( !empty( $email ) ) {
		
			$password = $this->input->post( 'password' );
			$cms_signin = $this->cms_model->get_cms_signin();
			$user = $cms_signin->validate( $email, $password );
			
			if( $user ) {
				$cms_signin->signin( $user );
				redirect( 'admin/dashboard' );
			}
			
			$this->data['error'] = 'Incorrect e-mail address or password.';
			$this->data['email'] = $email;
		}
	
		$this->data['content'] = 'admin/signin';
		$this->lang->load( 'profiles', $this->data['language'] );
		$this->data['title'] = $this->lang->line('menu_signin');
		$this->load->view( 'admin_template', $this->data );
	}
	
	public function signout() {
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $this->data['userid'] ) ) {
			$cms_signin->signout();
		}
		
		redirect( 'admin/signin' );
	}
}

/* End of file signin.php */
/* Location: ./application/controllers/admin/signin.php */