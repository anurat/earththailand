<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH .'controllers/admin/admin_ci.php';

class System extends Admin_CI {

	public function __construct() {
		parent::__construct();
		
	}
	
	/**
	 * Show statistics page
	 *
	 */
	public function statistics() {
	
		$this->data['content'] = 'admin/system/statistics';
		$this->data['title'] = $this->lang->line( 'menu_statistics', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show profiles page
	 *
	 */
	public function profiles() {
	
		$cms_user = $this->cms_model->get_cms_user();
		$user = $cms_user->get_user( $this->data['userid'] );
		
		$name = $this->input->post( 'name' );
		if( !empty( $name ) ) {
			$email = $this->input->post( 'email' );
			
			$cms_user->update_user( $user->get_user_id(), $name, $email, $user->get_role() );
		}
	
		$this->data['user'] = $user;
		$this->data['content'] = 'admin/system/profiles';
		$this->data['title'] = $this->lang->line( 'menu_profiles', $this->data['language'] );
		$this->lang->load( 'profiles', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show user manager
	 *
	 *
	 */
	public function users() {
	
		$cms_user = $this->cms_model->get_cms_user();
		$this->data['user_list'] = $cms_user->get_user_list();
		
		$this->data['content'] = 'admin/system/users';
		$this->data['title'] = $this->lang->line( 'menu_user', $this->data['language'] );
		$this->lang->load( 'user', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Add user
	 *
	 *
	 */
	public function add_user() {

		$this->user();
		
		$name = $this->input->post('name');
		if( !empty( $name ) ) {
			redirect( 'admin/system/users' );
		}
	}

	/**
	 * Update user
	 *
	 * @param int $user_id
	 *
	 */
	public function update_user( $user_id ) {

		$this->user( $user_id );
		
		$name = $this->input->post('name');
		if( !empty( $name ) ) {
			redirect( 'admin/system/users' );
		}
	}

	/**
	 * Show change password page
	 *
	 */
	public function change_password() {
		
		$cms_signin = $this->cms_model->get_cms_signin();
		$cms_user = $this->cms_model->get_cms_user();
		
		$old_password = $this->input->post( 'old_password' );
		if( !empty( $old_password ) ) {
		
			$user = $cms_signin->validate( $this->data['email_address'], $old_password );
			if( $user ) {
				$new_password = $this->input->post( 'new_password' );
				$cms_user->update_password( $this->data['userid'], $new_password );
				redirect( 'admin/system/profiles' );
			}
		}
		
		$this->data['content'] = 'admin/system/change_password';
		$this->data['title'] = $this->lang->line( 'menu_change_password', $this->data['language'] );
		$this->lang->load( 'profiles', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
    /**
     * Reset password
     *
     * @param int $user_id
     *
     */
    public function reset_password( $user_id ) {
		$cms_user = $this->cms_model->get_cms_user();
        $cms_user->reset_password( $user_id );
        
		$this->data['content'] = 'admin/system/reset_password';
		$this->data['title'] = $this->lang->line( 'menu_reset_password', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
    }
}

/* End of file system.php */
/* Location: ./application/controllers/admin/system.php */