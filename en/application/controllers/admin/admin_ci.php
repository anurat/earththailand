<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_CI extends CI_Controller {

	protected $data;

	public function __construct() {
		parent::__construct();
		
		$cms_signin = $this->cms_model->get_cms_signin();
		$this->data = $cms_signin->get_session_data();
		if( empty( $this->data['language'] ) ) {
			$this->data['language'] = $this->config->item('language');
		}
		$this->lang->load( 'menu', $this->data['language'] );
		$this->lang->load( 'header', $this->data['language'] );
		
		if( empty( $this->data['userid'] ) ) {
			redirect( '/admin/signin' );
		}
	}
	
	/**
	 * Manage category list
	 *
	 * @param int $category_id
	 */
	public function categories( $category_id ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		$category = $cms_category->get_category( $category_id );
		$this->data['category'] = $category;
		$this->data['category_id'] = $category_id;
		$this->data['cat_links'] = $cms_category->get_cat_links_by_category( $category_id );
		
		$this->data['content'] = 'admin/categories';
		$this->lang->load( 'category', $this->data['language'] );
		$this->data['title'] = $this->lang->line('menu_update_category') .'-' .$category->get_title();
		$this->load->view( 'admin_template', $this->data );
		
		$this->session->set_flashdata( 'url', current_url() );
	}
	
	/**
	 * Manage category page info
	 *
	 * @param int $category_id
	 */
	public function category( $category_id ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
		
		$category = $cms_category->get_category( $category_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$description = $this->input->post( 'editor2' );
			$tags = $this->input->post( 'tags' );
			
			$category = $cms_category->update_category( $category_id, $title, $description );
			$cms_tag->update_tags( 'category', $category_id, $tags );
			$cms_history->add_history( 'category', $category_id, $this->data['userid'] );
		}
	
		$this->data['category'] = $category;
		$this->data['category_id'] = $category_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'category', $category_id );
		$this->data['category_path'] = $cms_category->get_category_path( $category_id );
	
		$this->data['content'] = 'admin/category';
		$this->data['title'] = $category->get_title();
		$this->lang->load( 'category', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Manage article page
	 *
	 * @param int $article_id
	 */
	public function article( $article_id=null ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		$cms_article = $this->cms_model->get_cms_article();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_item_list = $this->cms_model->get_cms_item_list();
		
		$article = ( empty( $article_id ) )? new Article(): $cms_article->get_article( $article_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$video_code = $this->input->post( 'video_code' );
			$description = $this->input->post( 'editor2' );
			$attachment = $this->input->post( 'editor3' );
			$list_title = $this->input->post( 'list_title' );
			$list_desc = $this->input->post( 'editor1' );
			$list_img = $this->input->post( 'list_img' );
			$article_date = $this->input->post( 'article_date' );
			$tags = $this->input->post( 'tags' );
			$category = $this->input->post( 'category' );
			
			$adate = strtotime( $article_date );

			if( empty( $article_id ) ) {
				$article = $cms_article->add_article( $title, $video_code, $description, $attachment, 
						$list_title, $list_desc, $list_img, $adate );
				$article_id = $article->get_article_id();
				
				$cms_item_list->add_item_list( 'article', $article_id );
			}
			else {
				$cms_article->update_article( $article_id, $title, $video_code, $description,
						$attachment, $list_title, $list_desc, $list_img, $adate );
			}
			
			$cms_category->update_cat_links( 'article', $article_id, $category );			
			$cms_tag->update_tags( 'article', $article_id, $tags );
			$cms_history->add_history( 'article', $article_id, $this->data['userid'] );
			return;
		}
	
		$this->data['article'] = $article;
		$this->data['article_id'] = $article_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'article', $article_id );
		$this->data['category_list'] = $cms_category->get_categories( 'article' );
		$this->data['cat_links'] = $cms_category->get_cat_links( 'article', $article_id );
		$this->data['previous_url'] = $this->session->flashdata('url');
	
		$this->data['content'] = 'admin/article';
		$this->data['title'] = ( empty( $article_id ) )? $this->lang->line( 'menu_add_article', $this->data['language'] ):
				$this->lang->line( 'menu_update_article', $this->data['language'] )
				.'-' .$this->data['article']->get_title();
		$this->lang->load( 'article', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );

		$this->session->keep_flashdata( 'url' );
	}

	/**
	 * Manage document page
	 *
	 * @param int $document_id
	 */
	public function document( $document_id=null ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		$cms_document = $this->cms_model->get_cms_document();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
        $cms_item_list = $this->cms_model->get_cms_item_list();
		
		$document = ( empty( $document_id ) )? new Document(): $cms_document->get_document( $document_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$video_code = $this->input->post( 'video_code' );
			$description = $this->input->post( 'editor2' );
			$list_title = $this->input->post( 'list_title' );
			$list_desc = $this->input->post( 'editor1' );
			$list_img = $this->input->post( 'list_img' );
			$document_date = $this->input->post( 'document_date' );
			$tags = $this->input->post( 'tags' );
			$category = $this->input->post( 'category' );
			
			$ddate = strtotime( $document_date );

			if( empty( $document_id ) ) {
				$document = $cms_document->add_document( $title, $video_code, $description,
						$list_title, $list_desc, $list_img, $ddate );
				$document_id = $document->get_document_id();

				$cms_item_list->add_item_list( 'document', $document_id );
			}
			else {
				$cms_document->update_document( $document_id, $title, $video_code, $description,
						$list_title, $list_desc, $list_img, $ddate );
			}
			
			$cms_category->update_cat_links( 'document', $document_id, $category );			
			$cms_tag->update_tags( 'document', $document_id, $tags );
			$cms_history->add_history( 'document', $document_id, $this->data['userid'] );
			return;
		}
		
		$this->data['document'] = $document;
		$this->data['document_id'] = $document_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'document', $document_id );
		$this->data['category_list'] = $cms_category->get_categories( 'document' );
		$this->data['cat_links'] = $cms_category->get_cat_links( 'document', $document_id );
		$this->data['previous_url'] = $this->session->flashdata('url');
		
		$this->data['content'] = 'admin/document';
		$this->data['title'] = ( empty( $document_id ) )? $this->lang->line( 'menu_add_document', $this->data['language'] ):
				$this->lang->line( 'menu_update_document', $this->data['language'] )
				.'-' .$this->data['document']->get_title();
		$this->lang->load( 'document', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );

		$this->session->keep_flashdata( 'url' );
	}
	
	/**
	 * Update homepage item
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	protected function update_homepage( $table, $table_id ) {
	
		$cms_homepage = $this->cms_model->get_cms_homepage();
		$homepage = $cms_homepage->get_homepage_by_table( $table, $table_id );
		$this->data['homepage'] = $homepage;
		
		$title = $this->input->post( 'title' );
		if( !empty( $title ) ) {
			$image = $this->input->post( 'image_path' );
			$description = $this->input->post( 'editor2' );
			$cms_homepage->update_homepage( $homepage->get_homepage_id(), $table, $table_id, 
					$title, $description, $image,'', '' );
			return;
		}
		
		$this->data['table'] = $table;
		
		$this->data['content'] = 'admin/homepage';
		$this->data['title'] = $this->lang->line( 'menu_update_' .$table, $this->data['language'] )
			.'-' .$homepage->get_title();
		$this->lang->load( 'document', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Link page
	 *
	 * @param string $type
	 * @param int $homepage_id
	 *
	 */
	public function link_page( $type, $homepage_id=null ) {
		$cms_homepage = $this->cms_model->get_cms_homepage();

		$link_name = $this->input->post( 'link_name' );
		if( !empty( $link_name ) ) {
			$link_url = $this->input->post( 'link_url' );
			
			if( empty( $homepage_id ) ) {
				$cms_homepage->add_homepage( "{$type}_link", 1, '', '', '', $link_name, $link_url );
			}
			else {
				$cms_homepage->update_homepage( $homepage_id, "{$type}_link", 1, '', '', '', $link_name, $link_url );
			}
			return;
		}
		
		if( !empty( $homepage_id ) ) {
			$this->data['link'] = $cms_homepage->get_homepage( $homepage_id );
		}
		
		$this->data['content'] = 'admin/link';
		$this->data['title'] = ( empty( $homepage_id ) )? $this->lang->line( 'menu_add_link', $this->data['language'] ):
				$this->lang->line( 'menu_update_link', $this->data['language'] );
		$this->lang->load( 'link', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Manage multimedia page
	 *
	 * @param int $multimedia_id
	 */
	public function multimedia_page( $multimedia_id=null ) {

		$cms_category = $this->cms_model->get_cms_category();
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
        $cms_item_list = $this->cms_model->get_cms_item_list();
		
		$multimedia = ( empty( $multimedia_id ) )? new Multimedia(): $cms_multimedia->get_multimedia( $multimedia_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$description1 = $this->input->post( 'editor1' );
			$video_code = $this->input->post( 'video_code' );
			$description2 = $this->input->post( 'editor2' );
			$tags = $this->input->post( 'tags' );
			$list_title = $this->input->post( 'list_title' );
			$list_desc = $this->input->post( 'editor3' );
			$list_img = $this->input->post( 'list_img' );
			$category = $this->input->post( 'category' );

			if( empty( $multimedia_id ) ) {
				$multimedia = $cms_multimedia->add_multimedia( $title, $description1, $description2, 
						$video_code, $list_title, $list_desc, $list_img );
				$multimedia_id = $multimedia->get_multimedia_id();
			
				$cms_item_list->add_item_list( 'multimedia', $multimedia_id );
			}
			else {
				$multimedia = $cms_multimedia->update_multimedia( $multimedia_id, $title, $description1, 
						$description2, $video_code, $list_title, $list_desc, $list_img );
			}
			$cms_category->update_cat_links( 'multimedia', $multimedia_id, $category );			
			$cms_tag->update_tags( 'multimedia', $multimedia_id, $tags );
			$cms_history->add_history( 'multimedia', $multimedia_id, $this->data['userid'] );
			return;
		}
		
		$this->data['multimedia'] = $multimedia;
		$this->data['multimedia_id'] = $multimedia_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'multimedia', $multimedia_id );
		$this->data['category_list'] = $cms_category->get_categories( 'multimedia' );
		$this->data['cat_links'] = $cms_category->get_cat_links( 'multimedia', $multimedia_id );
		
		$this->data['content'] = 'admin/multimedia';
		$this->data['title'] = ( empty( $multimedia_id ) )? $this->lang->line( 'menu_add_multimedia', $this->data['language'] ):
				$this->lang->line( 'menu_update_multimedia', $this->data['language'] )
				.'-' .$this->data['multimedia']->get_title();
		$this->lang->load( 'multimedia', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );

		$this->session->keep_flashdata( 'url' );
	}
	
	/**
	 * Manage gallery page
	 *
	 * @param int $gallery_id
	 */
	public function gallery_page( $gallery_id=null ) {
	
		$cms_category = $this->cms_model->get_cms_category();
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
        $cms_item_list = $this->cms_model->get_cms_item_list();
		
		$gallery = ( empty( $gallery_id ) )? new Gallery(): $cms_gallery->get_gallery( $gallery_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$video_code = $this->input->post( 'video_code' );
			$description = $this->input->post( 'editor2' );
			$attachment = $this->input->post( 'editor3' );
			$list_title = $this->input->post( 'list_title' );
			$list_desc = $this->input->post( 'editor1' );
			$list_img = $this->input->post( 'list_img' );
			$gallery_date = $this->input->post( 'gallery_date' );
			$tags = $this->input->post( 'tags' );
			$category = $this->input->post( 'category' );
			
			$gdate = strtotime( $gallery_date );

			if( empty( $gallery_id ) ) {
				$gallery = $cms_gallery->add_gallery( $title, $video_code, $description,
						$attachment, $list_title, $list_desc, $list_img, $gdate );
				$gallery_id = $gallery->get_gallery_id();
				
				$cms_item_list->add_item_list( 'gallery', $gallery_id );
			}
			else {
				$cms_gallery->update_gallery( $gallery_id, $title, $video_code, $description,
						$attachment, $list_title, $list_desc, $list_img, $gdate );
			}
			
			$cms_category->update_cat_links( 'gallery', $gallery_id, $category );
			$cms_tag->update_tags( 'gallery', $gallery_id, $tags );
			$cms_history->add_history( 'gallery', $gallery_id, $this->data['userid'] );
			return;
		}
	
		$this->data['gallery'] = $gallery;
		$this->data['gallery_id'] = $gallery_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'gallery', $gallery_id );
		$this->data['category_list'] = $cms_category->get_categories( 'gallery' );
		$this->data['cat_links'] = $cms_category->get_cat_links( 'gallery', $gallery_id );
	
		$this->data['content'] = 'admin/gallery';
		$this->data['title'] = ( empty( $gallery_id ) )? $this->lang->line( 'menu_add_gallery', $this->data['language'] ):
				$this->lang->line( 'menu_update_gallery', $this->data['language'] )
				.'-' .$this->data['gallery']->get_title();
		$this->lang->load( 'gallery', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
		
		$this->session->keep_flashdata( 'url' );
	}
	
	/**
	 * Manage calendar page
	 *
	 * @param int $calendar_id
	 */
	public function calendar_page( $calendar_id=null ) {
	
		$cms_calendar = $this->cms_model->get_cms_calendar();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_item_list = $this->cms_model->get_cms_item_list();
		
		$calendar = ( empty( $calendar_id ) )? new Calendar(): 
		$cms_calendar->get_calendar( $calendar_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$video_code = $this->input->post( 'video_code' );
			$description = $this->input->post( 'editor2' );
			$start_date = $this->input->post( 'start_date' );
			$end_date = $this->input->post( 'end_date' );
			$tags = $this->input->post( 'tags' );
			
			$sdate = strtotime( $start_date );
			$edate = strtotime( $end_date );

			if( empty( $calendar_id ) ) {
				$calendar = $cms_calendar->add_calendar( $title, $video_code, $description,
						$sdate, $edate );
				$calendar_id = $calendar->get_calendar_id();
				
				$cms_item_list->add_item_list( 'calendar', $calendar_id );
			}
			else {
				$cms_calendar->update_calendar( $calendar_id, $title, $video_code, $description,
						$sdate, $edate );
			}
			
			$cms_tag->update_tags( 'calendar', $calendar_id, $tags );
			$cms_history->add_history( 'calendar', $calendar_id, $this->data['userid'] );
			return;
		}
	
		$this->data['calendar'] = $calendar;
		$this->data['calendar_id'] = $calendar_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'calendar', $calendar_id );
	
		$this->data['content'] = 'admin/calendar';
		$this->data['title'] = ( empty( $calendar_id ) )? $this->lang->line( 'menu_add_calendar', $this->data['language'] ):
				$this->lang->line( 'menu_update_calendar', $this->data['language'] )
				.'-' .$this->data['calendar']->get_title();
		$this->lang->load( 'calendar', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
		
		$this->session->keep_flashdata( 'url' );
	}
	
	/**
	 * Manage pollution page
	 *
	 * @param int $pollution_id
	 */
	public function pollution( $pollution_id=null ) {
	
		$cms_pollution = $this->cms_model->get_cms_pollution();
		$cms_history = $this->cms_model->get_cms_history();
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_item_list = $this->cms_model->get_cms_item_list();
		
		$pollution = ( empty( $pollution_id ) )? new Pollution(): $cms_pollution->get_pollution( $pollution_id );
		
		$title = $this->input->post('title');
		if( !empty( $title ) ) {
			$video_code = $this->input->post( 'video_code' );
			$description = $this->input->post( 'editor2' );
			$tags = $this->input->post( 'tags' );
			$list_title = $this->input->post( 'list_title' );
			$list_desc = $this->input->post( 'editor1' );
			$pollution_date = $this->input->post( 'pollution_date' );
			$type = $this->input->post( 'type' );
			$mark_img = $this->input->post( 'mark_img' );
			$x = $this->input->post( 'x' );
			$y = $this->input->post( 'y' );
			
			
			$pdate = strtotime( $pollution_date );

			if( empty( $pollution_id ) ) {
				$pollution = $cms_pollution->add_pollution( $title, $video_code, $description,
						$list_title, $list_desc, $pdate, $type, $mark_img, $x, $y );
				$pollution_id = $pollution->get_pollution_id();
				
				$cms_item_list->add_item_list( 'pollution', $pollution_id );
			}
			else {
				$cms_pollution->update_pollution( $pollution_id, $title, $video_code, $description,
						$list_title, $list_desc, $pdate, $type, $mark_img, $x, $y );
			}
			
			$cms_tag->update_tags( 'pollution', $pollution_id, $tags );
			$cms_history->add_history( 'pollution', $pollution_id, $this->data['userid'] );
			return;
		}
	
		$this->data['pollution'] = $pollution;
		$this->data['pollution_id'] = $pollution_id;
		$this->data['tags'] = $cms_tag->get_tag_str( 'pollution', $pollution_id );
		
		$this->data['content'] = 'admin/pollution';
		$this->data['title'] = ( empty( $pollution_id ) )? $this->lang->line( 'menu_add_pollution', $this->data['language'] ):
				$this->lang->line( 'menu_update_pollution', $this->data['language'] )
				.'-' .$this->data['pollution']->get_title();
		$this->lang->load( 'pollution', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Manage user page
	 *
	 * @param int $user_id
	 */
	public function user( $user_id=null ) {

		$cms_user = $this->cms_model->get_cms_user();
		$user = ( empty( $user_id ) )? new User(): $cms_user->get_user( $user_id );
		
		$name = $this->input->post('name');
		if( !empty( $name ) ) {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$role = $this->input->post('role');
			$enable = $this->input->post('enable');
			$enabled = ( $enable == 'on' )? 1: 0;
			
			if( empty( $user_id ) ) {
				$user = $cms_user->add_user( $name, $email, $password, $role, $enabled );
			}
			else {
				$cms_user->update_user( $user_id, $name, $email, $role, $enabled );
			}
			
			return;
		}
		
		$this->data['user'] = $user;
		$this->data['user_id'] = $user_id;
		
		$this->data['content'] = 'admin/user';
		$this->data['title'] = ( empty( $user_id ) )? $this->lang->line( 'menu_add_user', $this->data['language'] ):
				$this->lang->line( 'menu_update_user', $this->data['language'] )
				.'-' .$this->data['user']->get_name();
		$this->lang->load( 'user', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
}

/* End of file admin_ci.php */
/* Location: ./application/controllers/admin/admin_ci.php */