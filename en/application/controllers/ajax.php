<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	private $data;

	public function __construct() {
		parent::__construct();
		
	}
	
	/**
	 * Enable item
	 *
	 * @param string $table
	 *
	 */
	public function enable( $table ) {
		
        $table_id = $this->input->post( 'table_id' );
        $status = $this->input->post( 'status' );
        
        switch( $table ) {
        case 'user':
            $cms_user = $this->cms_model->get_cms_user();
            if( $status == 'on' ) {
                $cms_user->enable( $table_id );
            }
            else {
                $cms_user->disable( $table_id );
            }
            break;
        default:
            $cms_item_list = $this->cms_model->get_cms_item_list();
            if( $status == 'on' ) {
                $cms_item_list->enable( $table, $table_id );
            }
            else {
                $cms_item_list->disable( $table, $table_id );
            }
        }
	}
	
	/**
	 * Lock call
	 *
	 * @param string $table
	 *
	 */
	public function lock( $table ) {
		$cms_item_list = $this->cms_model->get_cms_item_list();
        $table_id = $this->input->post( 'table_id' );
        $status = $this->input->post( 'status' );
        if( $status == 'on' ) {
            $cms_item_list->lock( $table, $table_id );
        }
        else {
            $cms_item_list->unlock( $table, $table_id );
        }
	}
	
	/**
	 * Operate item number ordering
	 *
	 * @param string $table
	 */
	public function move( $table ) {
        $cms_item_list = $this->cms_model->get_cms_item_list();
        $table_id = $this->input->post( 'table_id' );
        $new_pos = $this->input->post( 'new_pos' );
        $cms_item_list->reorder( $table, $table_id, $new_pos );
	}
    
	/**
	 * Operate multiple items number ordering
	 *
	 * @param string $table
	 */
	public function move_all( $table ) {
        $cms_item_list = $this->cms_model->get_cms_item_list();
        $mm_table_id = $this->input->post( 'mm_table_id' );
        $new_pos = $this->input->post( 'new_pos' );
        $cms_item_list->multi_reorder( $table, $mm_table_id, $new_pos );
	}
    
	/**
	 * delete an item
	 *
	 * @param string $table
	 */
	public function delete( $table ) {
        $table_id = $this->input->post( 'table_id' );
        
        switch( $table ) {
        case 'article':
            $cms_article = $this->cms_model->get_cms_article();
            $cms_article->delete_article( $table_id );
            break;
        }
	}
    
	/**
	 * Homepage call
	 *
	 * @param string $table
	 *
	 */
	public function homepage( $table ) {
	
		$cms_homepage = $this->cms_model->get_cms_homepage();
		switch( $table ) {
		case 'article':
			$cms_article = $this->cms_model->get_cms_article();
			$article_id = $this->input->post( 'table_id' );
			$article = $cms_article->get_article( $article_id );
			$status = $this->input->post( 'status' );
			if( $status == 'on' ) {
				$cms_homepage->add_homepage( 'article', $article_id, $article->get_list_title(),
						$article->get_list_desc(), $article->get_list_img(), '', '' );
			}
			else {
				$cms_homepage->delete_homepage_by_table( 'article', $article_id );
			}
			break;
		case 'document':
			$cms_document = $this->cms_model->get_cms_document();
			$document_id = $this->input->post( 'table_id' );
			$document = $cms_document->get_document( $document_id );
			$status = $this->input->post( 'status' );
			$cms_homepage = $this->cms_model->get_cms_homepage();
			if( $status == 'on' ) {
				$cms_homepage->add_homepage( 'document', $document_id, $document->get_list_title(),
						$document->get_list_desc(), $document->get_list_img(), '', '' );
			}
			else {
				$cms_homepage->delete_homepage_by_table( 'document', $document_id );
			}
			break;
		}
	}
	
	/**
	 * Sortable call
	 *
	 * @param string $table
	 *
	 */
	public function sortable( $table ) {
		
		$cms_item_list = $this->cms_model->get_cms_item_list();
		switch( $table ) {
		case 'article':
		case 'document':
		case 'pollution':
		case 'gallery':
		case 'multimedia':
			$a_table = $this->input->post( 'a_table' );
			$a_order = $this->input->post( 'a_order' );
			$cms_item_list->update_orders( $table, $a_table, $a_order );
			break;
		case 'gallery_photo':
			$photo = $this->input->post( 'photo' );
			$order = $this->input->post( 'order' );
			$cms_photo = $this->cms_model->get_cms_photo();
			$cms_photo->update_orders( $photo, $order );
			break;
		case 'image':
			$image = $this->input->post( 'image' );
			$order = $this->input->post( 'order' );
			$cms_image = $this->cms_model->get_cms_image();
			$cms_image->update_orders( $image, $order );
			break;
		case 'homepage_article':
		case 'homepage_document':
		case 'link':
			$homepage = $this->input->post( 'homepage' );
			$order = $this->input->post( 'order' );
			$cms_homepage = $this->cms_model->get_cms_homepage();
			$cms_homepage->update_orders( $homepage, $order );
			break;
		case 'category':
			$cat_link = $this->input->post( 'cat_link' );
			$order = $this->input->post( 'order' );
			$cms_category = $this->cms_model->get_cms_category();
			$cms_category->update_orders( $cat_link, $order );
			break;
		}
	}
	
	/**
	 * Get search items in each category
	 *
	 * @param int $category_id
	 *
	 */
	public function categories( $category_id ) {
	
		$search = $this->input->get('term');
	
		$cms_category = $this->cms_model->get_cms_category();
		
		$cl = array();
		switch( $category_id ) {
		case Cms_category::MULTIMEDIA_CAT:
			$cms_multimedia = $this->cms_model->get_cms_multimedia();
			$multimedia_list = $cms_multimedia->ajax_search( $search );
			foreach( $multimedia_list as $multimedia ) {
				$cl[$multimedia->get_multimedia_id()] = $multimedia->get_title();
			}
			break;
		case Cms_category::GALLERY_CAT:
			$cms_gallery = $this->cms_model->get_cms_gallery();
			$gallery_list = $cms_gallery->ajax_search( $search );
			foreach( $gallery_list as $gallery ) {
				$cl[$gallery->get_gallery_id()] = $gallery->get_title();
			}
			break;
		case Cms_category::DOCUMENT_CAT:
			$cms_document = $this->cms_model->get_cms_document();
			$document_list = $cms_document->ajax_search( $search );
			foreach( $document_list as $document ) {
				$cl[$document->get_document_id()] = $document->get_title();
			}
			break;
		case Cms_category::ARTICLE_CAT:
			$cms_article = $this->cms_model->get_cms_article();
			$article_list = $cms_article->ajax_search( $search );
			foreach( $article_list as $article ) {
				$cl[$article->get_article_id()] = $article->get_title();
			}
			break;
		}
		
		
		$this->load->view( 'json', array( 'data' => $cl ) );
	}
	
	/**
	 * Update gallery photo and reload
	 *
	 * @param int $gallery_id
	 *
	 * @return Photo array
	 */
	public function gallery_photo( $gallery_id ) {
	
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$cms_gallery->update_from_folder( $gallery_id );
		
		$gallery_photos = $cms_gallery->get_gallery_photos( $gallery_id );
		$data = $gallery_photos->toArray();
		
		$this->load->view( 'json', array( 'data' => $data ) );
	}
	
	/**
	 * Operate photo
	 */
	public function photo( $action ) {
	
		$photo_id = $this->input->post( 'photo_id' );
		$name = $this->input->post( 'name' );
		$description = $this->input->post( 'description' );
		
		$cms_photo = $this->cms_model->get_cms_photo();
		$photo = $cms_photo->update_photo_info( $photo_id, $name, $description );
		$data = $photo->toArray();
		
		$this->load->view( 'json', array( 'data' => $data ) );
	}
	
	/**
	 * Update gallery images and reload
	 *
	 * @param string $type
	 * @param int $id
	 *
	 * @return Photo array
	 */
	public function gallery_image( $type, $id ) {
	
		$cms_image = $this->cms_model->get_cms_image();
		
		switch( $type ) {
		case 'category':
			$cms_category = $this->cms_model->get_cms_category();
			$cms_category->update_from_folder( $id );
			
			$images = $cms_category->get_images( $id );
			$data = $images->toArray();
			break;
		case 'article':
		case 'document':
		case 'gallery':
		case 'slideshow':
			$cms_image->update_from_folder( $type, $id );
			$images = $cms_image->get_images( $type, $id );
			$data = $images->toArray();
			break;
		}
		
		
		$this->load->view( 'json', array( 'data' => $data ) );
	}
	
	/**
	 * Operate image
	 *
	 * @param string $action
	 */
	public function image( $action ) {
	
		$image_id = $this->input->post( 'image_id' );
		$name = $this->input->post( 'name' );
		$description = $this->input->post( 'description' );
		
		$cms_image = $this->cms_model->get_cms_image();
		$image = $cms_image->update_image_info( $image_id, $name, $description );
		$data = $image->toArray();
		
		$this->load->view( 'json', array( 'data' => $data ) );
	}
    
    /**
     * Generate calendar events
     *
     */
    public function calendar() {
        $start = $this->input->get('start');
        $end = $this->input->get('end');
        
        $cms_calendar = $this->cms_model->get_cms_calendar();
        $events = $cms_calendar->get_events( strtotime( $start ), strtotime( $end ) );
        
        $data = array();
        foreach( $events as $event ) {
            $id = $event->get_calendar_id();
            $e = array(
                'id' => $id,
                'title' => mb_substr( $event->get_title(), 0, 10 ),
                'start' => date( 'Y-m-d\TH:i:s', $event->get_start_date() ),
                'end' => date( 'Y-m-d\TH:i:s', $event->get_end_date() ),
                'url' => site_url( "calendar/{$id}" ) 
            );
            $data[] = $e;
        }
        
		$this->load->view( 'json', array( 'data' => $data ) );
    }
}

/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */