<div id="article" >
	<div>
		<h1>News</h1>
		<br />

		<div class="items" >
<?php
foreach( $article_list as $article ) {
	$article_id = $article->get_article_id();
	$list_img = $article->get_list_img();
?>
			<div>
<?php
	if( !empty( $list_img ) ) {
?>
				<a href="<?php echo site_url( "article/" .$article_id ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$list_img ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "article/" .$article_id ); ?>" 
						target="_blank" ><?php echo $article->get_list_title(); ?></a></h4>
				<div class="text" ><?php echo $article->get_list_desc(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "article/" .$article_id ); ?>" 
						target="_blank" >read more...</a></span>
			</div>
<?php
}
?>
		</div>

		<div class="pagination" >
			<?php echo $page['links']; ?>
			<span class="summary" >
				Showing item <?php echo $page['index1']; ?> - <?php echo $page['index2']; ?><br />
				from <?php echo $page['count']; ?> items
			</span>
		</div>

<?php
include "social_media.php";
?>
	</div>
</div>