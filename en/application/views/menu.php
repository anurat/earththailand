<nav class="nav1600">
	<ul>
		<li><a href="<?php echo site_url( '' ); ?>" target="_blank">Home</a></li>
		<li><a href="<?php echo site_url( 'pollutions' ); ?>" target="_blank">Pollution
				Hotspots</a></li>
		<li><a href="<?php echo site_url( 'articles' ); ?>" target="_blank">News</a></li>
		<li><a href="<?php echo site_url( 'documents' ); ?>" target="_blank">Publications</a></li>
		<li><a href="<?php echo site_url( 'gallery_list' ); ?>"
			target="_blank">Gallery</a></li>
		<li><a href="<?php echo site_url( 'events' ); ?>" target="_blank">Calendar</a></li>
		<li><a href="<?php echo site_url( 'about' ); ?>" target="_blank">About
				EARTH</a></li>
		<li><a href="<?php echo site_url( 'tags' ); ?>" target="_blank">tags</a></li>
		<li>
			<form action="<?php echo site_url( 'search' ); ?>" method="post"
				data-ajax="false">
				<input id="text-search" name="text-search" maxlength="20"
					type="text" size="20" placeholder="Search here" />
			</form>
		</li>
	</ul>
</nav>

<nav class="nav1200">
	<ul>
		<li><a href="<?php echo site_url( '' ); ?>" target="_blank">Home</a></li>
		<li><a href="<?php echo site_url( 'pollutions' ); ?>" target="_blank">Pollution
				Hotspots</a></li>
		<li><a href="<?php echo site_url( 'articles' ); ?>" target="_blank">News</a></li>
		<li><a href="<?php echo site_url( 'documents' ); ?>" target="_blank">Publications</a></li>
		<li><a href="<?php echo site_url( 'gallery_list' ); ?>"
			target="_blank">Gallery</a></li>
		<li><a href="<?php echo site_url( 'about' ); ?>" target="_blank">About EARTH</a></li>
		
		<?php include "select_menu.php"; ?>
</nav>

<nav class="nav800" >
	<?php include "select_menu.php"; ?>
</nav>
