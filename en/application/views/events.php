<div id="article" >
	<div>
		<h1>Calendar</h1>
		<br />
		
		<link rel='stylesheet' type='text/css' href="<?php echo site_url( "js/fullcalendar-2.7.3/fullcalendar.css" ); ?>" />
		<script src="<?php echo site_url( "js/jquery-ui-1.11.0.custom/jquery-ui.min.js" ); ?>"></script>
		<script src="<?php echo site_url( "js/fullcalendar-2.7.3/lib/moment.min.js" ); ?>"></script>
		<script src="<?php echo site_url( "js/fullcalendar-2.7.3/fullcalendar.min.js" ); ?>"></script>
		<script type="text/javascript" >
		$( function() {

			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek'
				},
				defaultView: 'month',
				editable: false,
				events: '<?php echo site_url( '/ajax/calendar' ); ?>',
				eventClick: function(event) {
					if(event.url) {
						window.open(event.url, "_blank");
						return false;
					}
				},
				displayEventTime: false
			});

		});
		</script>
		
		<div id="calendar"></div>
		
<?php
include "social_media.php";
?>
		</div>
</div>