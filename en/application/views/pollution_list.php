<div id="article" >
	<div>
		<h1>Pollution Hotspots</h1>
		<br />
		<div class="items" >
<?php
foreach( $pollutions as $pollution ) {
	$pollution_id = $pollution->get_pollution_id();
?>
			<div>
				<h4><a href="<?php echo site_url( "pollution/" .$pollution_id ); ?>" 
						target="_blank" ><?php echo $pollution->get_list_title(); ?></a></h4>
				<div class="text" ><?php echo $pollution->get_list_desc(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "pollution/" .$pollution_id ); ?>" 
						target="_blank" >read more...</a></span>
			</div>
<?php
}
?>
		</div>
		
		<div class="pagination" >
			<?php echo $page['links']; ?>
			<span class="summary" >
				Showing item <?php echo $page['index1']; ?> - <?php echo $page['index2']; ?><br />
				from <?php echo $page['count']; ?> items.
			</span>
		</div>
<?php
include "social_media.php";
?>
	</div>
</div>