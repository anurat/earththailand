<div id="article" >
	<div>
		<h1>Search</h1>
		<br />

		<form method="post" name="search_form" action="<?php echo site_url( "/home/search" ); ?>" data-ajax="false" >
			<input type="text" name="search" value="<?php echo $search_text; ?>" placeholder="Search here" />
			
			<div data-role="controlgroup">
				<input type="checkbox" name="article_check" id="article_check" data-mini="true" 
					<?php echo ( !empty( $article_check) )? "checked=\"checked\"": ""; ?> />
					<label for="article_check" >News</label>
				<input type="checkbox" name="document_check" id="document_check" data-mini="true"
					<?php echo ( !empty( $document_check) )? "checked=\"checked\"": ""; ?> />
						<label for="document_check" >Publications</label>
				<input type="checkbox" name="gallery_check" id="gallery_check" data-mini="true"
					<?php echo ( !empty( $gallery_check) )? "checked=\"checked\"": ""; ?> />
						<label for="gallery_check" >Gallery</label>
				<input type="checkbox" name="multimedia_check" id="multimedia_check" data-mini="true"
					<?php echo ( !empty( $multimedia_check) )? "checked=\"checked\"": ""; ?> />
						<label for="multimedia_check" >Multimedia</label>
				<input type="checkbox" name="tag_check" id="tag_check" data-mini="true"
					<?php echo ( !empty( $tag_check) )? "checked=\"checked\"": ""; ?> />
						<label for="tag_check" >tags</label>
			</div>
			<input type="submit" name="search_submit" value="ค้นหา" data-mini="true" />
		</form>
		<script type="text/javascript" >
		$( function() {
			$('input[name=search_submit]').click( function() {
				$('form[name=search_form]').submit();
			});
		});
		</script>
		
		<h2>ผลการค้นหา</h2>
		<ul>
<?php
if( !empty( $articles ) ) {
	foreach( $articles as $article ) {
?>
			<li>News  <a href="<?php echo site_url( "article/" .$article->get_article_id() ); ?>" target="_blank" 
					><?php echo $article->get_title(); ?></a></li>
<?php
	}
}

if( !empty( $documents ) ) {
	foreach( $documents as $document ) {
?>
			<li>Publications  <a href="<?php echo site_url( "document/" .$document->get_document_id() ); ?>" target="_blank" 
					><?php echo $document->get_title(); ?></a></li>
<?php
	}
}

if( !empty( $galleries ) ) {
	foreach( $galleries as $gallery ) {
?>
			<li>Gallery <a href="<?php echo site_url( "gallery/" .$gallery->get_gallery_id() ); ?>" target="_blank" 
					><?php echo $gallery->get_title(); ?></a></li>
<?php
	}
}

if( !empty( $multimedia ) ) {
	foreach( $multimedia as $mult ) {
?>
			<li>Multimedia  <a href="<?php echo site_url( "multimedia/" .$mult->get_multimedia_id() ); ?>" target="_blank" 
					><?php echo $mult->get_title(); ?></a></li>
<?php
	}
}

if( !empty( $tags ) ) {
	foreach( $tags as $tag ) {
?>
			<li>tags  <a href="<?php echo site_url( "tag/" .$tag->get_tag_id() ); ?>" target="_blank" 
					><?php echo $tag->get_name(); ?></a></li>
<?php
	}
}
?>
		</ul>
	
	</div>
</div>