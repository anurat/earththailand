<form action="<?php echo site_url( 'search' ); ?>" method="post" data-ajax="false" >

<table class="menu_search" >
<tr>
	<td>
		<!-- responsive menu -->
		<select data-role="none" >
			<option value="" selected="selected" >MENU</option>
			<option value="<?php echo site_url( '' ); ?>" >Home</option>
			<option value="<?php echo site_url( 'pollutions' ); ?>" >Pollution Hotspots</option>
			<option value="<?php echo site_url( 'articles' ); ?>" >News</option>
			<option value="<?php echo site_url( 'documents' ); ?>" >Documents</option>
			<option value="<?php echo site_url( 'gallery_list' ); ?>" >Gallery</option>
			<option value="<?php echo site_url( 'events' ); ?>" >Events</option>
			<option value="<?php echo site_url( 'about' ); ?>" >About</option>
			<option value="<?php echo site_url( 'tags' ); ?>" >Tags</option>
			<option value="<?php echo site_url( "earth1" ); ?>" >PRTR & Community Right-to-Know</option>
			<option value="<?php echo site_url( "earth2" ); ?>" >Communities in Action</option>
			<option value="<?php echo site_url( "earth3" ); ?>" >Industrial & Hazardous Waste Mangement</option>
			<option value="<?php echo site_url( "earth4" ); ?>" >Map Ta Phut Studies</option>
			<option value="<?php echo site_url( "earth5" ); ?>" >Chemicals & Product Life-Cycle Management</option>
			<option value="<?php echo site_url( "earth7" ); ?>" >Corporate Accountability</option>
			<option value="<?php echo site_url( "earth6" ); ?>" >Policy Reference</option>
			<option value="<?php echo site_url( "multimedia_list" ); ?>" >Multimedia</option>
			
			<option value="" disabled="disabled" >Thailand Network</option>
<?php
foreach( $thai_link_list as $link ) {
?>
			<option value="<?php echo $link->get_link_url(); ?>"
					><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
	
			<option value="" disabled="disabled" >Regional Network</option>
<?php
foreach( $regional_link_list as $link ) {
?>
			<option value="<?php echo $link->get_link_url(); ?>"
					><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
			<option value="" disabled="disabled" >Global Network</option>
<?php
foreach( $global_link_list as $link ) {
?>
			<option value="<?php echo $link->get_link_url(); ?>"
					><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
		</select>
	</td>
	<td>
		<input id="text-search" name="text-search" data-role="none" maxlength="20"
			   type="text" size="10" placeholder="Search" />
	</td>
	<td>
		<input type="submit" data-role="none" value="SEARCH" />
	</td>
</tr>
</table>

</form>
