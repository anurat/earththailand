<div id="article" >
	<div>
		<h1>Tag</h1>
		
		<b><?php echo $tag->get_name(); ?></b><br />
 

		tagging <?php echo sizeof( $tag_links ); ?> items<br />
<?php
if( sizeof( $tag_links ) > 0 ) {
?>
		<script type="text/javascript" >
		$( function() {


			$("a.button").click( function(e) {
				e.preventDefault();
				
				
			});
		});
		</script>

		<ul class="oneline">
<?php
	$cms_category = $this->cms_model->get_cms_category();
	$cms_article = $this->cms_model->get_cms_article();
	$cms_document = $this->cms_model->get_cms_document();
	$cms_gallery = $this->cms_model->get_cms_gallery();
	$cms_multimedia = $this->cms_model->get_cms_multimedia();
	foreach( $tag_links as $tag_link ) {

		$table = $tag_link->get_table();
		$table_id = $tag_link->get_table_id();
		$obj = null;
		switch( $table ) {
		case 'category':
			$obj = $cms_category->get_category( $table_id );
			break;
		case 'article':
			$obj = $cms_article->get_article( $table_id );
			break;
		case 'document':
			$obj = $cms_document->get_document( $table_id );
			break;
		case 'gallery':
			$obj = $cms_gallery->get_gallery( $table_id );
			break;
		case 'multimedia':
			$obj = $cms_multimedia->get_multimedia( $table_id );
			break;
		}
		
		if( $table == 'category' ) {
?>
			<li>
				<span class="left_align box">Category</span>
				<a href="<?php echo site_url( "page/category/" .$table_id ); ?>" class="title"
						target="_blank" ><?php echo $obj->get_title(); ?></a></h4>
				<span class="right_align box" >
					<a class="button" href="#" data-button-type="delete_tag" ><?php echo $this->lang->line('header_delete'); ?></a>
				</span>
			</li>
<?php
		}
		else {
?>
			<div>
				<span class="left_align box"><?php echo $table; ?></span>
				<a href="<?php echo site_url( "{$table}/" .$table_id ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$obj->get_list_img() ); ?>" alt="" /></a>
				<a href="<?php echo site_url( "{$table}/" .$table_id ); ?>" 
						target="_blank" ><?php echo $obj->get_list_title(); ?></a>
				<span class="right_align box" >
					<a class="button" href="#" data-button-type="delete_tag" 
							data-tag-link-id="<?php echo $tag_link->get_tag_link_id(); ?>" 
							><?php echo $this->lang->line('header_delete'); ?></a>
				</span>
			</div>
<?php
		}
	}
?>
		</ul>
<?php
}
?>
	</div>
</div>
