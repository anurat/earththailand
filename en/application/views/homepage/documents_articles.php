<div class="two_columns" >
	<div class="column_one" >
		<h3><a href="<?php echo site_url( "/documents" ); ?>" target="_blank" >Documents</a></h3>
		<div class="items" >
<?php
foreach( $document_list as $i => $document ) {
	if( $i >= 15 ) {
		break;
	}
?>
			<div>
<?php
	$image = $document->get_image();
	if( !empty( $image ) ) {
?>
				<a href="<?php echo site_url( "document/" .$document->get_table_id() ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$image ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "document/" .$document->get_table_id() ); ?>" target="_blank"
						><?php echo $document->get_title(); ?></a></h4>
				<div class="desc" ><?php echo $document->get_description(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "document/" .$document->get_table_id() ); ?>"
						target="_blank" >read more...</a></span>
			</div>
<?php
}
?>
		</div>

		<span class="moreinfo" ><a href="<?php echo site_url( "/documents" ); ?>" >View all publications</a></span>
	</div>
 
	<div class="column_two" >
		<h3><a href="<?php echo site_url( "/articles" ); ?>" target="_blank" >News</a></h3>
		<div class="items" >
<?php
foreach( $article_list as $i => $article ) {
	if( $i >= 15 ) {
		break;
	}
?>
			<div>
<?php
	$image = $article->get_image();
	if( !empty( $image ) ) {
?>
				<a href="<?php echo site_url( "article/" .$article->get_table_id() ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$image ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "article/" .$article->get_table_id() ); ?>" target="_blank"
						><?php echo $article->get_title(); ?></a></h4>
				<div class="desc" ><?php echo $article->get_description(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "article/" .$article->get_table_id() ); ?>"
						target="_blank" >read more...</a></span>
			</div>
<?php
}
?>
		</div>

		<span class="moreinfo" ><a href="<?php echo site_url( "/articles" ); ?>" target="_blank" >View all news</a></span>
	</div>
</div>