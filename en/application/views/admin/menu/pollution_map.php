<?php include APPPATH .'views/admin/jquery_ui_sortable.php'; ?>

<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_pollution_map'); ?></h1>


<a class="button" href="#" id="add_button" ><?php echo $this->lang->line( 'header_add' ); ?></a><br />
<span id="message" class="result" ></span>
<br />

<?php include APPPATH .'views/admin/pagination.php'; ?>

<ul id="sortable" >
<?php
$i = $page['index1'];

$cms_history = $this->cms_model->get_cms_history();
$cms_item_list = $this->cms_model->get_cms_item_list();
foreach( $pollution_list as $pollution ) {

	$pollution_id = $pollution->get_pollution_id();
	$latest = $cms_history->get_latest_history( 'pollution', $pollution_id );
	$first = $cms_history->get_first_history( 'pollution', $pollution_id );

	$item_list = $cms_item_list->get_item_list_by_table( 'pollution', $pollution_id );
	$lock = $item_list->get_lock();
	$enable = $item_list->get_enable();
    $order = $item_list->get_order();
?>
	<li class="ui-state-default <?php echo ( $lock )? "ui-state-disabled": ""; ?>" 
            data-table-id="<?php echo $pollution_id; ?>" 
            data-order="<?php echo $order; ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<div class="left_align pad" >
            <?php echo $i; ?><br />
            <input type="checkbox" name="op[]" value="<?php echo $pollution_id; ?>" />
        </div>
		<div class="left_align pad" >
		    <div class="checkbox" >
<?php
    if( $role == Cms_user::ADMIN_ROLE ) {
?>
                <input type="checkbox" id="check_lock_<?php echo $pollution_id; ?>" 
                        name="check_lock_<?php echo $pollution_id; ?>"
                        <?php echo ( $lock )? "checked=\"checked\"": ""; ?> />
                <label for="check_lock_<?php echo $pollution_id; ?>" class="lock" title="Lock" >L</label>
<?php
    }
?>
            </div>
		</div>
		<div class="left_align pad" >
            <img src="<?php echo site_url( "userfiles/" .$pollution->get_mark_img() ); ?>" alt="" />
        </div>
		<a class="title" href="<?php echo site_url( "admin/menu/update_pollution/{$pollution_id}" ); ?>" 
				><?php echo $pollution_id; ?> <?php echo $pollution->get_title(); ?></a>
		<div class="right_align pad" >
			<div class="radio" >
				<input type="radio" id="radio_on_<?php echo $pollution_id; ?>" 
                        name="homepage_radio_<?php echo $pollution_id; ?>" 
						data-status="on" <?php echo ( $enable )? "checked=\"checked\"": ""; ?> />
				<label for="radio_on_<?php echo $pollution_id; ?>" class="on" 
						><?php echo $this->lang->line('header_on'); ?></label>
				<input type="radio" id="radio_off_<?php echo $pollution_id; ?>" 
                        name="homepage_radio_<?php echo $pollution_id; ?>" 
						data-status="off" <?php echo ( !$enable )? "checked=\"checked\"": ""; ?> />
                <label for="radio_off_<?php echo $pollution_id; ?>" class="off" 
                        ><?php echo $this->lang->line('header_off'); ?></label>
			</div>
			<select name="op" >
				<option value="0" >-</option>
				<option value="delete" ><?php echo $this->lang->line('header_delete'); ?></option>
				<option value="move" ><?php echo $this->lang->line('header_move'); ?></option>
			</select>
		</div>
		<div class="right_align pad" >
				<?php echo date( 'j M Y G:i', $first->get_changed_date() ); ?>
				<?php echo $first->getUser()->get_name(); ?><br />
				<a href="<?php echo site_url( "admin/menu/pollution_history/{$pollution_id}" ); ?>"
				        title="History" 
				        ><?php echo date( 'j M Y G:i', $latest->get_changed_date() ); ?> 
				        <?php echo $latest->getUser()->get_name(); ?></a>
		</div>
	</li>
<?php
    $i++;
}
?>
</ul>

<?php include APPPATH .'views/admin/pagination.php'; ?>
