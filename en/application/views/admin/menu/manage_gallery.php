<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_gallery'); ?>
 > <?php echo $this->lang->line('menu_manage_gallery'); ?></h1>

<h2><?php echo $gallery->get_title(); ?></h2>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {

	load_gallery( <?php echo $gallery_id; ?> );

	$('a[name=upload]').click( function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				load_gallery( <?php echo $gallery_id; ?> );
			},
			startupPath : "Gallery:/<?php echo $gallery_id; ?>/"
		});
	});
	
	$( "#sortable_box" ).sortable({
		placeholder: "ui-state-highlight",
		containment: "div#content",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_photo = new Array();
			var a_order = new Array();
			$("#sortable_box li").each( function(i) {
				a_photo.push(  $(this).data('photo-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { photo: a_photo, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/gallery_photo" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete.' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});
	
	dialog = $( "#dialog_form" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Update: function() {
			
				$.ajax({
					data: { 
						photo_id: $("#photo_id").val(), 
						name: $("#name").val(), 
						description: $("#description").val()
					},
					type: 'POST',
					url: "<?php echo site_url( "ajax/photo/update" ); ?>"
				})
				.always( function( data ) {
				
					load_gallery( <?php echo $gallery_id; ?> );
					$('#message').html( 'Photo info updated.' )
							.show()
							.delay( 5000 )
							.fadeOut();
				});
				dialog.dialog( "close" );
			},
			Cancel: function() {
				dialog.dialog( "close" );
			}
		},
	});
	
	$('#sortable_box').on( 'click', 'span.box', function() {
	
		var li = $(this).parent();
		$("#photo_id").val( li.data("photo-id") );
		$("#name").val( li.find("span.name").html() );
		$("#description").val( li.find("span.description").html() );
		dialog.dialog( "open" );
	});
});

function load_gallery( gallery_id ) {
	$.getJSON( "<?php echo site_url( "/ajax/gallery_photo" ); ?>/" +gallery_id, function( photos ) {
		
		var html = '';
		$.each( photos, function( i, photo ) {
			html += "<li class=\"ui-state-default\" data-photo-id=\"" +photo._photo_id +"\" data-order=\"" +photo._order +"\" title=\"" +photo._path +"\" >\n"
					+"	<span class=\"ui-icon ui-icon-arrow-4\"></span>\n"
					+"	<div>\n"
					+"		<img src=\"<?php echo site_url( "userfiles/_thumbs/Gallery" ); ?>/" +gallery_id +"/" +photo._path +"\" alt=\"\" />\n"
					+"	</div>\n"
					+"	<span class=\"box name\" title=\"" +photo._name +"\" >" +photo._name +"</span>\n"
					+"	<span class=\"box description\" title=\"" +photo._description +"\" >" +photo._description +"</span>\n"
					+"</li>\n";
		});
		
		$("#sortable_box").html( html );
		$("#photo_no").html( photos.length );
	});
}
</script>

<a href="#" class="button" name="upload" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
<span id="message" class="result" ></span><br />
<?php echo $this->lang->line('gallery_image_folder'); ?> <b>Gallery/<?php echo $gallery_id; ?></b> <br />
<br />

<?php echo $this->lang->line('gallery_image_no'); ?>: <span id="photo_no" ></span>
<ul id="sortable_box" ></ul>

<div id="dialog_form" title="Update photo info" >
	<form>
		<fieldset>
			<label for="name">Name</label><br />
			<input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"><br />
			<br />
			<label for="description">Description</label><br />
			<textarea name="description" id="description" class="textarea ui-widget-content ui-corner-all"></textarea><br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="photo_id" id="photo_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>