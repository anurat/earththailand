<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_slideshow'); ?></h1>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {

	load_gallery();

	$('a#upload_button').on( 'click', function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				load_gallery();
			},
			startupPath : "Slideshows:/1/"
		});
	});
	
	$( "#sortable_box" ).sortable({
		placeholder: "ui-state-highlight",
		containment: "div#content",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_image = new Array();
			var a_order = new Array();
			$("#sortable_box li").each( function(i) {
				a_image.push(  $(this).data('image-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { image: a_image, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/image" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete.' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});
	
	dialog = $( "#dialog_form" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Update: function() {
			
				$.ajax({
					data: { 
						image_id: $("#image_id").val(), 
						name: $("#name").val(), 
						description: $("#description").val()
					},
					type: 'POST',
					url: "<?php echo site_url( "ajax/image/update" ); ?>"
				})
				.always( function( data ) {
				
					load_gallery();
					$('#message').html( 'Photo info updated.' )
							.show()
							.delay( 5000 )
							.fadeOut();
				});
				dialog.dialog( "close" );
			},
			Cancel: function() {
				dialog.dialog( "close" );
			}
		},
	});
	
	$('#sortable_box').on( 'click', 'span.box', function() {
	
		var li = $(this).parent();
		$("#image_id").val( li.data("image-id") );
		$("#name").val( li.find("span.name").html() );
		$("#description").val( li.find("span.description").html() );
		dialog.dialog( "open" );
	});
});

function load_gallery() {
	$.getJSON( "<?php echo site_url( "/ajax/gallery_image/slideshow/1" ); ?>", function( images ) {
		
		var html = '';
		$.each( images, function( i, image ) {
			html += "<li class=\"ui-state-default\" data-image-id=\"" +image._image_id +"\" data-order=\"" +image._order +"\" title=\"" +image._path +"\" >\n"
					+"	<span class=\"ui-icon ui-icon-arrow-4\"></span>\n"
					+"	<div>\n"
					+"		<img src=\"<?php echo site_url( "userfiles/_thumbs/Slideshows/1" ); ?>/" +image._path +"\" alt=\"\" />\n"
					+"	</div>\n"
					+"	<span class=\"box name\" title=\"" +image._name +"\" >" +image._name +"</span>\n"
					+"	<span class=\"box description\" title=\"" +image._description +"\" >" +image._description +"</span>\n"
					+"</li>\n";
		});
		
		$("#sortable_box").html( html );
		$("#image_no").html( images.length );
	});
}
</script>

<a href="#" class="button" id="upload_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
<span id="message" class="result" ></span><br />
Please upload images into folder <b>Slideshows/1</b> <br />
<br />

No. of slides: <span id="image_no" ></span>
<ul id="sortable_box" ></ul>

<div id="dialog_form" title="Update slide info" >
	<form>
		<fieldset>
			<label for="name">Title</label><br />
			<input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"><br />
			<br />
			<label for="description">Link</label><br />
			<textarea name="description" id="description" class="textarea ui-widget-content ui-corner-all"></textarea><br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="image_id" id="image_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>