<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_footer'); ?></h1>

<script type="text/javascript" >
$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor = CKEDITOR.replace( 'editor1', {
		bodyClass: 'footer',
		height: 300
	});
	
	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});
</script>
<form method="post" >
	<textarea class="ckeditor" name="editor1" >
		<?php echo $footer; ?>
	</textarea>

	<div class="save_cancel" >
		<?php echo ( !empty( $result ) )? "<span class=\"result\" >{$result}</span>": ''; ?>
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
</form>

