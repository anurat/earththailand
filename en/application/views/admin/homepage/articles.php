<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_articles'); ?></h1>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {	

	$("#sortable").sortable({
		placeholder: "ui-state-highlight",
		axis: "y",
		cursor: "pointer",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_homepage = new Array();
			var a_order = new Array();
			$("#sortable li").each( function(i) {
				a_homepage.push(  $(this).data('homepage-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { homepage: a_homepage, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/homepage_article" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});

	$('li a.button').on( 'click', function(e) {
		e.preventDefault();
        var homepage_id = $(this).closest('li').data('homepage-id');
		confirm_dialog( homepage_id );
	});
});

function confirm_dialog( homepage_id ) {
	$('<div></div>').appendTo('body')
			.html( 'Do you want to remove this article from homepage?' )
			.dialog({
				modal: true, 
				title: 'Remove article', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						location.href = "<?php echo site_url( "admin/homepage/remove_article" ); ?>/" +homepage_id;
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}

</script>

<span id="message" class="result" ></span><br />

<?php echo $this->lang->line('article_no'); ?>: <?php echo sizeof( $homepage_list ); ?><br />

<ul id="sortable" >
<?php
$cms_article = $this->cms_model->get_cms_article();
foreach( $homepage_list as $homepage ) {
	$homepage_id = $homepage->get_homepage_id();
	$article_id = $homepage->get_table_id();
	$article = $cms_article->get_article( $article_id );
?>
	<li class="ui-state-default" data-homepage-id="<?php echo $homepage_id; ?>" data-order="<?php echo $homepage->get_order(); ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<a class="title" href="<?php echo site_url( "admin/homepage/update_article/" .$article_id ); ?>" 
		        ><?php echo $article_id .' ' .$article->get_title(); ?></a>
		<div class="right_align pad" >
            <a href="#" class="button" ><?php echo $this->lang->line('header_remove'); ?></a>
		</div>
	</li>
<?php
}
?>
</ul>
