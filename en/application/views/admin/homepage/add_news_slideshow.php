<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_news_slideshow'); ?>
	> <?php echo $this->lang->line('menu_add_news_slideshow'); ?></h1>
	
<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
	$("select").selectmenu({
		change: function() {
			$("#content_title").autocomplete( "option", "source", 
				"<?php echo site_url("/ajax/categories"); ?>/" +$("select :selected").val() );
		}
	});
	
	$("#content_title").autocomplete({
		minLength: 1,
		source: "<?php echo site_url("/ajax/categories"); ?>/" +$("select :selected").val()
	});
	
	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});

</script>

<span class="red_text" ><?php echo ( !empty( $error ) )? $error: ''; ?></span>
<form method="POST" >
	<?php echo $this->lang->line('news_slideshow_content_type'); ?>:<br />
	<select name="content_type" >
	<?php
	foreach( $cat_list as $cat ) {
	?>
		<option value="<?php echo $cat->get_category_id(); ?>" 
				><?php echo $cat->get_title(); ?></option>
	<?php
	}
	?>
	</select>
	<br />
	<br />

	<?php echo $this->lang->line('news_slideshow_content_title'); ?>:<br />
	<input type="text" id="content_title" name="content_title" value="" />
	<br />
	<br />

	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
</form>
