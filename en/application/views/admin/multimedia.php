<?php
if( empty( $multimedia_id ) ) {
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_multimedia'); ?> 
 > <?php echo $this->lang->line('menu_add_multimedia'); ?></h1>
<?php
}
else {
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_multimedia'); ?> 
 > <?php echo $this->lang->line('menu_update_multimedia'); ?></h1>
<?php
}

$title = ( empty( $multimedia_id ) )? '': htmlspecialchars( $multimedia->get_title() );
$description1 = ( empty( $multimedia_id ) )? '': $multimedia->get_description1();
$video_code = ( empty( $multimedia_id ) )? '': $multimedia->get_video_code();
$description2 = ( empty( $multimedia_id ) )? '': $multimedia->get_description2();
$list_title = ( empty( $multimedia_id ) )? '': htmlspecialchars( $multimedia->get_list_title() );
$list_desc = ( empty( $multimedia_id ) )? '': $multimedia->get_list_desc();
$list_img = ( empty( $multimedia_id ) )? '': $multimedia->get_list_img();

?>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor1 = CKEDITOR.replace( 'editor1', {
		bodyClass: 'article_description',
		height: 300
	});
	var editor2 = CKEDITOR.replace( 'editor2', {
		bodyClass: 'article_description',
		height: 300
	});
	var editor3 = CKEDITOR.replace( 'editor3', {
		bodyClass: 'article_description',
		height: 100
	});

	$('a#list_img_button').on( 'click', function( e ) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				d = new Date();
				$("#multimedia_list_img").attr("src", file_url +"?"+d.getTime());
				var n = file_url.indexOf( 'userfiles/' ) +10;
				$("#list_img").val( file_url.substring( n ) );
			}
		});
	});
	
	$("#checkbox_box").buttonset();

	$('div.save_cancel a.button').on( 'click', function(e) {
		e.preventDefault();
        var button_type = $(this).data('button-type');
        switch( button_type ) {
        case 'cancel':
            history.back();
            break;
        default:
            $('form[name=multimedia_form] input[name=save_type]').val( button_type );
            $('form[name=multimedia_form]').submit();
            break;
        }
	});
});
</script>

<form method="post" name="multimedia_form" enctype="multipart/form-data" >
	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $multimedia_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>

	<?php echo $this->lang->line('multimedia_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" />
	<br />
	<br />
	
	<?php echo $this->lang->line('multimedia_description1'); ?><br />
	<textarea class="ckeditor" name="editor1" >
		<?php echo $description1; ?>
	</textarea>
	<br />
	
	<?php echo $this->lang->line('multimedia_video_code'); ?><br />
	<textarea name="video_code" ><?php echo $video_code; ?></textarea>
	<br />
	<br />
	
	<?php echo $this->lang->line('multimedia_description2'); ?><br />
	<textarea class="ckeditor" name="editor2" >
		<?php echo $description2; ?>
	</textarea>
	<br />

	<?php echo $this->lang->line('multimedia_tags'); ?><br />
	<input type="text" name="tags" value="<?php echo $tags; ?>" /><br />
	<br />
	
	<hr>
	<h2 id="list_item" ><?php echo $this->lang->line('header_item'); ?></h2>
	
	<?php echo $this->lang->line('multimedia_list_title'); ?><br />
	<input type="text" name="list_title" value="<?php echo $list_title; ?>" /><br />
	<?php echo $this->lang->line('multimedia_newline'); ?><br />
	<br />
	
	<?php echo $this->lang->line('multimedia_list_desc'); ?><br />
	<textarea class="ckeditor" name="editor3" >
		<?php echo $list_desc; ?>
	</textarea>
	<br />
	
	<?php echo $this->lang->line('multimedia_list_img'); ?><br />
	<input type="text" id="list_img" name="list_img" style="width: 400px; " readonly="readonly"
			value="<?php echo $list_img; ?>" />
	<a href="#" class="button" id="list_img_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a><br />
	<?php echo $this->lang->line('multimedia_image_upload'); ?><br />
	<img id="multimedia_list_img" src="<?php echo site_url( 'userfiles/' .$list_img ); ?>" style="max-width: 200px; max-height: 200px; " alt="" />
	<br />
	
	<?php echo $this->lang->line('header_category'); ?><br />
	<div id="checkbox_box">
<?php
$cms_category = $this->cms_model->get_cms_category();
foreach( $category_list as $category ) {
	$category_id = $category->get_category_id();
	$has_cat_link = $cms_category->has_cat_link( $category_id, $cat_links );
?>
		<input type="checkbox" id="category[<?php echo $category_id; ?>]" name="category[<?php echo $category_id; ?>]" 
				<?php echo ( empty( $multimedia_id ) && $category_id == Cms_category::MULTIMEDIA_CAT )? "checked=\"checked\"": ""; ?>
				<?php echo ( !empty( $multimedia_id ) && $has_cat_link )? "checked=\"checked\"": ""; ?>
				><label for="category[<?php echo $category_id; ?>]"><?php echo $category->get_title(); ?></label>
<?php
}
?>
	</div>

	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $multimedia_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<input type="hidden" name="save_type" value=""/>
	
</form>
<br />

