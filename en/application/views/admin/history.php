<table>
<tr>
	<th>Date</th>
	<th>Name</th>
	<th>E-mail</th>
</tr>
<?php
foreach( $histories as $history ) {
	$user = $history->getUser();
?>
<tr>
	<td><?php echo date( 'j F Y G:i', $history->get_changed_date() ); ?></td>
	<td><?php echo $user->get_name(); ?></td>
	<td><?php echo $user->get_email(); ?></td>
</tr>
<?php
}
?>
</table>