<?php
if( empty( $user_id ) ) {
?>
<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_user'); ?>
 > <?php echo $this->lang->line('menu_add_user'); ?></h1>
<?php
}
else {
?>
<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_user'); ?>
 > <?php echo $this->lang->line('menu_update_user'); ?></h1>
<?php
}

$cms_user = $this->cms_model->get_cms_user();
$user = $cms_user->get_user( $user_id );

$name = ( empty( $user_id ) )? '': $user->get_name();
$email = ( empty( $user_id ) )? '': $user->get_email();
$role = ( empty( $user_id ) )? '': $user->get_role();
$enabled = ( empty( $user_id ) )? '': $user->get_enable();
$created_date = ( empty( $user_id ) )? 0: $user->get_created_date();
$last_signedin = ( empty( $user_id ) )? 0: $user->get_last_signedin();
?>
<br />

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
    
    $('select').selectmenu();
    $('div#enable_checkbox').buttonset();

	$('a#reset_button').on( 'click', function(e) {
		e.preventDefault();
		confirm_dialog();		
	});
	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});

function confirm_dialog() {
	$('<div></div>').appendTo('body')
			.html( 'Do you want to reset the password?' )
			.dialog({
				modal: true, 
				title: 'Reset password', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						window.location = "<?php echo site_url( "admin/system/reset_password/{$user_id}" ); ?>";
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}

</script>

<form method="post">

	<?php echo $this->lang->line('user_name'); ?>: <input type="text" name="name" value="<?php echo $name; ?>" style="width: 400px;" /><br />
	<br />
	
	<?php echo $this->lang->line('user_email'); ?>: <input type="text" name="email" value="<?php echo $email; ?>" style="width: 400px;" /><br />
	<br />
	
	<?php echo $this->lang->line('user_password'); ?>: 
<?php
if( !empty( $user_id ) ) {
?>
	<a href="#" class="button" id="reset_button" ><?php echo $this->lang->line('header_reset'); ?></a>
<?php
}
else {
?>
    <input type="password" name="password" value="toxicfree" style="width: 200px;" />
<?php
}
?>
	<br />
	<br />
	
	<?php echo $this->lang->line('user_role'); ?>:
	<select name="role" style="min-width: 100px;" >
		<option value="admin" <?php echo ( $role == Cms_user::ADMIN_ROLE )? 'selected="selected"': ''; ?> >admin</option>
		<option value="editor" <?php echo ( $role == Cms_user::EDITOR_ROLE )? 'selected="selected"': ''; ?> >editor</option>
	</select><br />
	<br />
	
	<?php echo $this->lang->line('user_enabled'); ?>: 
	<div id="enable_checkbox" style="display: inline" >
	    <input type="checkbox" name="enable" id="enable" 
	            <?php echo ( $enabled )? 'checked="checked"': ''; ?> />
        <label for="enable">Enabled</label>
	</div>
	<br />
	<br />
	
	<?php echo $this->lang->line('user_created_date'); ?>: 
    <?php echo empty( $created_date )? '': date( 'j F Y G:i:s', $created_date ); ?><br />
	
	<?php echo $this->lang->line('user_last_signedin'); ?>: 
	<?php echo empty( $last_signedin )? '': date( 'j F Y G:i:s', $last_signedin ); ?><br />
	
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
</form>