<?php
if( $table == 'article' ) {
?>
<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_articles'); ?>
 > <?php echo $this->lang->line('menu_update_article'); ?></h1>
<?php
}
else if( $table == 'document' ) {
?>
<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_documents'); ?>
 > <?php echo $this->lang->line('menu_update_document'); ?></h1>
<?php
}

$title = htmlspecialchars( $homepage->get_title() );
$description = $homepage->get_description();
$image = $homepage->get_image();
?>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor2 = CKEDITOR.replace( 'editor2', {
		bodyClass: 'item_description',
		height: 100
	});

	$('a#image_path_button').on( 'click', function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				d = new Date();
				$("#homepage_img").attr("src", file_url +"?"+d.getTime());
				var n = file_url.indexOf( 'userfiles/' ) +10;
				$("#image_path").val( file_url.substring( n ) );
			}
		});
	});
	
	$('a#clear_image').on( 'click', function(e) {
		e.preventDefault();
		$("#homepage_img").attr("src", "" );
		$("#image_path").val("");
	});
		
	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});
</script>

<form method="post" enctype="multipart/form-data" >
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
	<?php echo $this->lang->line('document_list_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" /><br />
	<?php echo $this->lang->line('document_newline'); ?><br />
	<br />
	
	<?php echo $this->lang->line('document_list_desc'); ?><br />
	<textarea class="ckeditor" name="editor2" >
		<?php echo $description; ?>
	</textarea>
	<br />

	<?php echo $this->lang->line('document_list_img'); ?><br />
	<input type="text" id="image_path" name="image_path" style="width: 400px; " readonly="readonly" 
			value="<?php echo $image; ?>" />
	<a href="#" class="button" id="image_path_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
	<a href="#" id="clear_image" ><?php echo $this->lang->line('header_clear'); ?></a><br />
	<?php echo $this->lang->line('document_image_upload'); ?><br />
	<img id="homepage_img" src="<?php echo site_url( 'userfiles/' .$image ); ?>" style="max-width: 200px; max-height: 200px; " alt="" />
	<br />
	<br />

			
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
	
</form>

