<?php
if( empty( $pollution_id ) ) {
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_pollution_map'); ?>
 > <?php echo $this->lang->line('menu_add_pollution'); ?></h1>
<?php
}
else {
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_pollution_map'); ?>
 > <?php echo $this->lang->line('menu_update_pollution'); ?></h1>
<?php
}

$cms_pollution = $this->cms_model->get_cms_pollution();

$current_id = ( empty( $pollution_id ) )? $cms_pollution->get_next_id(): $pollution->get_pollution_id();
$title = ( empty( $pollution_id ) )? '': htmlspecialchars( $pollution->get_title() );
$video_code = ( empty( $pollution_id ) )? '': $pollution->get_video_code();
$description = ( empty( $pollution_id ) )? '': $pollution->get_description();
$list_title = ( empty( $pollution_id ) )? '': htmlspecialchars( $pollution->get_list_title() );
$list_desc = ( empty( $pollution_id ) )? '': $pollution->get_list_desc();
$pollution_date = ( empty( $pollution_id ) )? time(): $pollution->get_pollution_date();
$type = ( empty( $pollution_id ) )? '': $pollution->get_type();
$mark_img = ( empty( $pollution_id ) )? '': $pollution->get_mark_img();
$x = ( empty( $pollution_id ) )? '0': $pollution->get_x();
$y = ( empty( $pollution_id ) )? '0': $pollution->get_y();
?>

<script type="text/javascript" src="<?php echo site_url( "js/mapsvg/js/raphael.js" ); ?>" ></script>
<script type="text/javascript" src="<?php echo site_url( "js/mapsvg/js/jquery.mousewheel.min.js" ); ?>" ></script>    
<script type="text/javascript" src="<?php echo site_url( "js/mapsvg/js/mapsvg.js" ); ?>" ></script>
<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor1 = CKEDITOR.replace( 'editor1', {
		bodyClass: 'popover_description',
		height: 100
	});
	var editor2 = CKEDITOR.replace( 'editor2', {
		bodyClass: 'article_description',
		height: 1000
	});
    
    $("select[name=type]").selectmenu();

	$('a#mark_img_button').on( 'click', function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				d = new Date();
				$("#pollution_mark_img").attr("src", file_url +"?"+d.getTime());
				var n = file_url.indexOf( 'userfiles/' ) +10;
				$("#mark_img").val( file_url.substring( n ) );
			},
			startupPath : "Images:/marker/"
		});
	});
	
	$('a#clear_mark_img').on( 'click', function(e) {
		e.preventDefault();
		$("#pollution_mark_img").attr("src", "" );
		$("#mark_img").val("");
	});
		
	$("#pollution_date").datepicker({ 
		dateFormat: "d MM yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "2005:2020"
	});
	
	$('div.save_cancel a.button').on( 'click', function(e) {
		e.preventDefault();
        var button_type = $(this).data('button-type');
        switch( button_type ) {
        case 'cancel':
            history.back();
            break;
        default:
            $('form[name=pollution_form] input[name=save_type]').val( button_type );
            $('form[name=pollution_form]').submit();
            break;
        }
	});
	
    msvg = $("#thai_map").mapSvg({
        source     : "<?php echo site_url( "images/thai_map.svg" ); ?>",
        loadingText: "กำลังเปิดแผนที่ ...",
        disableAll : true,
        zoom       : true,
        zoomButtons: {
            'show': true,
            'location': 'left'
        },
        zoomLimit  : [0,7],
        zoomDelta  : 1.5,
        pan        : true,
        //disabledClickable : true,
        panLimit   : true,
        responsive : true,
        marks      : [{
            "attrs": {
                "x":"<?php echo $x +1; ?>",
                "y":"<?php echo $y +1; ?>",
                "width":"6.8",
                "height":"12",
                "src":"<?php echo site_url( "js/mapsvg/markers/_pin_default.png" ); ?>",
                "cursor":"pointer",
                "target":"",
            },
            "tooltip":"",
            "popover":"",
            "width":"13",
            "height":"23"
        }],
        editMode   : true,
        marksEditHandler: function() {}
    });
    
    $("#thai_map").on( "mouseup", function() {
        var marks = $(this).mapSvg().marksGet();
        $("input[name=x]").val( (marks[0].attrs.x -1).toFixed(2) );
        $("input[name=y]").val( (marks[0].attrs.y -1).toFixed(2) );
    });
});
</script>

<form method="post" name="pollution_form">
	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $pollution_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
	<?php echo $this->lang->line('pollution_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" />
	<br />
	<br />
	
	<?php echo $this->lang->line('pollution_video_code'); ?><br />
	<textarea name="video_code" ><?php echo $video_code; ?></textarea>
	<br />
	<br />

	<?php echo $this->lang->line('pollution_description'); ?><br />
	<textarea class="ckeditor" name="editor2" >
		<?php echo $description; ?>
	</textarea>
	<br />

	<?php echo $this->lang->line('pollution_tags'); ?><br />
	<input type="text" name="tags" value="<?php echo $tags; ?>" /><br />
	<br />
	
	<hr />
	<h2 id="list_item" ><?php echo $this->lang->line('header_item'); ?></h2>
	
	<?php echo $this->lang->line('pollution_list_title'); ?><br />
	<input type="text" class="title" name="list_title" value="<?php echo $list_title; ?>" /><br />
	<?php echo $this->lang->line('pollution_newline'); ?><br />
	<br />

	<?php echo $this->lang->line('pollution_list_desc'); ?><br />
	<textarea class="ckeditor" name="editor1" >
		<?php echo $list_desc; ?>
	</textarea>
	<br />
	
	<?php echo $this->lang->line('pollution_date'); ?><br />
	<input type="text" id="pollution_date" name="pollution_date" style="width: 400px; "
			value="<?php echo date( 'j F Y', $pollution_date ); ?>" /><br />
	<br />
	
	<hr />
	<h2><?php echo $this->lang->line('pollution_marker'); ?></h2>
	
	<?php echo $this->lang->line('pollution_type'); ?>:<br />
	<select name="type" >
		<option value="" >-</option>
		<option value="1" <?php echo ( $type == '1' )? 'selected="selected"': ''; ?> >พื้นที่เสี่ยง</option>
		<option value="2" <?php echo ( $type == '2' )? 'selected="selected"': ''; ?> >พื้นที่ปนเปื้อน</option>
		<option value="3" <?php echo ( $type == '3' )? 'selected="selected"': ''; ?> >พื้นที่ฟื้นฟูแล้ว</option>
		<option value="4" <?php echo ( $type == '4' )? 'selected="selected"': ''; ?> >พื้นที่เกิดความรุนแรง</option>
	</select>
	<br />
	<br />
	
	<?php echo $this->lang->line('pollution_mark_img'); ?>:<br />
	<input type="text" id="mark_img" name="mark_img" style="width: 400px; " readonly="readonly"
			value="<?php echo $mark_img; ?>" />
	<a href="#" class="button" id="mark_img_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
	<a href="#" id="clear_mark_img" ><?php echo $this->lang->line('header_clear'); ?></a><br />
	<?php echo $this->lang->line('pollution_image_upload'); ?><br />
	<img id="pollution_mark_img" src="<?php echo site_url( 'userfiles/' .$mark_img ); ?>" 
	        style="max-width: 200px; max-height: 200px; " alt="" />
	<br />
	<br />
	
	<?php echo $this->lang->line('pollution_coordinate'); ?>:<br />
	<div id="thai_map" ></div>
	<?php echo $this->lang->line('pollution_x'); ?> <input type="text" name="x" value="<?php echo $x; ?>" style="width: 150px; " readonly="readonly" />
	<?php echo $this->lang->line('pollution_y'); ?> <input type="text" name="y" value="<?php echo $y; ?>" style="width: 150px; " readonly="readonly" />
	<br />
	<br />
	
	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $pollution_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
    <input type="hidden" name="save_type" value=""/>	
</form>
