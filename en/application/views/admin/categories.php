<?php
switch( $category_id ) {
case 1:
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_about'); ?></h1>
<?php
	break;
case 2:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_rights'); ?></h1>
<?php
	break;
case 3:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_monitoring'); ?></h1>
<?php
	break;
case 4:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_waste'); ?></h1>
<?php
	break;
case 5:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_mabtapud'); ?></h1>
<?php
	break;
case 6:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_paint'); ?></h1>
<?php
	break;
case 7:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_data'); ?></h1>
<?php
	break;
case 8:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_policy'); ?></h1>
<?php
	break;
case 13:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_business'); ?></h1>
<?php
	break;
default:
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_update_category'); ?></h1>
<?php
}
?>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {	

	$("#sortable").sortable({
		placeholder: "ui-state-highlight",
		axis: "y",
		cursor: "pointer",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_cat_link = new Array();
			var a_order = new Array();
			$("#sortable li").each( function(i) {
				a_cat_link.push(  $(this).data('cat-link-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { cat_link: a_cat_link, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/category" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});

	$('a#update_info_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = "<?php echo site_url( "admin/menu/update_category/{$category_id}" ); ?>";
	});

	$('li a.button').on( 'click', function(e) {
		e.preventDefault();
		var cat_link_id = $(this).closest('li').data('cat-link-id');
		confirm_dialog( cat_link_id );
	});
});

function confirm_dialog( cat_link_id ) {
	$('<div></div>').appendTo('body')
			.html( 'Do you want to remove this item?' )
			.dialog({
				modal: true, 
				title: 'Remove item', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						location.href = "<?php echo site_url( "admin/menu/remove_cat_link" ); ?>/" +cat_link_id;
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}

</script>

<a class="button" id="update_info_button" href="#" ><?php echo $this->lang->line('category_update_info'); ?></a><br />
<span id="message" class="result" ></span><br />

<?php echo $this->lang->line('category_item_no'); ?>: <?php echo sizeof( $cat_links ); ?><br />

<ul id="sortable" >
<?php

$obj = null;

$cms_article = $this->cms_model->get_cms_article();
$cms_document = $this->cms_model->get_cms_document();
$cms_gallery = $this->cms_model->get_cms_gallery();
$cms_multimedia = $this->cms_model->get_cms_multimedia();
foreach( $cat_links as $cat_link ) {

	$cat_link_id = $cat_link->get_cat_link_id();
	$table = $cat_link->get_table();
	$table_id = $cat_link->get_table_id();
	$enable = $cat_link->get_enable();
	$order = $cat_link->get_order();
	switch( $table ) {
	case 'article':
		$obj = $cms_article->get_article( $table_id );
		$url = "admin/menu/update_{$table}/{$table_id}#list_item";
		break;
	case 'document':
		$obj = $cms_document->get_document( $table_id );
		$url = "admin/menu/update_{$table}/{$table_id}#list_item";
		break;
	case 'gallery':
		$obj = $cms_gallery->get_gallery( $table_id );
		$url = "admin/menu/update_{$table}/{$table_id}#list_item";
		break;
	case 'multimedia':
		$obj = $cms_multimedia->get_multimedia( $table_id );
		$url = "admin/left_menu/update_{$table}/{$table_id}#list_item";
		break;
	}
?>
	<li class="ui-state-default" data-cat-link-id="<?php echo $cat_link_id; ?>" data-order="<?php echo $order; ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<a class="title" href="<?php echo site_url( $url ); ?>" ><?php echo $obj->get_title(); ?></a>
		<div class="right_align pad" >
			<a href="#" class="button" ><?php echo $this->lang->line('header_remove'); ?></a>
		</div>
	</li>
<?php
}
?>
</ul>
<br />
