<?php
switch( $category_id ) {
case 1:
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_about'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 2:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_rights'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 3:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_monitoring'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 4:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_waste'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 5:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_mabtapud'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 6:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_paint'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 7:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_data'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 8:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_policy'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
case 13:
?>
<h1><?php echo $this->lang->line('menu_leftmenu'); ?> > <?php echo $this->lang->line('menu_business'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
	break;
default:
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_update_category'); ?> > 
		<?php echo $this->lang->line('category_update_category'); ?></h1>
<?php
}

$title = htmlspecialchars( $category->get_title() );
$description = $category->get_description();
?>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );

	var editor2 = CKEDITOR.replace( 'editor2', {
		bodyClass: 'article_description',
		height: 1000
	});
	
	load_images( 'category', <?php echo $category_id; ?>, '<?php echo site_url(); ?>' );
	
	$('a#upload_button').on( 'click', function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				load_images( 'category', <?php echo $category_id; ?>, '<?php echo site_url(); ?>' );
			},
			startupPath : "Images:/<?php echo substr( $category_path, 7 ); ?>"
		});
	});

	$( "#sortable_box" ).sortable({
		placeholder: "ui-state-highlight",
		containment: "div#content",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_image = new Array();
			var a_order = new Array();
			$("#sortable_box li").each( function(i) {
				a_image.push(  $(this).data('image-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { image: a_image, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/image" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete.' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});
	
	dialog = $( "#dialog_form" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Update: function() {
			
				$.ajax({
					data: { 
						image_id: $("#image_id").val(), 
						name: $("#name").val(), 
						description: $("#description").val()
					},
					type: 'POST',
					url: "<?php echo site_url( "ajax/image/update" ); ?>"
				})
				.always( function( data ) {
				
					load_images( 'category', <?php echo $category_id; ?>, '<?php echo site_url(); ?>' );
					$('#message').html( 'Image info updated.' )
							.show()
							.delay( 5000 )
							.fadeOut();
				});
				dialog.dialog( "close" );
			},
			Cancel: function() {
				dialog.dialog( "close" );
			}
		},
	});
	
	$('#sortable_box').on( 'click', 'span.box', function() {
	
		var li = $(this).parent();
		$("#image_id").val( li.data("image-id") );
		$("#name").val( li.find("span.name").html() );
		$("#description").val( li.find("span.description").html() );
		dialog.dialog( "open" );
	});
	
	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form[name=category_form]').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});

function load_images( type, id, base_url ) {
	$.getJSON( base_url +'ajax/gallery_image/' +type +'/' +id, function( images ) {
		
		var html = '';
		$.each( images, function( i, image ) {
			html += "<li class=\"ui-state-default\" data-image-id=\"" +image._image_id +"\" data-order=\"" +image._order +"\" title=\"" +image._path +"\" >\n"
					+"	<span class=\"ui-icon ui-icon-arrow-4\"></span>\n"
					+"	<div>\n"
					+"		<img src=\"" +base_url +"userfiles/_thumbs/<?php echo $category_path; ?>" +image._path +"\" alt=\"\" />\n"
					+"	</div>\n"
					+"	<span class=\"box name\" title=\"" +image._name +"\" >" +image._name +"</span>\n"
					+"	<span class=\"box description\" title=\"" +image._description +"\" >" +image._description +"</span>\n"
					+"</li>\n";
		});
		
		$("#sortable_box").html( html );
		$("#image_no").html( images.length );
	});
}

</script>

<form method="post" name="category_form" >
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
	<?php echo $this->lang->line('category_title'); ?><br />
	<input type="text" class="title" name="title" readonly="readonly" value="<?php echo $title; ?>" />
	<br />
	<br />
	
	<?php echo $this->lang->line('category_images'); ?><br />
	<a href="#" class="button" id="upload_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
	<span id="message" class="result" ></span><br />
	Please upload images into folder <b><?php echo $category_path; ?></b> <br />

	No. of photos: <span id="image_no" ></span>
	<ul id="sortable_box" ></ul>
	<br />

	<?php echo $this->lang->line('category_description'); ?><br />
	<textarea class="ckeditor" name="editor2" >
		<?php echo $description; ?>
	</textarea>
	<br />


	<?php echo $this->lang->line('category_tags'); ?><br />
	<input type="text" name="tags" value="<?php echo $tags; ?>" /><br />
	<br />
				
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
	
</form>

<div id="dialog_form" title="Update photo info" >
	<form>
		<fieldset>
			<label for="name">Name</label><br />
			<input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"><br />
			<br />
			<label for="description">Description</label><br />
			<textarea name="description" id="description" class="textarea ui-widget-content ui-corner-all"></textarea><br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="image_id" id="image_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>

