<?php
if( empty( $link ) ) {
?>
<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_link'); ?>
	> <?php echo $this->lang->line('menu_add_link'); ?></h1>
<?php
}
else {
?>
<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_link'); ?>
	> <?php echo $this->lang->line('menu_update_link'); ?></h1>
<?php
}
	
$link_name = ( empty( $link ) )? '': htmlspecialchars( $link->get_link_name() );
$link_url = ( empty( $link ) )? '': $link->get_link_url();
?>

<script type="text/javascript" >
$( function() {

	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});
</script>

<form method="post" name="link_form" >
	<?php echo $this->lang->line('link_title'); ?><br />
	<input type="text" name="link_name" value="<?php echo $link_name; ?>" />
	<br />
	<br />

	<?php echo $this->lang->line('link_link'); ?><br />
	<input type="text" name="link_url" value="<?php echo $link_url; ?>" />
	<br />
	<br />
	
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
	
</form>
