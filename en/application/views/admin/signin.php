<h1><?php echo $this->lang->line('menu_signin'); ?></h1>

<script src="<?php echo site_url( 'js/jquery.validate.min.js' ); ?>"></script>
<script type="text/javascript" >
$( function() {

	$('form').validate();
	
	$(document).keypress( function(e) {
		if(e.which == 13) {	// press enter
			$('form').submit();
		}
	});
	
	$('a[name=signin]').click( function(e) {
		e.preventDefault();
		$('form').submit();
	});
});
</script>

<form method="post" action="" >

<?php
if( !empty( $error ) ) {
?>
	<span class="red_text" ><?php echo $error; ?></span><br />
<?php
}
?>

	<?php echo $this->lang->line('profiles_email'); ?>:<br />
	<input type="text" id="name" name="email" class="required" style="width: 250px; " value="<?php echo ( empty( $email ) )? '': $email; ?>"  /><br />

	<?php echo $this->lang->line('profiles_password'); ?>:<br />
	<input type="password" id="password" name="password" class="required" style="width: 200px; " /><br />
	<br />

	<a href="#" class="button" name="signin" ><?php echo $this->lang->line('menu_signin'); ?></a>
</form>