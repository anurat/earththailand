<div class="all_op" >
    <a href="#" >check all</a>/<a href="#" >uncheck all</a>
    <select name="all_op" >
        <option value="0" >-</option>
        <option value="move" ><?php echo $this->lang->line('header_move'); ?></option>
    </select>
</div>
<div class="pagination" >
	<span class="per_page" >
		No. of items
		<select name="per_page" >
			<option value="10" <?php echo ( $page['per_page'] == 10 )? 'selected="selected"': ''; ?> >10</option>
			<option value="25" <?php echo ( $page['per_page'] == 25 )? 'selected="selected"': ''; ?> >25</option>
			<option value="50" <?php echo ( $page['per_page'] == 50 )? 'selected="selected"': ''; ?> >50</option>
			<option value="100" <?php echo ( $page['per_page'] == 100 )? 'selected="selected"': ''; ?> >100</option>
			<option value="100000" <?php echo ( $page['per_page'] == 100000 )? 'selected="selected"': ''; ?> >All</option>
		</select>
	</span>
	<?php echo $page['links']; ?>
	<span class="summary" >
		Displaying <?php echo $page['index1']; ?> - <?php echo $page['index2']; ?> 
		of <?php echo $page['count']; ?>
	</span>
</div>
