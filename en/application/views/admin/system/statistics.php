<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_statistics'); ?></h1>

<h2>Visits over time</h2>
<div id="widgetIframe"><iframe width="100%" height="350" src="http://earththailand.org/th/piwik/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=4&period=range&date=last30&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>

<h2>Visits overview</h2>
<div id="widgetIframe2"><iframe width="100%" height="500" src="http://earththailand.org/th/piwik/index.php?module=Widgetize&action=iframe&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=4&period=range&date=last30&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>

<h2>Pages</h2>
<div id="widgetIframe3"><iframe width="100%" height="800" src="http://earththailand.org/th/piwik/index.php?module=Widgetize&action=iframe&widget=1&moduleToWidgetize=Actions&actionToWidgetize=getPageUrls&idSite=4&period=range&date=last30&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>