<script src="<?php echo site_url( 'js/jquery.validate.min.js' ); ?>"></script>

<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_profiles'); ?> 
> <?php echo $this->lang->line('menu_change_password' ); ?></h1>

<script type="text/javascript" >
$( function() {
    $('form').validate({
        rules: {
            old_password: {
                minlength: 5
            },
            new_password: {
                minlength: 5
            },
            retype_password: {
                minlength: 5,
                equalTo: "#new_password"
            }
        }
    });

	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});
</script>

<form method="post" >

	<span class="red_text" >*</span><?php echo $this->lang->line('profiles_old_password'); ?>
	<input type="password" id="old_password" name="old_password" value="" required="required" style="width: 400px; " />
	<br />
	<br />

	<span class="red_text" >*</span><?php echo $this->lang->line('profiles_new_password'); ?>
	<input type="password" id="new_password" name="new_password" value=""  required="required" style="width: 400px; " />
	<br />
	<br />

	<span class="red_text" >*</span><?php echo $this->lang->line('profiles_retype_password'); ?>
	<input type="password" id="retype_password" name="retype_password" value=""  required="required" style="width: 400px; " />
	<br />
	<br />

	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
</form>