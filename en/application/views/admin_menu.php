<div id="menu_header" >
	<a href="<?php echo site_url( 'admin/dashboard' ); ?>" ><img src="<?php echo site_url( '/images/logo.jpg' ); ?>" alt="logo" /></a>
</div>
<?php
if( !empty( $userid ) ) {
?>
<script type="text/javascript" >
$( function() {
	var content = "<?php echo str_replace( "/", "_", $content ); ?>";
	$("#" +content ).addClass( "active_menu" );
});
</script>
<ul>
	<li><span><?php echo $this->lang->line('menu_search'); ?></span>
		<ul>
			<li><a href="<?php echo site_url( 'admin/search' ); ?>" id="admin_search_link" ><?php echo $this->lang->line('menu_search'); ?></a></li>
		</ul>
	</li>
	<li><span><?php echo $this->lang->line('menu_homepage'); ?></span>
		<ul>
			<li><a href="<?php echo site_url( 'admin/homepage/header_image' ); ?>" id="admin_homepage_header_image" ><?php echo $this->lang->line('menu_header_image'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/link' ); ?>" id="admin_homepage_link" ><?php echo $this->lang->line('menu_link'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/slideshow' ); ?>" id="admin_homepage_slideshow" ><?php echo $this->lang->line('menu_slideshow'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/news_slideshow' ); ?>" id="admin_homepage_news_slideshow" ><?php echo $this->lang->line('menu_news_slideshow'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/documents' ); ?>" id="admin_homepage_documents" ><?php echo $this->lang->line('menu_documents'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/articles' ); ?>" id="admin_homepage_articles" ><?php echo $this->lang->line('menu_articles'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/tags' ); ?>" id="admin_homepage_tags" ><?php echo $this->lang->line('menu_tags'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/homepage/footer' ); ?>" id="admin_homepage_footer" ><?php echo $this->lang->line('menu_footer'); ?></a></li>
		</ul>
	</li>
	<li><span><?php echo $this->lang->line('menu_topmenu'); ?></span>
		<ul>
			<li><a href="<?php echo site_url( 'admin/menu/pollution_map' ); ?>" id="admin_menu_pollution_map" ><?php echo $this->lang->line('menu_pollution_map'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/menu/articles' ); ?>" id="admin_menu_articles" ><?php echo $this->lang->line('menu_top_articles'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/menu/documents' ); ?>" id="admin_menu_documents" ><?php echo $this->lang->line('menu_top_documents'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/menu/gallery' ); ?>" id="admin_menu_gallery" ><?php echo $this->lang->line('menu_gallery'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/menu/calendar' ); ?>" id="admin_menu_calendar" ><?php echo $this->lang->line('menu_calendar'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/menu/about' ); ?>" id="admin_menu_about" ><?php echo $this->lang->line('menu_about'); ?></a></li>
		</ul>
	</li>
	<li><span><?php echo $this->lang->line('menu_leftmenu'); ?></span>
		<ul>
			<li><a href="<?php echo site_url( 'admin/left_menu/rights' ); ?>" id="admin_left_menu_rights" ><?php echo $this->lang->line('menu_rights'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/monitoring' ); ?>" id="admin_left_menu_monitoring" ><?php echo $this->lang->line('menu_monitoring'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/sub_grant' ); ?>" id="admin_left_menu_sub_grant" ><?php echo $this->lang->line('menu_sub_grant'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/waste' ); ?>" id="admin_left_menu_waste" ><?php echo $this->lang->line('menu_waste'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/mabtapud' ); ?>" id="admin_left_menu_mabtapud" ><?php echo $this->lang->line('menu_mabtapud'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/paint' ); ?>" id="admin_left_menu_paint" ><?php echo $this->lang->line('menu_paint'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/business' ); ?>" id="admin_left_menu_business" ><?php echo $this->lang->line('menu_business'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/policy' ); ?>" id="admin_left_menu_policy" ><?php echo $this->lang->line('menu_policy'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/left_menu/multimedia' ); ?>" id="admin_left_menu_multimedia" ><?php echo $this->lang->line('menu_multimedia'); ?></a></li>
		</ul>
	</li>
	<li><span><?php echo $this->lang->line('menu_system'); ?></span>
		<ul>
			<li><a href="<?php echo site_url( 'admin/system/statistics' ); ?>" id="admin_system_statistics" ><?php echo $this->lang->line('menu_statistics'); ?></a></li>
			<li><a href="<?php echo site_url( 'admin/system/profiles' ); ?>" id="admin_system_profiles" ><?php echo $this->lang->line('menu_profiles'); ?></a></li>
<?php
	if( $role == Cms_user::ADMIN_ROLE ) {
?>
			<li><a href="<?php echo site_url( 'admin/system/users' ); ?>" id="admin_system_users" ><?php echo $this->lang->line('menu_user'); ?></a></li>
<?php
	}
?>
		</ul>
	</li>
</ul>
<?php
}
else {
?>
<ul>
	<li><span><?php echo $this->lang->line('menu_admin'); ?></span>
		<ul>
			<li><a href="<?php echo site_url( 'admin/signin' ); ?>"><?php echo $this->lang->line('menu_signin'); ?></a></li>
		</ul>
	</li>
</ul>
<?php
}
?>
<div id="menu_footer" >
	<?php echo $this->lang->line('menu_customer'); ?><br />
	<?php echo $this->lang->line('menu_phone'); ?> 091 139 4600, 091 138 3567<br />
	<?php echo $this->lang->line('menu_email'); ?> admin@connectiv.co.th<br />
</div>