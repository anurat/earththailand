<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	
	<link href="<?php echo site_url( "images/favicon.ico" ); ?>" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<?php $style_css = "css/style.css?ver=".time(); ?>
	<link rel="stylesheet" href="<?php echo site_url( "js/jquery.mobile-1.4.2/jquery.mobile-1.4.2.css" ); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo site_url( $style_css ); ?>" type="text/css" />
	
	<script src="<?php echo site_url( "js/jquery-1.11.1.min.js" ); ?> "></script>
	<script src="<?php echo site_url( "js/jquery.mobile-1.4.2/jquery.mobile-1.4.2.min.js" ); ?>"></script>
</head>
<body>
	<div data-role="page" data-dom-cache="false" >
		<div data-role="content" >
			<header><?php include 'header.php'; ?></header>
			<?php include 'menu.php'; ?>
			<div id="content"><?php include "{$content}.php"; ?></div>
            <div id="beforefooter" ><?php include 'beforefooter.php'; ?></div>
			<footer>
				<div><?php include 'footer.php'; ?></div>
			</footer>
		</div>
	</div>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//earththailand.org/th/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 4]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//earththailand.org/th/piwik/piwik.php?idsite=4" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
</body>
</html>