<div id="article" >
	<div>
		<h1>Publications</h1>
		<br />

		<div class="items" >
<?php
foreach( $document_list as $document ) {
	$document_id = $document->get_document_id();
	$list_img = $document->get_list_img();
?>
			<div>
<?php
	if( !empty( $list_img ) ) {
?>
				<a href="<?php echo site_url( "document/" .$document_id ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$document->get_list_img() ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "document/" .$document_id ); ?>" 
						target="_blank" ><?php echo $document->get_list_title(); ?></a></h4>
				<div class="text" ><?php echo $document->get_list_desc(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "document/" .$document_id ); ?>" 
						target="_blank" >read more...</a></span>
			</div>
<?php
}
?>
		</div>

		<div class="pagination" >
			<?php echo $page['links']; ?>
			<span class="summary" >
				Showing item <?php echo $page['index1']; ?> - <?php echo $page['index2']; ?><br />
				from <?php echo $page['count']; ?> items
			</span>
		</div>

<?php
include "social_media.php";
?>
	</div>
</div>