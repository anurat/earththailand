<section id="homepage1">
	<table>
	<tr>
		<td>
			<span>&nbsp;</span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<br />
			<span></span>
			<span></span>
			<span></span>
		</td>
		<td class="leftmenu">
			<span><a href="<?php echo site_url( "earth1" ); ?>" target="_blank">PRTR & Community Right-to-Know</a></span><br />
			<span><a href="<?php echo site_url( "earth2" ); ?>" target="_blank">Communities in Action</a></span><br />
			<!--<span><a href="<?php echo site_url( "earth8" ); ?>" target="_blank">Sub grant</a></span><br />-->
			<span><a href="<?php echo site_url( "earth3" ); ?>" target="_blank">Industrial & Hazardous Waste Mangement</a></span><br />
			<span><a href="<?php echo site_url( "earth4" ); ?>" target="_blank">Map Ta Phut Studies</a></span><br />
			<span><a href="<?php echo site_url( "earth5" ); ?>" target="_blank">Chemicals & Product Life-Cycle Management</a></span><br />
			<span><a href="<?php echo site_url( "pollution_type" ); ?>" target="_blank">Pollution Hotspots</a></span><br />
			<span><a href="<?php echo site_url( "earth7" ); ?>" target="_blank">Corporate Accountability</a></span><br />
			<span><a href="<?php echo site_url( "earth6" ); ?>" target="_blank">Policy Reference</a></span><br />
			<span><a href="<?php echo site_url( "multimedia_list" ); ?>" target="_blank">Multimedia</a></span><br />
			<span><a href="<?php echo site_url( "events" ); ?>" target="_blank">Calendar</a></span><br />
			<br /> 
			<span>
				<select name="thai_link" data-mini="true" data-role="none">
					<option value="">Thailand Network</option>
<?php
foreach( $thai_link_list as $link ) {
?>
					<option value="<?php echo $link->get_link_url(); ?>"><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
				</select>
			</span><br />
			<span>
				<select name="regional_link" data-mini="true" data-role="none">
					<option value="">Regional Network</option>
<?php
foreach( $regional_link_list as $link ) {
?>
					<option value="<?php echo $link->get_link_url(); ?>"><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
				</select>
			</span><br />
			<span>
				<select name="global_link" data-mini="true" data-role="none">
					<option value="">Global Network</option>
<?php
foreach( $global_link_list as $link ) {
?>
					<option value="<?php echo $link->get_link_url(); ?>"><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
				</select>
			</span>
			<script type="text/javascript">
			$( function() {
				$("select").on("change", function() {
					var url = $(this).find("option:selected").val();
					if( url ) {
						window.open( url, '_blank' );
					}
				});
			});
			</script>
		</td>
		<td class="slideshow">


			<link rel='stylesheet' href="<?php echo site_url( "css/slideshow/owl.carousel.css" ); ?>" type='text/css' media='all' />
			<link rel='stylesheet' href="<?php echo site_url( "css/slideshow/owl.theme.css" ); ?>" type='text/css' media='all' />
			<link rel='stylesheet' href="<?php echo site_url( "css/slideshow/owl.transitions.css" ); ?>" type='text/css' media='all' />
			<script src="<?php echo site_url( "js/slideshow/owl.carousel.min.js" ); ?>"></script>
			<div id="owl-slideshow" class="owl-carousel owl-theme">
<?php
foreach( $slideshow_list as $slideshow ) {
?>
				<div class="item">
					<a href="<?php echo $slideshow->get_description(); ?>" target="_blank"
							><img src="<?php echo site_url( "userfiles/Slideshows/1/" .$slideshow->get_path() ); ?>"
							alt=""></a>
					<div class="owl_caption">
						<a href="<?php echo $slideshow->get_description(); ?>"><?php echo $slideshow->get_name(); ?></a>
					</div>
				</div>
<?php
}
?>
			</div>
			<script>
			$( function() {
				$("#owl-slideshow").owlCarousel({
					navigation : true, // Show next and prev buttons
					slideSpeed : 300,
					paginationSpeed : 400,
					singleItem:true,
					autoPlay: true,
					stopOnHover: true,
					navigation: true,
					navigationText : ["<",">"]

				});
			});
			</script>
		</td>
		<td>&nbsp;</td>
	</tr>
	</table>
</section>
<section id="homepage2">
	<div>
		<!-- news slideshow -->
		<?php include 'homepage/news_slideshow.php'; ?>
		<!-- pollution map/ event calendar -->
		<?php include 'homepage/pollution.php'; ?>
		<!-- documents/articles -->
		<?php include 'homepage/documents_articles.php'; ?>
		<!-- social media share -->
		<br />
		<?php include 'social_media.php'; ?>
		<!-- counter -->
		<?php include 'homepage/counter.php'; ?>
	</div>
</section>