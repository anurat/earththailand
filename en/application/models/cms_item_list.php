<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS item list class
 * 
 * a class for managing item list in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 11/02/14
 *
 */
class Cms_item_list {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of item lists
	 *
	 * @return ItemList object array
	 */
	public function get_item_list_list() {
		return ItemListQuery::create()
				->orderBy_order()
				->find();
	}
	
	/**
	 * Get the number of item lists
	 *
	 * @return int
	 */
	public function get_item_list_no() {
		return ItemListQuery::create()->count();
	}

	/**
	 * Get a item_list
	 * 
	 * @param int $item_list_id
	 *
	 * @return ItemList object
	 */
	public function get_item_list( $item_list_id ) {
		return ItemListQuery::create()->findPk( $item_list_id );
	}
	
	/**
	 * Get item_lists in each table
	 *
	 * @param string $table
     * @param string $type
	 *
	 * @return ItemList object array
	 */
	public function get_item_lists( $table, $type="enabled" ) {

        $item_lists = ItemListQuery::create()
                ->filterBy_table( $table )
                ->_if( $type == "enabled" )
                    ->filterBy_enable( 1 )
                ->_endif()
                ->orderBy_order()
                ->find();
		return $item_lists;
	}
	
	/**
	 * Get the number of item_lists in each table
	 *
	 * @param string $table
     * @param string $type
	 *
	 * @return ItemList object array
	 */
	public function get_item_lists_no( $table, $type="enabled" ) {

        return ItemListQuery::create()
                ->filterBy_table( $table )
                ->_if( $type == "enabled" )
                    ->filterBy_enable( 1 )
                ->_endif()
                ->orderBy_order()
                ->count();
	}
    
	/**
	 * Get a item_list by table ID
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return ItemList object
	 */
	public function get_item_list_by_table( $table, $table_id ) {
		return ItemListQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->findOne();
	}
	
	/**
	 * Check whether item table already exists
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return boolean
	 */
	public function has_item_list( $table, $table_id ) {
		return ( ItemListQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->count() > 0 );
	}
	
	/**
	 * Get maximum number of item_list order from 
	 *
	 * @param int $gallery_id
	 *
	 * @return int
	 */
	public function get_max_order( $table ) {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( it.order )
				FROM item_list it
				WHERE it.table = '{$table}'";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * increment all table order by one
	 *
	 * @param string $table
	 *
	 */
	private function increment_order( $table ) {
		$item_lists = $this->get_item_lists( $table );
		foreach( $item_lists as $item_list ) {
			$item_list->set_order( $item_list->get_order() +1 );
			$item_list->save();
		}
	}
			
	/**
	 * Add new item_list
	 *
	 * @param string $table
	 * @param int $table_id
	 * 
	 * @return ItemList object
	 */
	public function add_item_list( $table, $table_id ) {
	
		$item_list = new ItemList();
		$item_list->set_table( $table );
		$item_list->set_table_id( $table_id );
		$item_list->set_order( $this->get_max_order( $table ) +1 );
		$item_list->set_lock( 0 );
		$item_list->set_enable( 1 );
		$item_list->save();
		
		return $item_list;
	}
	
	/**
	 * Update item_list
	 *
	 * @param int $item_list_id
	 * @param string $table
	 * @param int $table_id
	 * @param int $order
	 * @param int $lock
	 * @param int $enable
	 * 
	 * @return ItemList object
	 */
	public function update_item_list( $item_list_id, $table, $table_id, $order, $lock, $enable ) {
	
		$item_list = ItemListQuery::create()->findPk( $item_list_id );
		$item_list->set_table( $table );
		$item_list->set_table_id( $table_id );
		$item_list->set_order( $order );
		$item_list->set_lock( $lock );
		$item_list->set_enable( $enable );
		$item_list->save();
		
		return $item_list;
	}
	
	/**
	 * Lock an item list
	 *
	 * @param string $table
     * @param int $table_id
	 *
	 */
	public function lock( $table, $table_id ) {
	
		$item_list = $this->get_item_list_by_table( $table, $table_id );
		$item_list->set_lock( 1 );
		$item_list->save();
	}
	
	/**
	 * Unlock an item list
	 *
	 * @param string $table
     * @param int $table_id
	 *
	 */
	public function unlock( $table, $table_id ) {
	
		$item_list = $this->get_item_list_by_table( $table, $table_id );
		$item_list->set_lock( 0 );
		$item_list->save();
	}
	
	/**
	 * Enable an item list
	 *
	 * @param string $table
     * @param int $table_id
	 *
	 */
	public function enable( $table, $table_id ) {
	
		$item_list = $this->get_item_list_by_table( $table, $table_id );
		$item_list->set_enable( 1 );
		$item_list->save();
	}
	
	/**
	 * Disable an item list
	 *
	 * @param string $table
     * @param int $table_id
	 *
	 */
	public function disable( $table, $table_id ) {
	
		$item_list = $this->get_item_list_by_table( $table, $table_id );
		$item_list->set_enable( 0 );
		$item_list->save();
	}
    
    /**
     * Get the current position of an item list
     * order descendingly
     *
     * @param ItemList object $item_list
     *
     * @return int
     */
    protected function get_current_position( $item_list ) {
        
        $table = $item_list->get_table();
        $order = $item_list->get_order();
        return ItemListQuery::create()
                ->filterBy_table( $table )
                ->filterBy_order( array( 'min' => $order ) )
                ->count();
    }
    
	/**
	 * Reorder items by new order
	 *
     * @param string $table
	 * @param int $table_id
	 * @param int $new_pos
	 *
	 */
	public function reorder( $table, $table_id, $new_pos ) {
	
        $item_list = $this->get_item_list_by_table( $table, $table_id );
        $cur_pos = $this->get_current_position( $item_list );
        
        $reverse = ( $cur_pos > $new_pos );
        $window = abs( $cur_pos -$new_pos ) +1;
        $ils = ItemListQuery::create()
                ->filterBy_table( $table )
                ->orderBy_order( 'DESC' )
                ->offset( ( $reverse )? $new_pos -1: $cur_pos -1 )
                ->limit( $window )
                ->find();
        
        $window = min( $window, sizeof( $ils ) );
        
		$il1 = $ils[0];
		$il2 = $ils[$window -1];
		$min_order = min( $il1->get_order(), $il2->get_order() );
		$max_order = max( $il1->get_order(), $il2->get_order() );
		
		$i = 0;
		foreach( $ils as $il ) {
			if( $reverse ) {
				if( $il == $il2 ) {
					$il->set_order( $max_order );
					$il->save();
				}
				else if( $il == $il1 ) {
					$il->set_order( $max_order -1 );
					$il->save();
				}
				else {
					$il->set_order( $max_order -$i -1 );
					$il->save();
				}
			}
			else {
				if( $il == $il1 ) {
					$il->set_order( $min_order );
					$il->save();
				}
				else {
					$il->set_order( $max_order -$i +1 );
					$il->save();
				}
			}
			$i++;
		}
	}
    
    /**
	 * Reorder items by new order
	 *
     * @param string $table
	 * @param string $mm_table_id
	 * @param int $new_pos
	 *
     * @return int
	 */
    public function multi_reorder( $table, $mm_table_id, $new_pos ) {
        
        $total_no = $this->get_item_lists_no( $table, "all" );
        $table_ids = explode( ',', $mm_table_id );
        $reorder_no = sizeof( $table_ids );
        
        // invalid new position
        if( $new_pos < 1 || $new_pos > $total_no ) {
            return 1;
        }
    
        // populate table_ids
        $reorders = ItemListQuery::create()
                ->filterBy_table( $table )
                ->filterBy_table_id( $table_ids )
                ->orderBy_order( 'DESC' )
                ->find();
            
        // populate the rest
		$con = Propel::getConnection();
		$sql = "SELECT *
				FROM item_list il
				WHERE il.table = '{$table}'
				AND il.table_id NOT IN ( {$mm_table_id} )
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$rests = ItemListPeer::populateObjects( $stmt );

        $new_pos = ( $new_pos > $total_no -$reorder_no )? $total_no -$reorder_no +1: $new_pos;
        
        // reorders are in the middle of the list,
        // position the rest, reorders then the rest
        $i = 0;
        echo "reorders\n";
        foreach( $reorders as $reorder ) {
            $reorder->set_order( $total_no -$new_pos -$i +1 );
            $reorder->save();
            echo ( $total_no -$new_pos -$i +1 ) ."\n";
            $i++;
        }

        $i = 0;
        $j = 0;
        echo "rests\n";
        foreach( $rests as $rest ) {
            if( $i +1 < $new_pos ) {
                $rest->set_order( $total_no -$i );
                $rest->save();
                echo ( $total_no -$i ) ."\n";
                $i++;
            }
            else {
                $rest->set_order( $total_no -$new_pos +1 -$reorder_no -$j );
                $rest->save();
                echo ( $total_no -$new_pos +1 -$reorder_no -$j ) ."\n";
                $j++;
            }
        }
        
        return 0;
    }
	
	/**
	 * Update item_list orders
	 *
	 * @param array $id_list
	 * @param array $order
	 *
	 */
	public function update_orders( $table, $id_list, $order ) {
	
		$max_order = max( $order );
		foreach( $id_list as $i => $id ) {
			$item_list = $this->get_item_list_by_table( $table, $id );
			$item_list->set_order( $max_order -$i );
			$item_list->save();
		}
	}
		
	/**
	 * Delete item_list
	 *
	 * @param int $item_list_id
	 *
	 */
	public function delete_item_list( $item_list_id ) {
		ItemListQuery::create()->findPk( $item_list_id )->delete();
	}
	
	/**
	 * Delete item_list by table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	public function delete_item_list_by_table( $table, $table_id ) {
		ItemListQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->delete();
	}
}

?>