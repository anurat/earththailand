<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS homepage class
 * 
 * a class for managing homepage item in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/24/14
 *
 */
class Cms_homepage {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of homepages
	 *
	 * @return Homepage object array
	 */
	public function get_homepage_list() {
		return HomepageQuery::create()
				->orderBy_order()
				->find();
	}
	
	/**
	 * Get the number of homepages
	 *
	 * @return int
	 */
	public function get_homepage_no() {
		return HomepageQuery::create()->count();
	}

	/**
	 * Get a homepage uten
	 * 
	 * @param int $homepage_id
	 *
	 * @return Homepage object
	 */
	public function get_homepage( $homepage_id ) {
		return HomepageQuery::create()->findPk( $homepage_id );
	}
	
	/**
	 * Get homepages in an item table
	 *
	 * @param string $table
	 *
	 * @return Homepage object array
	 */
	public function get_homepages( $table ) {

		$con = Propel::getConnection();
		
		switch( $table ) {
		case 'article':
			$sql = "SELECT h.*
					FROM homepage h, item_list il
					WHERE h.table = 'article'
					AND h.table_id = il.table_id
                    AND il.table = h.table
					AND il.enable = 1
					ORDER BY h.order";
			$stmt = $con->prepare( $sql );
			$stmt->execute();
			$homepages = HomepagePeer::populateObjects( $stmt );
			break;
		case 'document':
			$sql = "SELECT h.*
					FROM homepage h, item_list il
					WHERE h.table = 'document'
					AND h.table_id = il.table_id
                    AND il.table = h.table
					AND il.enable = 1
					ORDER BY h.order";
			$stmt = $con->prepare( $sql );
			$stmt->execute();
			$homepages = HomepagePeer::populateObjects( $stmt );
			break;
        /*
		case 'gallery':
			$sql = "SELECT h.*
					FROM homepage h, item_list il
					WHERE h.table = 'gallery'
					AND h.table_id = il.table_id
                    AND il.table = h.table
					AND il.enable = 1
					ORDER BY h.order";
			$stmt = $con->prepare( $sql );
			$stmt->execute();
			$homepages = HomepagePeer::populateObjects( $stmt );
			break;
		case 'multimedia':
			$sql = "SELECT h.*
					FROM homepage h, item_list il
					WHERE h.table = 'multimedia'
					AND h.table_id = m.multimedia_id
                    AND il.table = 'multimedia'
                    AND m.multimedia_id = il.table_id
					AND il.enable = 1
					ORDER BY h.order";
			$stmt = $con->prepare( $sql );
			$stmt->execute();
			$homepages = HomepagePeer::populateObjects( $stmt );
			break;
        */
		default:
			$homepages = HomepageQuery::create()
					->filterBy_table( $table )
					->orderBy_order()
					->find();
		}
		
		return $homepages;
	}
	
	/**
	 * Get a homepage item in an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return Homepage object
	 */
	public function get_homepage_by_table( $table, $table_id ) {
		return HomepageQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->findOne();
	}
	
	/**
	 * Check whether item table already exists
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return boolean
	 */
	public function has_homepage( $table, $table_id ) {
		return ( HomepageQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->count() > 0 );
	}
	
	/**
	 * Get maximum number of homepage order from a gallery
	 *
	 * @param int $gallery_id
	 *
	 * @return int
	 */
	public function get_max_order( $table ) {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( h.order )
				FROM homepage h
				WHERE h.table = '{$table}'";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * increment all table order by one
	 *
	 * @param string $table
	 *
	 */
	private function increment_order( $table ) {
		$homepages = $this->get_homepages( $table );
		foreach( $homepages as $homepage ) {
			$homepage->set_order( $homepage->get_order() +1 );
			$homepage->save();
		}
	}
			
	/**
	 * Add new homepage
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param string $title
	 * @param string $description
	 * @param string $image
	 * @param string $link_name
	 * @param string $link_url
	 * 
	 * @return Homepage object
	 */
	public function add_homepage( $table, $table_id, $title, $description, $image, $link_name,
			$link_url ) {
	
		$homepage = new Homepage();
		$homepage->set_table( $table );
		$homepage->set_table_id( $table_id );
		$homepage->set_title( $title );
		$homepage->set_description( $description );
		$homepage->set_image( $image );
		$homepage->set_homepage_date( time() );
		$homepage->set_link_name( $link_name );
		$homepage->set_link_url( $link_url );
		$homepage->set_order( 0 );
		$homepage->save();
		
		$this->increment_order( $table );
		
		return $homepage;
	}
	
	/**
	 * Update homepage
	 *
	 * @param int $homepage_id
	 * @param string $table
	 * @param int $table_id
	 * @param string $title
	 * @param string $description
	 * @param string $image
	 * @param string $link_name
	 * @param string $link_url
	 * 
	 * @return Homepage object
	 */
	public function update_homepage( $homepage_id, $table, $table_id, $title, $description, $image, 
			$link_name, $link_url ) {
	
		$homepage = HomepageQuery::create()->findPk( $homepage_id );
		$homepage->set_table( $table );
		$homepage->set_table_id( $table_id );
		$homepage->set_title( $title );
		$homepage->set_description( $description );
		$homepage->set_image( $image );
		$homepage->set_link_name( $link_name );
		$homepage->set_link_url( $link_url );
		$homepage->save();
		
		return $homepage;
	}
	
	/**
	 * Update homepage orders
	 *
	 * @param array $homepage
	 * @param array $order
	 *
	 */
	public function update_orders( $homepage, $order ) {
	
		$min_order = min( $order );
		foreach( $homepage as $i => $homepage_id ) {
			$homepage = $this->get_homepage( $homepage_id );
			$homepage->set_order( $min_order +$i );
			$homepage->save();
		}
	}
		
	/**
	 * Delete homepage
	 *
	 * @param int $homepage_id
	 *
	 */
	public function delete_homepage( $homepage_id ) {
		HomepageQuery::create()->findPk( $homepage_id )->delete();
	}
	
	/**
	 * Delete homepage by table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	public function delete_homepage_by_table( $table, $table_id ) {
		HomepageQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->delete();
	}
}

?>