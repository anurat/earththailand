<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS multimedia class
 * 
 * a class for managing multimedia in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/13/14
 *
 */
class Cms_multimedia {

	private $cms_model;
	
	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * generate where clause
	 *
	 * @param string $query
	 *
	 * @return string
	 */
	private function _generate_query( $query ) {
	
		if( empty( $query ) ) {
			return '0';
		}
		$where = '';
		
		$qs = explode( ' ,', $query );
		foreach( $qs as $q ) {
			$where = "( title LIKE '%{$q}%' OR
					description1 LIKE '%{$q}%' OR
					description2 LIKE '%{$q}%' ) AND ";
		}
		
		$where = substr( $where, 0, strlen( $where ) -4 );
		return $where;
	}
	
	/**
	 * Search from query string
	 *
	 * @param string $query
	 *
	 * @return Multimedia object array
	 */
	public function search( $query ) {
	
		$where = $this->_generate_query( $query );
	
		$con = Propel::getConnection();
		$sql = "SELECT m.* 
				FROM multimedia m, item_list il
				WHERE il.table = 'multimedia'
				AND m.multimedia_id = il.table_id
				AND il.enable = 1
                AND {$where}
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return MultimediaPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get a list of multimedias
	 *
	 * @param string $display
	 *
	 * @return Multimedia object array
	 */
	public function get_multimedia_list( $display="all", $index=0, $no=10 ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT m.* 
				FROM multimedia m, item_list il
				WHERE il.table = 'multimedia'
				AND m.multimedia_id = il.table_id
				{$enabled}
				ORDER BY il.order DESC
                LIMIT {$no}
                OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return MultimediaPeer::populateObjects( $stmt );
	}
	
	/**
	 * Count the number of multimedias
	 *
	 * @param string $display
	 *
	 * @return int
	 */
	public function count_multimedia_list( $display='all' ) {
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*)
				FROM multimedia m, item_list il
				WHERE il.table = 'multimedia'
				AND m.multimedia_id = il.table_id
				{$enabled}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Get a list of multimedias with multimedia category only
	 *
	 * @return Multimedia object array
	 */
	public function get_multimedia_cat_list() {
	
		$this->cms_model->get_cms_category();
		$category_id = Cms_Category::MULTIMEDIA_CAT;
	
		$con = Propel::getConnection();
		$sql = "SELECT DISTINCT m.* 
				FROM multimedia m, cat_link cl
				WHERE cl.table = 'multimedia'
				AND m.multimedia_id = cl.table_id
				AND cl.category_id = '{$category_id}'
				AND m.enable = 1
				ORDER BY m.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return MultimediaPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get no. of multimedia
	 *
	 * @return int
	 */
	public function get_multimedia_no() {
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM multimedia m, item_list il
				WHERE il.table = 'multimedia'
				AND m.multimedia_id = il.table_id";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get a multimedia
	 * 
	 * @param int $multimedia_id
	 *
	 * @return Multimedia object
	 */
	public function get_multimedia( $multimedia_id ) {
		return MultimediaQuery::create()->findPk( $multimedia_id );
	}
	
	/**
	 * Get a multimedia
	 *
	 * @param string $title
	 *
	 * @return Multimedia object
	 */
	public function get_multimedia_by_title( $title ) {
		return MultimediaQuery::create()
				->filterBy_title( $title )
				->findOne();
	}
	
	/**
	 * Get the maximum number of order
	 *
	 * @return int
	 */
	public function get_max_order() {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( m.order )
				FROM multimedia m";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Enable multimedia
	 *
	 * @param int $multimedia_id
	 */
	public function enable( $multimedia_id ) {
		
		$multimedia = $this->get_multimedia( $multimedia_id );
		$multimedia->set_enable( 1 );
		$multimedia->save();
	}
	
	/**
	 * Disable multimedia
	 *
	 * @param int $multimedia_id
	 */
	public function disable( $multimedia_id ) {
		
		$multimedia = $this->get_multimedia( $multimedia_id );
		$multimedia->set_enable( 0 );
		$multimedia->save();
	}
	
	/**
	 * Search multimedia
	 *
	 * @param string $search
	 *
	 * @return Multimedia object array
	 */
	public function ajax_search( $search ) {
		return MultimediaQuery::create()
				->filterBy_title( "%{$search}%" )
				->limit( 10 )
				->find();
	}
	
	/**
	 * Reorder between two IDs
	 *
	 * @param int $id1
	 * @param int $id2
	 *
	 */
	public function reorder( $id1, $id2 ) {
	
		$m1 = $this->get_multimedia( $id1 );
		$m2 = $this->get_multimedia( $id2 );
		$min_order = min( $m1->get_order(), $m2->get_order() );
		$max_order = max( $m1->get_order(), $m2->get_order() );
		
		$ms = MultimediaQuery::create()
				->filterBy_order( array( 'min' => $min_order, 'max' => $max_order ) )
				->orderBy_order( 'DESC' )
				->find();
				
		$reverse = ( $m1->get_order() == $min_order );
		$i = 0;
		foreach( $ms as $m ) {
			if( $reverse ) {
				if( $m == $m2 ) {
				}
				else if( $m == $m1 ) {
					$m->set_order( $max_order -1 );
					$m->save();
				}
				else {
					$m->set_order( $max_order -$i -1 );
					$m->save();
				}
			}
			else {
				if( $m == $m1 ) {
					$m->set_order( $min_order );
					$m->save();
				}
				else {
					$m->set_order( $max_order -$i +1 );
					$m->save();
				}
			}
			$i++;
		}
	}
	
	/**
	 * Add new multimedia
	 *
	 * @param string $title
	 * @param string $description1
	 * @param string $description2
	 * @param string $video_code
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * 
	 * @return Multimedia object
	 */
	public function add_multimedia( $title, $description1, $description2, $video_code, $list_title, 
			$list_desc, $list_img ) {
	
		$multimedia = new Multimedia();
		$multimedia->set_title( $title );
		$multimedia->set_order( $this->get_max_order() +1 );
		$multimedia->set_description1( $description1 );
		$multimedia->set_description2( $description2 );
		$multimedia->set_video_code( $video_code );
		$multimedia->set_list_title( $list_title );
		$multimedia->set_list_desc( $list_desc );
		$multimedia->set_list_img( $list_img );
		$multimedia->set_enable( 1 );
		$multimedia->set_created_date( time() );
		$multimedia->save();
		
		return $multimedia;
	}
	
	/**
	 * Update multimedia
	 *
	 * @param int $multimedia_id
	 * @param string $title
	 * @param string $description1
	 * @param string $description2
	 * @param string $video_code
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * 
	 * @return Multimedia object
	 */
	public function update_multimedia( $multimedia_id, $title, $description1, $description2, 
			$video_code, $list_title, $list_desc, $list_img ) {
		
		$multimedia = MultimediaQuery::create()->findPk( $multimedia_id );
		$multimedia->set_title( $title );
		$multimedia->set_description1( $description1 );
		$multimedia->set_description2( $description2 );
		$multimedia->set_video_code( $video_code );
		$multimedia->set_list_title( $list_title );
		$multimedia->set_list_desc( $list_desc );
		$multimedia->set_list_img( $list_img );
		$multimedia->save();
		
		return $multimedia;
	}
	
	/**
	 * Update multimedia list order
	 *
	 * @param string $order
	 */
	public function update_orders( $order ) {
	
		$no = $this->get_multimedia_no();
	
		$a_order = explode( ',', $order );
		foreach( $a_order as $i => $multimedia_id ) {
			$multimedia = $this->get_multimedia( $multimedia_id );
			$multimedia->set_order( $no -$i );
			$multimedia->save();
		}
	
	}
		
	/**
	 * Delete multimedia
	 *
	 * @param int $multimedia_id
	 *
	 */
	public function delete_multimedia( $multimedia_id ) {
		MultimediaQuery::create()->findPk( $multimedia_id )->delete();
	}
}

?>