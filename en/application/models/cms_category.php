<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS category class
 * 
 * a class for managing category in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/21/14
 *
 */
class Cms_category {

	const MULTIMEDIA_CAT = 9;
	const GALLERY_CAT = 10;
	const DOCUMENT_CAT = 11;
	const ARTICLE_CAT = 12;

	private $cms_model;
	
	private $category_path = array(
		1 => 'Images/menu/about/',
		2 => 'Images/menu/earth1/',
		3 => 'Images/menu/earth2/',
		4 => 'Images/menu/earth3/',
		5 => 'Images/menu/earth4/',
		6 => 'Images/menu/earth5/',
		7 => 'Images/menu/earth-land/',
		8 => 'Images/menu/earth6/',
		13 => 'Images/menu/earth7/',
		14 => 'Images/menu/earth8/'
	);

	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * Get a list of categories
	 *
	 * @return Category object array
	 */
	public function get_category_list() {
		return CategoryQuery::create()
				->orderBy_category_id( 'DESC' )
				->find();
	}
	
	/**
	 * Get a list of categories
	 *
	 * @param string $main_cat
	 *
	 * @return Category object array
	 */
	public function get_categories( $main_cat ) {
	
		$not_article = implode( ',', array( self::MULTIMEDIA_CAT, self::GALLERY_CAT, self::DOCUMENT_CAT ) );
		$not_document = implode( ',', array( self::MULTIMEDIA_CAT, self::GALLERY_CAT, self::ARTICLE_CAT ) );
		$not_gallery = implode( ',', array( self::MULTIMEDIA_CAT, self::DOCUMENT_CAT, self::ARTICLE_CAT ) );
		$not_multimedia = implode( ',', array( self::GALLERY_CAT, self::DOCUMENT_CAT, self::ARTICLE_CAT ) );
	
		return CategoryQuery::create()
				->_if( $main_cat == 'article' )
					->where( "category_id NOT IN ( {$not_article} )" )
				->_elseif( $main_cat == 'document' )
					->where( "category_id NOT IN ( {$not_document} )" )
				->_elseif( $main_cat == 'gallery' )
					->where( "category_id NOT IN ( {$not_gallery} )" )
				->_elseif( $main_cat == 'multimedia' )
					->where( "category_id NOT IN ( {$not_multimedia} )" )
				->_endIf()
				->orderBy_category_id( 'DESC' )
				->find();
	}
	
	/**
	 * Get only main categories
	 *
	 * @return Category object array
	 */
	public function get_main_categories() {
		return CategoryQuery::create()
				->filterBy_category_id( array( self::MULTIMEDIA_CAT, self::GALLERY_CAT, 
						self::DOCUMENT_CAT, self::ARTICLE_CAT ) )
				->orderBy_category_id( 'DESC' )
				->find();
	}
	
	/**
	 * Get the number of categories
	 *
	 * @return int
	 */
	public function get_category_no() {
		return CategoryQuery::create()->count();
	}

	/**
	 * Get a category
	 * 
	 * @param int $category_id
	 *
	 * @return Category object
	 */
	public function get_category( $category_id ) {
		return CategoryQuery::create()->findPk( $category_id );
	}
	
	/**
	 * Get images in a category
	 *
	 * @param int $category_id
	 *
	 * @return Image object array
	 */
	public function get_images( $category_id ) {
		return ImageQuery::create()
				->filterBy_table( 'category' )
				->filterBy_table_id( $category_id )
				->orderBy_order()
				->find();
	}
	
	public function get_category_path( $category_id ) {
		return $this->category_path[$category_id];
	}
	
	/**
	 * Add new category
	 *
	 * @param string $title
	 * @param string $description
	 * 
	 * @return Category object
	 */
	public function add_category( $title, $description ) {
	
		$category = new Category();
		$category->set_title( $title );
		$category->set_description( $description );
		$category->set_category_date( time() );
		$category->save();
		
		return $category;
	}
	
	/**
	 * Update category
	 *
	 * @param int $category_id
	 * @param string $title
	 * @param string $description
	 * 
	 * @return Category object
	 */
	public function update_category( $category_id, $title, $description ) {
		
		$category = CategoryQuery::create()->findPk( $category_id );
		$category->set_title( $title );
		$category->set_description( $description );
		$category->save();
		
		return $category;
	}
	
	/**
	 * Get category link
	 *
	 * @param int $cat_link_id
	 *
	 * @return CatLink object
	 */
	public function get_cat_link( $cat_link_id ) {
	
		return CatLinkQuery::create()->findPk( $cat_link_id );
	}
	
	/**
	 * Add category link
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param int $category_id
	 *
	 * @return CatLink object
	 */
	public function add_cat_link( $table, $table_id, $category_id ) {
	
		$cat_link = new CatLink();
		$cat_link->set_table( $table );
		$cat_link->set_table_id( $table_id );
		$cat_link->set_category_id( $category_id );
		$cat_link->save();
		
		return $cat_link;
	}
	
	/**
	 * Get category links by table item
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return CatLink object array
	 */
	public function get_cat_links( $table, $table_id ) {
		return CatLinkQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->find();
	}
	
	/**
	 * Get category links by category ID
	 *
	 * @param int $category_id
	 *
	 * @return CatLink object array
	 */
	public function get_cat_links_by_category( $category_id ) {
		return CatLinkQuery::create()
				->filterBy_category_id( $category_id )
				->orderBy_order( 'DESC' )
				->find();
	}
	
	/**
	 * Check whether a category list has category ID
	 *
	 * @param int $category_id
	 * @param CatLink object array
	 *
	 * @return boolean
	 */
	public function has_cat_link( $category_id, $category_list ) {
		foreach( $category_list as $category ) {
			if( $category->get_category_id() == $category_id ) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Update category link
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param array $category_list
	 *
	 */
	public function update_cat_links( $table, $table_id, $category_list ) {
	
		$this->delete_cat_links( $table, $table_id );
		foreach( $category_list as $category_id => $on ) {
			$this->add_cat_link( $table, $table_id, $category_id );
		}
	}
	
	/**
	 * Update category link orders
	 *
	 * @param array $cat_links
	 * @param array $order
	 *
	 */
	public function update_orders( $cat_links, $order ) {
	
		$max_order = max( $order );
		foreach( $cat_links as $i => $cat_link_id ) {
			$cat_link = $this->get_cat_link( $cat_link_id );
			$cat_link->set_order( $max_order -$i );
			$cat_link->save();
		}
	}
		
	/**
	 * Delete category links by table item
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	public function delete_cat_links( $table, $table_id ) {
		CatLinkQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->delete();
	}
	
	/**
	 * Update image list in database for category ID
	 *
	 * @param int $category_id
	 *
	 */
	public function update_from_folder( $category_id ) {
	
		$path = APPPATH .'../userfiles/' .$this->category_path[$category_id];
		$files = scandir( $path );
		$images = $this->get_images( $category_id );
		
		// delete images from database if files do not exist
		foreach( $images as $image ) {
			if( !in_array( $image->get_path(), $files ) ) {
				$image->delete();
			}
		}
		
		// update image from folder
		$cms_image = $this->cms_model->get_cms_image();
		foreach( $files as $file ) {
			if( $this->_has_image( $category_id, $file ) ) {
			}
			else {
				if( $file == '.' || $file == '..' ) {
					continue;
				}
				$cms_image->add_image( 'category', $category_id, '', '', $file, time() );
			}
		}
	}
	
	/**
	 * Check whether the folder has old image
	 *
	 * @param int $category_id
	 * @param string $file
	 *
	 * @return boolean
	 */
	private function _has_image( $category_id, $file ) {
		$images = $this->get_images( $category_id );
		foreach( $images as $image ) {
			if( $image->get_path() == $file ) {
				return true;
			}
		}
		
		return false;
	}
		
	/**
	 * Delete category
	 *
	 * @param int $category_id
	 *
	 */
	public function delete_category( $category_id ) {
		CategoryQuery::create()->findPk( $category_id )->delete();
	}
}

?>