<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS article class
 * 
 * a class for managing article in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 06/30/14
 *
 */
class Cms_article {

	private $cms_model;
	
	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * generate where clause
	 *
	 * @param string $query
	 *
	 * @return string
	 */
	private function _generate_query( $query ) {
	
		if( empty( $query ) ) {
			return '0';
		}
		$where = '';
		
		$qs = explode( ' ,', $query );
		foreach( $qs as $q ) {
			$where = "( title LIKE '%{$q}%' OR
					description LIKE '%{$q}%' ) AND ";
		}
		
		$where = substr( $where, 0, strlen( $where ) -4 );
		return $where;
	}
	
	/**
	 * Search from query string
	 *
	 * @param string $query
	 *
	 * @return Article object array
	 */
	public function search( $query ) {
	
		$where = $this->_generate_query( $query );
	
		$con = Propel::getConnection();
		$sql = "SELECT a.* 
				FROM article a, item_list il
				WHERE il.table = 'article'
				AND a.article_id = il.table_id
				AND il.enable = 1
                AND {$where}
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return ArticlePeer::populateObjects( $stmt );
	}
	
	/**
	 * Get a list of articles
	 *
	 * @param string $display
	 * @param int $index
	 * @param int $no
	 *
	 * @return Article object array
	 */
	public function get_article_list( $display='all', $index=0, $no=10 ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT a.* 
				FROM article a, item_list il
				WHERE il.table = 'article'
				AND a.article_id = il.table_id
				{$enabled}
				ORDER BY il.order DESC
                LIMIT {$no}
                OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return ArticlePeer::populateObjects( $stmt );
	}
	
	/**
	 * Count the number of articles
	 *
	 * @param string $display
	 *
	 * @return int
	 */
	public function count_article_list( $display='all' ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*)
				FROM article a, item_list il
				WHERE il.table = 'article'
				AND a.article_id = il.table_id
				{$enabled}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Get a list of articles with article category only
	 *
	 * @return Article object array
	 */
	public function get_article_cat_list() {
	
		$this->cms_model->get_cms_category();
		$category_id = Cms_Category::ARTICLE_CAT;
	
		$con = Propel::getConnection();
		$sql = "SELECT a.* 
				FROM article a, cat_link cl
				WHERE cl.table = 'article'
				AND a.article_id = cl.table_id
				AND cl.category_id = '{$category_id}'
				AND a.enable = 1
				ORDER BY a.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return ArticlePeer::populateObjects( $stmt );
	}
	
	/**
	 * Get the number of articles
	 *
	 * @return int
	 */
	public function get_article_no() {
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM article a, item_list il
				WHERE il.table = 'article'
				AND a.article_id = il.table_id";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get an article
	 * 
	 * @param int $article_id
	 *
	 * @return Article object
	 */
	public function get_article( $article_id ) {
		return ArticleQuery::create()->findPk( $article_id );
	}
	
	/**
	 * Get an article
	 *
	 * @param string $title
	 *
	 * @return Article object
	 */
	public function get_article_by_title( $title ) {
		return ArticleQuery::create()
				->filterBy_title( $title )
				->findOne();
	}
	
	/**
	 * Get next value of article ID
	 *
	 * @return int
	 */
	public function get_next_id() {
	
		$con = Propel::getConnection();
		$sql = "SELECT AUTO_INCREMENT
				FROM  information_schema.TABLES
				WHERE TABLE_SCHEMA = 'earth_en'
				AND TABLE_NAME = 'article'";

		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Search articles
	 *
	 * @param string $search
	 *
	 * @return Article object array
	 */
	public function ajax_search( $search ) {
		return ArticleQuery::create()
				->filterBy_title( "%{$search}%" )
				->limit( 10 )
				->find();
	}
	
	/**
	 * Add new article
	 *
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $image_path
	 * @param string $image_desc
	 * @param string $attachment
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * @param int $article_date
	 * 
	 * @return Article object
	 */
	public function add_article( $title, $video_code, $description, 
			$attachment, $list_title, $list_desc, $list_img, $article_date ) {
	
		$article = new Article();
		$article->set_title( $title );
		$article->set_video_code( $video_code );
		$article->set_description( $description );
		$article->set_attachment( $attachment );
		$article->set_list_title( $list_title );
		$article->set_list_desc( $list_desc );
		$article->set_list_img( $list_img );
		$article->set_article_date( $article_date );
		$article->set_created_date( time() );
		$article->save();
        
		return $article;
	}
	
	/**
	 * Update article
	 *
	 * @param int $article_id
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $image_path
	 * @param string $image_desc
	 * @param string $attachment
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * @param int $article_date
	 * 
	 * @return Article object
	 */
	public function update_article( $article_id, $title, $video_code, $description, $attachment, 
            $list_title, $list_desc, $list_img, $article_date ) {
		
		$article = ArticleQuery::create()->findPk( $article_id );
		$article->set_title( $title );
		$article->set_video_code( $video_code );
		$article->set_description( $description );
		$article->set_attachment( $attachment );
		$article->set_list_title( $list_title );
		$article->set_list_desc( $list_desc );
		$article->set_list_img( $list_img );
		$article->set_article_date( $article_date );
		$article->save();
		
		return $article;
	}
	
	/**
	 * Delete article
	 *
	 * @param int $article_id
	 *
	 */
	public function delete_article( $article_id ) {
		ArticleQuery::create()->findPk( $article_id )->delete();
	}
}

?>