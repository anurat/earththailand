<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS calendar class
 * 
 * a class for managing calendar in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 08/09/14
 *
 */
class Cms_calendar {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of calendar events
	 *
	 * @param string $display
	 * @param int $index
	 * @param int $no
	 *
	 * @return Calendar object array
	 */
	public function get_calendar_list( $display='all', $index=0, $no=10 ) {
		
		$enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
		
		$con = Propel::getConnection();
		$sql = "SELECT c.* 
				FROM calendar c, item_list il
				WHERE il.table = 'calendar'
				AND c.calendar_id = il.table_id
				{$enabled}
				ORDER BY il.order DESC
				LIMIT {$no}
				OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return CalendarPeer::populateObjects( $stmt );
	}
	
	/**
	 * Count the number of calendar events
	 *
	 * @param string $display
	 *
	 * @return int
	 */
	public function count_calendar_list( $display='all' ) {
		return CalendarQuery::create()
				->_if( $display == 'enabled' )
					->filterBy_enable( 1 )
				->_endif()
				->count();
	}
	
	/**
	 * Get calendar events
	 *
	 * @param int $start
	 * @param int $end
	 *
	 * @return Calendar object array
	 */
	public function get_events( $start, $end ) {
	    
		$con = Propel::getConnection();
		$sql = "SELECT *
				FROM calendar c, item_list il
				WHERE il.table = 'calendar'
				AND il.table_id = c.calendar_id
				AND il.enable = 1
				AND start_date >= '{$start}'
				AND end_date < '{$end}'";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return CalendarPeer::populateObjects( $stmt );
    }
	
	/**
	 * Get a calendar
	 * 
	 * @param int $calendar_id
	 *
	 * @return Calendar object
	 */
	public function get_calendar( $calendar_id ) {
		return CalendarQuery::create()->findPk( $calendar_id );
	}
	
	/**
	 * Get the number of calendar events
	 *
	 * @return int
	 */
	public function get_calendar_no() {
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM calendar c, item_list il
				WHERE il.table = 'calendar'
				AND c.calendar_id = il.table_id";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get next value of calendar ID
	 *
	 * @return int
	 */
	public function get_next_id() {
	
		$con = Propel::getConnection();
		$sql = "SELECT AUTO_INCREMENT
				FROM  information_schema.TABLES
				WHERE TABLE_SCHEMA = 'earth_en'
				AND TABLE_NAME = 'calendar'";

		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * generate where clause
	 *
	 * @param string $query
	 *
	 * @return string
	 */
	private function _generate_query( $query ) {
		
		if( empty( $query ) ) {
			return '0';
		}
		$where = '';
		
		$qs = explode( ' ,', $query );
		foreach( $qs as $q ) {
			$where = "( title LIKE '%{$q}%' OR
					description LIKE '%{$q}%' ) AND ";
		}
		
		$where = substr( $where, 0, strlen( $where ) -4 );
		return $where;
	}
	
	/**
	 * Search from query string
	 *
	 * @param string $query
	 *
	 * @return Calendar object array
	 */
	public function search( $query ) {
		
		$where = $this->_generate_query( $query );
		
		$con = Propel::getConnection();
		$sql = "SELECT c.* 
				FROM calendar c, item_list il
				WHERE il.table = 'calendar'
				AND c.calendar_id = il.table_id
				AND il.enable = 1
				AND {$where}
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return CalendarPeer::populateObjects( $stmt );
	}
	
	/**
	 * Add new calendar
	 *
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param int $start_date
	 * @param int $end_date
	 * 
	 * @return Calendar object
	 */
	public function add_calendar( $title, $video_code, $description, $start_date, $end_date ) {
	
		$calendar = new Calendar();
		$calendar->set_title( $title );
		$calendar->set_video_code( $video_code );
		$calendar->set_description( $description );
		$calendar->set_start_date( $start_date );
		$calendar->set_end_date( $end_date );
		$calendar->set_created_date( time() );
		$calendar->save();
		
		return $calendar;
	}
	
	/**
	 * Update calendar
	 *
	 * @param int $calendar_id
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param int $start_date
	 * @param int $end_date
	 * 
	 * @return Calendar object
	 */
	public function update_calendar( $calendar_id, $title, $video_code, $description, 
			$start_date, $end_date ) {
		
		$calendar = CalendarQuery::create()->findPk( $calendar_id );
		$calendar->set_title( $title );
		$calendar->set_video_code( $video_code );
		$calendar->set_description( $description );
		$calendar->set_start_date( $start_date );
		$calendar->set_end_date( $end_date );
		$calendar->save();
		
		return $calendar;
	}
		
	/**
	 * Delete calendar
	 *
	 * @param int $calendar_id
	 *
	 */
	public function delete_calendar( $calendar_id ) {
		CalendarQuery::create()->findPk( $calendar_id )->delete();
	}
}

?>