<?php

/**
 * Cms signin class
 * 
 * a class for user authentication in thaiecoalert project. 
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 06/28/14
 *
 */
class Cms_signin {
	// session data
	private $session_data;
	
	// CMS model
	private $cms_model;
	
	/**
	 * Constructor
	 *
	 * @param Cms_model object $_cms_model
	 * 
	 * @return Cms_signin
	 */
	function __construct( $_cms_model ) {
		
		$this->cms_model = $_cms_model;
		
		// initialize session data
		$this->_add_session_data();
	}
	
	/**
	 * Add session data
	 *
	 */
	private function _add_session_data( ) {
	
		$cms_user = $this->cms_model->get_cms_user();
		
		$session = $this->cms_model->session;
		$session_userid = $session->userdata( 'userid' );
		if( !empty( $session_userid ) ) {
			$cms_user->log_access( $session_userid );
		}
		$onlines = $cms_user->get_online_users();
		
		// session data
		$this->session_data = array(
			'userid' => $session->userdata( 'userid' ),
			'email_address' => $session->userdata( 'email_address' ),
			'role' => $session->userdata( 'role' ),
			'language' => $session->userdata( 'language' ),
			'signedin' => $session->userdata( 'signedin' ),
			'no_per_page' => $session->userdata( 'no_per_page' ),
			'onlines' => $onlines
		);
	}
	
	/**
	 * Get session data
	 *
	 * @return session array
	 */
	public function get_session_data() {
		return $this->session_data;
	}

	/**
	 * Check whether email and password is valid
	 *
	 * @param string $email
	 * @param string $password
	 * 
	 * @return User object if valid, false if not valid
	 */
	public function validate( $email, $password ) {
		
		$user = UserQuery::create()
				->filterBy_email( $email )
				->filterBy_password( md5( $password ) )
				->filterBy_enable( 1 )
				->findOne();
		
		return $user;
	}
		
	/**
	 * Sign in
	 *
	 * @param User object $user
	 *
	 */
	public function signin( $user ) {
	
		$user_id = $user->get_user_id();
		$session = $this->cms_model->session;
		$session->set_userdata( 'userid', $user_id );
		$session->set_userdata( 'email_address', $user->get_email() );
		$session->set_userdata( 'role', $user->get_role() );
		$session->set_userdata( 'signedin', time() );
		$session->set_userdata( 'no_per_page', 10 );
		$this->_add_session_data();
		
		$cms_user = $this->cms_model->get_cms_user();
		$cms_user->update_last_signedin( $user->get_user_id() );
	}
	
	/**
	 * Sign out
	 *
	 */
	public function signout() {
	
		$session = $this->cms_model->session;
		$cms_user = $this->cms_model->get_cms_user();
		$cms_user->reset_access( $session->userdata( 'userid' ) );
	
		$this->cms_model->session->sess_destroy();
	}
	
	/**
	 * Toggle language
	 *
	 */
	public function toggle_lang() {
		$session = $this->cms_model->session;
		$language = $session->userdata( 'language' );
		
		if( empty( $language ) || $language == 'english' ) {
			$session->set_userdata( 'language', 'thai' );
		}
		else {
			$session->set_userdata( 'language', 'english' );
		}
	}
	
	/**
	 * Set the number of items per page
	 *
	 * @param int $no
	 */
	public function set_no_per_page( $no ) {
		$session = $this->cms_model->session;
		$session->set_userdata( 'no_per_page', $no );
		$this->session_data['no_per_page'] = $no;
	}
}
?>