<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS image class
 * 
 * a class for managing image in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/21/14
 *
 */
class Cms_image {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of images
	 *
	 * @return Image object array
	 */
	public function get_image_list() {
		return ImageQuery::create()
				->orderBy_order()
				->find();
	}
	
	/**
	 * Get the number of images
	 *
	 * @return int
	 */
	public function get_image_no() {
		return ImageQuery::create()->count();
	}

	/**
	 * Get a image
	 * 
	 * @param int $image_id
	 *
	 * @return Image object
	 */
	public function get_image( $image_id ) {
		return ImageQuery::create()->findPk( $image_id );
	}
	
	/**
	 * Get images in an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return Image object array
	 */
	public function get_images( $table, $table_id ) {
		return ImageQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->orderBy_order()
				->find();
	}
	
	
	/**
	 * Get maximum number of image order from a gallery
	 *
	 * @param int $gallery_id
	 *
	 * @return int
	 */
	public function get_max_order( $table, $table_id ) {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( i.order )
				FROM image i
				WHERE i.table = '{$table}'
				AND table_id = '{$table_id}'";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Check whether the folder has old image
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param string $file
	 *
	 * @return boolean
	 */
	public function has_image( $table, $table_id, $file ) {
		$images = $this->get_images( $table, $table_id );
		foreach( $images as $image ) {
			if( $image->get_path() == $file ) {
				return true;
			}
		}
		
		return false;
	}
		
	/**
	 * Add new image
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param string $name
	 * @param string $description
	 * @param string $path
	 * @param int $image_date
	 * 
	 * @return Image object
	 */
	public function add_image( $table, $table_id, $name, $description, $path, $image_date ) {
	
		$image = new Image();
		$image->set_table( $table );
		$image->set_table_id( $table_id );
		$image->set_name( $name );
		$image->set_description( $description );
		$image->set_path( $path );
		$image->set_image_date( $image_date );
		$image->set_order( $this->get_max_order( $table, $table_id ) +1 );
		$image->save();
		
		return $image;
	}
	
	/**
	 * Update image
	 *
	 * @param int $image_id
	 * @param string $table
	 * @param int $table_id
	 * @param string $name
	 * @param string $description
	 * @param string $path
	 * @param int $image_date
	 * 
	 * @return Image object
	 */
	public function update_image( $image_id, $table, $table_id, $name, $description, $path, $image_date ) {
		
		$image = ImageQuery::create()->findPk( $image_id );
		$image->set_table( $table );
		$image->set_table_id( $table_id );
		$image->set_name( $name );
		$image->set_description( $description );
		$image->set_path( $path );
		$image->set_image_date( $image_date );
		$image->save();
		
		return $image;
	}
	
	/**
	 * Update image info
	 *
	 * @param int $image_id
	 * @param string $name
	 * @param string $description
	 *
	 * @return Image object
	 */
	public function update_image_info( $image_id, $name, $description ) {

		$image = ImageQuery::create()->findPk( $image_id );
		$image->set_name( $name );
		$image->set_description( $description );
		$image->save();
		
		return $image;
	}
	
	/**
	 * Update image orders
	 *
	 * @param array $image
	 * @param array $order
	 *
	 */
	public function update_orders( $image, $order ) {
	
		$min_order = min( $order );
		foreach( $image as $i => $image_id ) {
			$image = $this->get_image( $image_id );
			$image->set_order( $min_order +$i );
			$image->save();
		}
	}
		
	/**
	 * Update image list in database for an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	public function update_from_folder( $table, $table_id ) {
	
		$cap_name = ucfirst( $table );
		$cap_name = ( $cap_name == 'Gallery' )? $cap_name: $cap_name .'s';
		$path = APPPATH ."../userfiles/{$cap_name}/" .$table_id;
		if( !file_exists( $path ) ) {
			return;
		}
		
		$files = scandir( $path );
		$images = $this->get_images( $table, $table_id );

		// delete images from database if files do not exist
		foreach( $images as $image ) {
			if( !in_array( $image->get_path(), $files ) ) {
				$image->delete();
			}
		}
		
		// update image from folder
		foreach( $files as $file ) {
			if( $this->has_image( $table, $table_id, $file ) ) {
			}
			else {
				if( $file == '.' || $file == '..' ) {
					continue;
				}
				$this->add_image( $table, $table_id, '', '', $file, time() );
			}
		}
	}
	
	/**
	 * Delete image
	 *
	 * @param int $image_id
	 *
	 */
	public function delete_image( $image_id ) {
		ImageQuery::create()->findPk( $image_id )->delete();
	}
	
	/**
	 * Delete images from table items
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	public function delete_images( $table, $table_id ) {
		ImageQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->delete();
	}
}

?>