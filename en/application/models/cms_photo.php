<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS photo class
 * 
 * a class for managing photo in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/19/14
 *
 */
class Cms_photo {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of photos
	 *
	 * @return Photo object array
	 */
	public function get_photo_list() {
		return PhotoQuery::create()
				->orderBy_order( 'DESC' )
				->find();
	}
	
	/**
	 * Get the number of photos
	 *
	 * @return int
	 */
	public function get_photo_no() {
		return PhotoQuery::create()->count();
	}

	/**
	 * Get a photo
	 * 
	 * @param int $photo_id
	 *
	 * @return Photo object
	 */
	public function get_photo( $photo_id ) {
		return PhotoQuery::create()->findPk( $photo_id );
	}
	
	/**
	 * Get maximum number of photo order from a gallery
	 *
	 * @param int $gallery_id
	 *
	 * @return int
	 */
	public function get_max_order( $gallery_id ) {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( p.order )
				FROM photo p
				WHERE gallery_id = '{$gallery_id}'";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Add new photo
	 *
	 * @param int $gallery_id
	 * @param string $name
	 * @param string $description
	 * @param string $path
	 * @param int $photo_date
	 * 
	 * @return Photo object
	 */
	public function add_photo( $gallery_id, $name, $description, $path, $photo_date ) {
	
		$photo = new Photo();
		$photo->set_gallery_id( $gallery_id );
		$photo->set_name( $name );
		$photo->set_description( $description );
		$photo->set_path( $path );
		$photo->set_photo_date( $photo_date );
		$photo->set_order( $this->get_max_order( $gallery_id ) +1 );
		$photo->save();
		
		return $photo;
	}
	
	/**
	 * Update photo
	 *
	 * @param int $photo_id
	 * @param int $gallery_id
	 * @param string $name
	 * @param string $description
	 * @param string $path
	 * @param int $photo_date
	 * @param int $order
	 * 
	 * @return Photo object
	 */
	public function update_photo( $photo_id, $gallery_id, $name, $description, $path, $photo_date ) {
		
		$photo = PhotoQuery::create()->findPk( $photo_id );
		$photo->set_gallery_id( $gallery_id );
		$photo->set_name( $name );
		$photo->set_description( $description );
		$photo->set_path( $path );
		$photo->set_photo_date( $photo_date );
		$photo->save();
		
		return $photo;
	}
	
	/**
	 * Update photo info
	 *
	 * @param int $photo_id
	 * @param string $name
	 * @param string $description
	 *
	 * @return Photo object
	 */
	public function update_photo_info( $photo_id, $name, $description ) {

		$photo = PhotoQuery::create()->findPk( $photo_id );
		$photo->set_name( $name );
		$photo->set_description( $description );
		$photo->save();
		
		return $photo;
	}
	
	/**
	 * Update photo orders
	 *
	 * @param array $photo
	 * @param array $order
	 *
	 */
	public function update_orders( $photo, $order ) {
	
		$min_order = min( $order );
		foreach( $photo as $i => $photo_id ) {
			$photo = $this->get_photo( $photo_id );
			$photo->set_order( $min_order +$i );
			$photo->save();
		}
	}
		
	/**
	 * Delete photo
	 *
	 * @param int $photo_id
	 *
	 */
	public function delete_photo( $photo_id ) {
		PhotoQuery::create()->findPk( $photo_id )->delete();
	}
}

?>