<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

$route['admin'] = 'admin2';

$route['about'] = 'page/category/1';
$route['earth1'] = 'page/category/2';
$route['earth2'] = 'page/category/3';
$route['earth3'] = 'page/category/4';
$route['earth4'] = 'page/category/5';
$route['earth5'] = 'page/category/6';
$route['earth-land'] = 'page/category/7';
$route['earth6'] = 'page/category/8';
$route['earth7'] = 'page/category/13';
$route['earth8'] = 'page/category/14';
$route['pollution_type'] = 'page/pollution_type';
$route['pollution_list/(:any)'] = 'page/pollution_list/$1';

$route['articles'] = 'home/articles';
$route['documents'] = 'home/documents';
$route['gallery_list'] = 'home/gallery_list';
$route['multimedia_list'] = 'home/multimedia_list';
$route['events'] = 'home/events';
$route['tags'] = 'home/tags';
$route['pollutions'] = 'home/pollutions';
$route['search'] = 'home/search';

$route['articles/(:any)'] = 'home/articles/$1';
$route['documents/(:any)'] = 'home/documents/$1';
$route['gallery_list/(:any)'] = 'home/gallery_list/$1';
$route['multimedia_list/(:any)'] = 'home/multimedia_list/$1';

$route['article/(:any)'] = 'page/article/$1';
$route['document/(:any)'] = 'page/document/$1';
$route['gallery/(:any)'] = 'page/gallery/$1';
$route['multimedia/(:any)'] = 'page/multimedia/$1';
$route['tag/(:any)'] = 'page/tag/$1';
$route['pollution/(:any)'] = 'page/pollution/$1';
$route['calendar/(:any)'] = 'page/calendar/$1';

$route['admin/homepage/(:any)'] = 'admin/homepage2/$1/$2';

/* End of file routes.php */
/* Location: ./application/config/routes.php */