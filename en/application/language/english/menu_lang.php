<?php

$lang['menu_homepage'] = 'Homepage Management';
$lang['menu_header_image'] = 'Header Image';
$lang['menu_slideshow'] = 'Slideshow';
$lang['menu_news_slideshow'] = 'News Slideshow';
$lang['menu_add_news_slideshow'] = 'Add News Slideshow';
$lang['menu_update_news_slideshow'] = 'Update News Slideshow';
$lang['menu_documents'] = 'Documents';
$lang['menu_short_documents'] = 'Documents';
$lang['menu_articles'] = 'News/Articles';
$lang['menu_link'] = 'External Links';
$lang['menu_add_link'] = 'Add External Link';
$lang['menu_update_link'] = 'Update External Link';
$lang['menu_tags']	= 'Tags';
$lang['menu_tag_detail']	= 'Tag Detail';
$lang['menu_footer'] = 'Footer';
$lang['menu_topmenu'] = 'Top Menu Management';
$lang['menu_pollution_map'] = 'Pollution Map';
$lang['menu_add_pollution'] = 'Add Pollution';
$lang['menu_update_pollution'] = 'Update Pollution';
$lang['menu_pollution_history'] = 'Pollution History';
$lang['menu_top_articles']	= 'News/Articles';
$lang['menu_add_article'] = 'Add News/Article';
$lang['menu_update_article'] = 'Update News/Article';
$lang['menu_article_history'] = 'News/Article History';
$lang['menu_top_documents']	= 'Documents';
$lang['menu_add_document'] = 'Add Document';
$lang['menu_update_document'] = 'Update Document';
$lang['menu_document_history'] = 'Document History';
$lang['menu_gallery'] = 'Gallery';
$lang['menu_add_gallery'] = 'Add Gallery';
$lang['menu_update_gallery'] = 'Update Gallery';
$lang['menu_manage_gallery'] = 'Manage Gallery';
$lang['menu_gallery_history'] = 'Gallery History';
$lang['menu_calendar'] = 'Activity Calendar';
$lang['menu_add_calendar'] = 'Add Activity Calendar';
$lang['menu_update_calendar'] = 'Update Activity Calendar';
$lang['menu_calendar_history'] = 'Calendar History';
$lang['menu_about'] = 'About/Contact';
$lang['menu_short_about'] = 'About/Contact';
$lang['menu_leftmenu'] = 'Left Menu Management';
$lang['menu_rights'] = 'PRTR & Community Right-to-Know';
$lang['menu_monitoring'] = 'Communities in Action';
$lang['menu_sub_grant'] = 'Sub grant';
$lang['menu_waste'] = 'Industrial & Hazardous Waste Management';
$lang['menu_mabtapud'] = 'Map Ta Phut Studies';
$lang['menu_paint'] = 'Chemicals & Products Life-Cycle Management';
$lang['menu_data'] = 'Pollution Hotspots';
$lang['menu_business'] = 'Corporate Accountability';
$lang['menu_policy'] = 'Policy Reference';
$lang['menu_multimedia'] = 'Multimedia';
$lang['menu_add_multimedia'] = 'Add Multimedia';
$lang['menu_update_multimedia'] = 'Update Multimedia';
$lang['menu_multimedia_history'] = 'Multimedia History';
$lang['menu_system'] = 'System';
$lang['menu_statistics'] = 'Web Site Statistics';
$lang['menu_profiles'] = 'Profiles';
$lang['menu_user'] = 'Users';
$lang['menu_add_user'] = 'Add User';
$lang['menu_update_user'] = 'Update User';
$lang['menu_change_password'] = 'Change Password';
$lang['menu_reset_password'] = 'Reset Password';
$lang['menu_search'] = 'Search';

$lang['menu_update_category'] = 'Update Category';

$lang['menu_customer'] = 'Customer service dept. Connectiv Co., Ltd.';
$lang['menu_phone'] = 'Phone';
$lang['menu_email'] = 'E-mail';

$lang['menu_admin'] = 'Administrator';
$lang['menu_signin'] = 'Sign In';


/* End of file menu_lang.php */
/* Location: ./system/language/english/menu_lang.php */