<?php

$lang['news_slideshow_no'] = 'No. of news slideshows';
$lang['news_slideshow_content_type'] = 'Content Type';
$lang['news_slideshow_content_title'] = 'Content Title';

/* End of file news_slideshow_lang.php */
/* Location: ./system/language/english/news_slideshow_lang.php */