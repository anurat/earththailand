<?php

$lang['user_name'] = 'Name';
$lang['user_email'] = 'E-mail Address';
$lang['user_password'] = 'Password';
$lang['user_role'] = 'Role';
$lang['user_enabled'] = 'Enabled';
$lang['user_created_date'] = 'Created Date';
$lang['user_last_signedin'] = 'Last Signed In';
$lang['user_last_accessed'] = 'Last Accessed';

/* End of file user_lang.php */
/* Location: ./system/language/english/user_lang.php */