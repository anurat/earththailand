<?php

$lang['profiles_name'] = 'Name';
$lang['profiles_email'] = 'E-mail address';
$lang['profiles_password'] = 'Password';
$lang['profiles_change_password'] = 'CHANGE PASSWORD';

$lang['profiles_old_password'] = 'Old Password';
$lang['profiles_new_password'] = 'New Password';
$lang['profiles_retype_password'] = 'Retype Password';

/* End of file profiles_lang.php */
/* Location: ./system/language/english/profiles_lang.php */