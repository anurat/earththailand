<?php

$lang['gallery_title'] = 'Title';
$lang['gallery_image'] = 'Image';
$lang['gallery_image_folder'] = 'Please upload images into folder';
$lang['gallery_image_no'] = 'No. of images';
$lang['gallery_image_upload'] = 'support JPG, GIF, PNG file extension and recommended image size is 800 x 350 pixels and file size less than 2MB';
$lang['gallery_image_desc'] = 'Image Description';
$lang['gallery_video_code'] = 'Video Code';
$lang['gallery_description'] = 'Full Description';
$lang['gallery_attachment'] = 'Attachment (support doc, docx, ppt, pptx, pdf file extension) file size less than 20MB';
$lang['gallery_tags'] = 'Tag (please put comma between each keyword)';
$lang['gallery_list_title'] = 'Item Title';
$lang['gallery_list_desc'] = 'Item Description';
$lang['gallery_list_img'] = 'Item Image';
$lang['gallery_date'] = 'Gallery Date';
$lang['gallery_newline'] = 'Please type &lt;br /&gt; to add new line';

$lang['gallery_image_folder'] = 'Please upload images into folder';
$lang['gallery_image_no'] = 'No. of images';

/* End of file gallery_lang.php */
/* Location: ./system/language/english/gallery_lang.php */