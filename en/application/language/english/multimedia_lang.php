<?php

$lang['multimedia_title'] = 'Title';
$lang['multimedia_description1'] = 'Top description';
$lang['multimedia_video_code'] = 'Video Code';
$lang['multimedia_description2'] = 'Bottom description';
$lang['multimedia_list_title'] = 'Item title';
$lang['multimedia_list_desc'] = 'Item description';
$lang['multimedia_list_img'] = 'Item Image';
$lang['multimedia_tags'] = 'Tags';
$lang['multimedia_newline'] = 'Please type &lt;br /&gt; to add new line';


/* End of file multimedia_lang.php */
/* Location: ./system/language/english/multimedia_lang.php */