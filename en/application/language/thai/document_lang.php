<?php

$lang['document_no'] = 'จำนวนเอกสารเผยแพร่';
$lang['document_title'] = 'หัวข้อ';
$lang['document_image'] = 'รูปภาพ';
$lang['document_image_folder'] = 'กรุณาอัพโหลดรูปภาพไปที่โฟลเดอร์';
$lang['document_image_no'] = 'จำนวนรูปภาพ';
$lang['document_image_upload'] = 'รองรับไฟล์ JPG, GIF, PNG และขนาดภาพที่เหมาะสมคือ 800 x 350 pixels ขนาดไฟล์ไม่เกิน 2MB';
$lang['document_video_code'] = 'คำสั่งวีดีโอ';
$lang['document_description'] = 'คำอธิบายฉบับเต็ม';
$lang['document_tags'] = 'แท็ก (กรุณาใส่เครื่องหมาย , ระหว่างคำ)';
$lang['document_list_title'] = 'หัวข้อรายการ';
$lang['document_list_desc'] = 'คำอธิบายรายการ';
$lang['document_list_img'] = 'รูปภาพรายการ';
$lang['document_date'] = 'วันที่เอกสาร';
$lang['document_newline'] = 'กรุณาพิมพ์ &lt;br /&gt; เพื่อขึ้นบรรทัดใหม่';

/* End of file document_lang.php */
/* Location: ./system/language/thai/document_lang.php */