<?php

$lang['calendar_no'] = 'จำนวนปฏิทินกิจกรรม';
$lang['calendar_title'] = 'หัวข้อ';
$lang['calendar_video_code'] = 'คำสั่งวีดีโอ';
$lang['calendar_description'] = 'คำอธิบายฉบับเต็ม';
$lang['calendar_tags'] = 'แท็ก (กรุณาใส่เครื่องหมาย , ระหว่างคำ)';
$lang['start_date'] = 'วันที่เริ่มต้น';
$lang['end_date'] = 'วันที่สิ้นสุด';
$lang['calendar_newline'] = 'กรุณาพิมพ์ &lt;br /&gt; เพื่อขึ้นบรรทัดใหม่';

/* End of file calendar_lang.php */
/* Location: ./system/language/thai/calendar_lang.php */