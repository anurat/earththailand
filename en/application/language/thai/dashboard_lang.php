<?php

$lang['dashboard_dashboard'] = 'แผงหน้าปัด';

$lang['dashboard_article_no'] = 'จำนวนบทความทั้งหมด';
$lang['dashboard_group'] = 'หมวดหมู่';
$lang['dashboard_no'] = 'จำนวน (เรื่อง)';

$lang['dashboard_quick_icon'] = 'ปุ่มทางลัด';
$lang['dashboard_statistics'] = 'สถิติการเข้าชมเว็บไซต์';


/* End of file dashboard_lang.php */
/* Location: ./system/language/thai/dashboard_lang.php */