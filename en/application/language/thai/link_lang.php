<?php

$lang['link_title'] = 'ชื่อ';
$lang['link_link'] = 'ลิงก์';
$lang['link_thai_link'] = 'เครือข่ายในประเทศ';
$lang['link_regional_link'] = 'เครือข่ายระดับภูมิภาค';
$lang['link_global_link'] = 'เครือข่ายระดับสากล';
$lang['link_no'] = 'จำนวนลิงก์:';

/* End of file link_lang.php */
/* Location: ./system/language/thai/link_lang.php */