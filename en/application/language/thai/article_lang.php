<?php

$lang['article_no'] = 'จำนวนข่าว/บทความ';
$lang['article_title'] = 'หัวข้อ';
$lang['article_image'] = 'รูปภาพ';
$lang['article_image_folder'] = 'กรุณาอัพโหลดรูปภาพไปที่โฟลเดอร์';
$lang['article_image_no'] = 'จำนวนรูปภาพ';
$lang['article_image_upload'] = 'รองรับไฟล์ JPG, GIF, PNG และขนาดภาพที่เหมาะสมคือ 800 x 350 pixels ขนาดไฟล์ไม่เกิน 2MB';
$lang['article_image_desc'] = 'คำอธิบายรูปภาพ';
$lang['article_video_code'] = 'คำสั่งวีดีโอ';
$lang['article_description'] = 'คำอธิบายฉบับเต็ม';
$lang['article_tags'] = 'แท็ก (กรุณาใส่เครื่องหมาย , ระหว่างคำ)';
$lang['article_attachment'] = 'เอกสารแนบ (รองรับไฟล์ .doc, .docx, .ppt, .pptx, .pdf) ขนาดไฟล์ไม่เกิน 20MB';
$lang['article_list_title'] = 'หัวข้อรายการ';
$lang['article_list_desc'] = 'คำอธิบายรายการ';
$lang['article_list_img'] = 'รูปภาพรายการ';
$lang['article_date'] = 'วันที่ข่าว/บทความ';
$lang['article_newline'] = 'กรุณาพิมพ์ &lt;br /&gt; เพื่อขึ้นบรรทัดใหม่';

/* End of file article_lang.php */
/* Location: ./system/language/thai/article_lang.php */