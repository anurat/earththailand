<?php

$lang['menu_homepage'] = 'การจัดการข้อมูลหน้าแรก';
$lang['menu_header_image'] = 'ภาพหัวเรื่อง';
$lang['menu_slideshow'] = 'สไลด์โชว์';
$lang['menu_news_slideshow'] = 'สไลด์ข่าว';
$lang['menu_add_news_slideshow'] = 'เพิ่มสไลด์ข่าว';
$lang['menu_update_news_slideshow'] = 'แก้ไขสไลด์ข่าว';
$lang['menu_documents'] = 'เอกสาร/ข้อมูลเผยแพร่';
$lang['menu_short_documents'] = 'เอกสาร/ข้อมูล';
$lang['menu_articles'] = 'ข่าว/บทความ';
$lang['menu_link'] = 'ลิงก์เครือข่าย';
$lang['menu_add_link'] = 'เพิ่มลิงก์เครือข่าย';
$lang['menu_update_link'] = 'แก้ไขลิงก์เครือข่าย';
$lang['menu_tags']	= 'แท็ก';
$lang['menu_tag_detail']	= 'รายละเอียดแท็ก';
$lang['menu_footer'] = 'ท้ายหน้า';
$lang['menu_topmenu'] = 'การจัดการข้อมูลเมนูหลัก';
$lang['menu_pollution_map'] = 'แผนที่มลพิษ';
$lang['menu_add_pollution'] = 'เพิ่มข่าวมลพิษ';
$lang['menu_update_pollution'] = 'แก้ไขข่าวมลพิษ';
$lang['menu_pollution_history'] = 'ประวัติข่าวมลพิษ';
$lang['menu_top_articles']	= 'ข่าว/บทความ';
$lang['menu_add_article'] = 'เพิ่มข่าว/บทความ';
$lang['menu_update_article'] = 'แก้ไขข่าว/บทความ';
$lang['menu_article_history'] = 'ประวัติข่าว/บทความ';
$lang['menu_top_documents']	= 'เอกสาร/ข้อมูลเผยแพร่';
$lang['menu_add_document'] = 'เพิ่มเอกสาร/ข้อมูลเผยแพร่';
$lang['menu_update_document'] = 'แก้ไขเอกสาร/ข้อมูลเผยแพร่';
$lang['menu_document_history'] = 'ประวัติเอกสาร/ข้อมูลเผยแพร่';
$lang['menu_gallery'] = 'ห้องภาพ';
$lang['menu_add_gallery'] = 'เพิ่มห้องภาพ';
$lang['menu_update_gallery'] = 'แก้ไขห้องภาพ';
$lang['menu_manage_gallery'] = 'จัดการภาพ';
$lang['menu_gallery_history'] = 'ประวัติห้องภาพ';
$lang['menu_calendar'] = 'ปฏิทินกิจกรรม';
$lang['menu_add_calendar'] = 'เพิ่มปฏิทินกิจกรรม';
$lang['menu_update_calendar'] = 'แก้ไขปฏิทินกิจกรรม';
$lang['menu_calendar_history'] = 'ประวัติปฏิทินกิจกรรม';
$lang['menu_about'] = 'เกี่ยวกับมูลนิธิ/ติดต่อเรา';
$lang['menu_short_about'] = 'เกี่ยวกับมูลนิธิ';
$lang['menu_leftmenu'] = 'การจัดการข้อมูลเมนูด้านซ้าย';
$lang['menu_rights'] = 'สิทธิการเข้าถึงข้อมูลมลพิษ (PRTR)';
$lang['menu_monitoring'] = 'การเฝ้าระวังและตรวจสอบมลพิษ';
$lang['menu_sub_grant'] = 'โครงการทุนขนาดเล็ก';
$lang['menu_waste'] = 'ขยะอุตสาหกรรมและสารเคมีอันตราย';
$lang['menu_mabtapud'] = 'มาบตาพุดศึกษา';
$lang['menu_paint'] = 'สารเคมีในวงจรผลิตภัณฑ์';
$lang['menu_data'] = 'พื้นที่มลพิษ';
$lang['menu_business'] = 'ภาคธุรกิจและความรับผิดชอบ';
$lang['menu_policy'] = 'ข้อมูลนโยบายและกฏหมาย';
$lang['menu_multimedia'] = 'มัลติมีเดีย';
$lang['menu_add_multimedia'] = 'เพิ่มมัลติมีเดีย';
$lang['menu_update_multimedia'] = 'แก้ไขมัลติมีเดีย';
$lang['menu_multimedia_history'] = 'ประวัติมัลติมีเดีย';
$lang['menu_system'] = 'ระบบ';
$lang['menu_statistics'] = 'สถิติการเข้าชมเว็บไซต์';
$lang['menu_profiles'] = 'ข้อมูลส่วนตัว';
$lang['menu_user'] = 'ผู้ใช้';
$lang['menu_add_user'] = 'เพิ่มผู้ใช้';
$lang['menu_update_user'] = 'แก้ไขผู้ใช้';
$lang['menu_change_password'] = 'เปลี่ยนรหัสผ่าน';
$lang['menu_reset_password'] = 'รีเซ็ทรหัสผ่าน';
$lang['menu_search'] = 'ค้นหา';

$lang['menu_update_category'] = 'แก้ไขกลุ่มบทความ';

$lang['menu_customer'] = 'ฝ่ายบริการลูกค้า บริษัท คอนเนคทีฟ จำกัด';
$lang['menu_phone'] = 'โทรศัพท์';
$lang['menu_email'] = 'อีเมล';

$lang['menu_admin'] = 'ผู้ดูแลระบบ';
$lang['menu_signin'] = 'เข้าระบบ';


/* End of file menu_lang.php */
/* Location: ./system/language/thai/menu_lang.php */