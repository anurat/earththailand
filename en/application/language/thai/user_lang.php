<?php

$lang['user_name'] = 'ชื่อ';
$lang['user_email'] = 'อีเมล';
$lang['user_password'] = 'รหัสผ่าน';
$lang['user_role'] = 'หน้าที่';
$lang['user_enabled'] = 'เปิดใช้งาน';
$lang['user_created_date'] = 'วันที่สร้าง';
$lang['user_last_signedin'] = 'ล็อกอินล่าสุด';
$lang['user_last_accessed'] = 'เข้าถึงล่าสุด';

/* End of file user_lang.php */
/* Location: ./system/language/thai/user_lang.php */