<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH .'controllers/admin/admin_ci.php';

class Menu extends Admin_CI {

	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Show pollution map page
	 *
	 * @param int $index
	 */
	public function pollution_map( $index=0 ) {
	
		// set the number of items per page
		$segment5 = $this->uri->segment(5);
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $segment5 ) ) {
			$cms_signin->set_no_per_page( $segment5 );
		}
		$session_data = $cms_signin->get_session_data();
		$no = $session_data['no_per_page'];
	
		$cms_pollution = $this->cms_model->get_cms_pollution();
		$this->data['pollution_list'] = $cms_pollution->get_pollution_list( 'all', $index, $no );
		$count = $cms_pollution->count_pollution_list();

		$this->load->library('pagination');
		$config['base_url'] = site_url( "/admin/menu/pollution_map" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['uri_segment'] = 4;
		$this->pagination->initialize( $config );
		$this->data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $this->data['pollution_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
		
		// save current url
		$this->session->set_flashdata( 'url', current_url());
		
		$this->data['table'] = 'pollution';
		$this->data['content'] = 'admin/menu/pollution_map';
		$this->data['title'] = $this->lang->line('menu_pollution_map', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Add pollution
	 *
	 */
	public function add_pollution() {
	
		$this->pollution();
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			redirect( 'admin/menu/pollution_map' );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_pollution' );
			break;
		}
	}
	
	/**
	 * Update pollution
	 *
	 * @param int $pollution_id
	 *
	 */
	public function update_pollution( $pollution_id ) {
		$this->pollution( $pollution_id );
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			$url = $this->input->post( 'previous_url' );
			if( empty( $url ) ) {
				redirect( 'admin/menu/pollution_map' );
			}
			redirect( $url );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_pollution' );
			break;
		case 'save':
			redirect( 'admin/menu/update_pollution/' .$pollution_id );
			break;
		}
	}
	
	/**
	 * Delete pollution
	 *
	 * @param int $pollution_id
	 *
	 */
	public function delete_pollution( $pollution_id ) {
	
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tags( 'pollution', $pollution_id );

		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_item_list->delete_item_list_by_table( 'pollution', $pollution_id );

		$cms_pollution = $this->cms_model->get_cms_pollution();
		$cms_pollution->delete_pollution( $pollution_id );
		redirect( 'admin/menu/pollution_map' );
	}

	/**
	 * Show pollution history
	 *
	 * @param int $pollution_id
	 *
	 */
	public function pollution_history( $pollution_id ) {
		$cms_pollution = $this->cms_model->get_cms_pollution();
		$this->data['pollution'] = $cms_pollution->get_pollution( $pollution_id );
		
		$cms_history = $this->cms_model->get_cms_history();
		$this->data['histories'] = $cms_history->get_histories( 'pollution', $pollution_id );
		
		$this->data['content'] = 'admin/menu/pollution_history';
		$this->data['title'] = $this->lang->line('menu_pollution_history', $this->data['language'] )
				.'-' .$this->data['pollution']->get_title();
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show articles page
	 *
	 * @param int $index
	 */
	public function articles( $index=0 ) {
	
		// set the number of items per page
		$segment5 = $this->uri->segment(5);
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $segment5 ) ) {
			$cms_signin->set_no_per_page( $segment5 );
		}
		$session_data = $cms_signin->get_session_data();
		$no = $session_data['no_per_page'];
	
		$cms_article = $this->cms_model->get_cms_article();
		$this->data['article_list'] = $cms_article->get_article_list( 'all', $index, $no );
		$count = $cms_article->count_article_list();

		$this->load->library('pagination');
		$config['base_url'] = site_url( "/admin/menu/articles" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['uri_segment'] = 4;
		$this->pagination->initialize( $config );
		$this->data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $this->data['article_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
		
		$this->session->set_flashdata( 'url', current_url());
		
		$this->data['table'] = 'article';
		$this->data['content'] = 'admin/menu/articles';
		$this->data['title'] = $this->lang->line('menu_articles', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Add article
	 *
	 */
	public function add_article() {
		$this->article();
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			redirect( 'admin/menu/articles' );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_article' );
			break;
		}
	}

	/**
	 * Update article
	 *
	 * @param int $article_id
	 *
	 */
	public function update_article( $article_id ) {
		$this->article( $article_id );
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			$url = $this->input->post( 'previous_url' );
			if( empty( $url ) ) {
				redirect( 'admin/menu/articles' );
			}
			redirect( $url );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_article' );
			break;
		case 'save':
			redirect( 'admin/menu/update_article/' .$article_id );
			break;
		}
	}
	
	/**
	 * Delete article
	 *
	 * @param int $article_id
	 *
	 */
	public function delete_article( $article_id ) {
	
		$cms_image = $this->cms_model->get_cms_image();
		$cms_image->delete_images( 'article', $article_id );
		
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tags( 'article', $article_id );

		$cms_category = $this->cms_model->get_cms_category();
		$cms_category->delete_cat_links( 'article', $article_id );

		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_item_list->delete_item_list_by_table( 'article', $article_id );

		$cms_article = $this->cms_model->get_cms_article();
		$cms_article->delete_article( $article_id );
	}

	/**
	 * Show article history
	 *
	 * @param int $article_id
	 *
	 */
	public function article_history( $article_id ) {
		$cms_article = $this->cms_model->get_cms_article();
		$this->data['article'] = $cms_article->get_article( $article_id );
		
		$cms_history = $this->cms_model->get_cms_history();
		$this->data['histories'] = $cms_history->get_histories( 'article', $article_id );
		
		$this->data['content'] = 'admin/menu/article_history';
		$this->data['title'] = $this->lang->line('menu_article_history', $this->data['language'] )
				.'-' .$this->data['article']->get_title();
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show documents page
	 *
	 * @param int $index
	 */
	public function documents( $index=0 ) {

		// set the number of items per page
		$segment5 = $this->uri->segment(5);
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $segment5 ) ) {
			$cms_signin->set_no_per_page( $segment5 );
		}
		$session_data = $cms_signin->get_session_data();
		$no = $session_data['no_per_page'];
	
		$cms_document = $this->cms_model->get_cms_document();
		$this->data['document_list'] = $cms_document->get_document_list( 'all', $index, $no );
		$count = $cms_document->count_document_list();
			
		$this->load->library('pagination');
		$config['base_url'] = site_url( "/admin/menu/documents" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['uri_segment'] = 4;
		$this->pagination->initialize( $config );
		$this->data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $this->data['document_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
		
		// save current url
		$this->session->set_flashdata( 'url', current_url());
		
		$this->data['table'] = 'document';
		$this->data['content'] = 'admin/menu/documents';
		$this->data['title'] = $this->lang->line('menu_documents', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Add document
	 *
	 */
	public function add_document() {
		$this->document();
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			redirect( 'admin/menu/documents' );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_document' );
			break;
		}
	}

	/**
	 * Update document
	 *
	 * @param int $document_id
	 *
	 */
	public function update_document( $document_id ) {
		$this->document( $document_id );
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			$url = $this->input->post( 'previous_url' );
			if( empty( $url ) ) {
				redirect( 'admin/menu/documents' );
			}
			redirect( $url );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_document' );
			break;
		case 'save':
			redirect( 'admin/menu/update_document/' .$document_id );
			break;
		}
	}
	
	/**
	 * Delete document
	 *
	 * @param int $document_id
	 *
	 */
	public function delete_document( $document_id ) {
	
		$cms_image = $this->cms_model->get_cms_image();
		$cms_image->delete_images( 'document', $document_id );
		
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tags( 'document', $document_id );

		$cms_category = $this->cms_model->get_cms_category();
		$cms_category->delete_cat_links( 'document', $document_id );

		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_item_list->delete_item_list_by_table( 'document', $document_id );

		$cms_document = $this->cms_model->get_cms_document();
		$cms_document->delete_document( $document_id );
		redirect( 'admin/menu/documents' );
	}

	/**
	 * Show document history
	 *
	 * @param int $document_id
	 *
	 */
	public function document_history( $document_id ) {
		$cms_document = $this->cms_model->get_cms_document();
		$this->data['document'] = $cms_document->get_document( $document_id );
		
		$cms_history = $this->cms_model->get_cms_history();
		$this->data['histories'] = $cms_history->get_histories( 'document', $document_id );
		
		$this->data['content'] = 'admin/menu/document_history';
		$this->data['title'] = $this->lang->line('menu_document_history', $this->data['language'] )
				.'-' .$this->data['document']->get_title();
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show gallery page
	 *
	 * @param int $index
	 */
	public function gallery( $index=0 ) {

		// set the number of items per page
		$segment5 = $this->uri->segment(5);
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $segment5 ) ) {
			$cms_signin->set_no_per_page( $segment5 );
		}
		$session_data = $cms_signin->get_session_data();
		$no = $session_data['no_per_page'];
	
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$this->data['gallery_list'] = $cms_gallery->get_gallery_list( 'all', $index, $no );
		$count = $cms_gallery->count_gallery_list();
	
		$this->load->library('pagination');
		$config['base_url'] = site_url( "/admin/menu/gallery" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['uri_segment'] = 4;
		$this->pagination->initialize( $config );
		$this->data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $this->data['gallery_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
		
		// save current url
		$this->session->set_flashdata( 'url', current_url());
		
		$this->data['table'] = 'gallery';
		$this->data['content'] = 'admin/menu/gallery';
		$this->data['title'] = $this->lang->line('menu_gallery', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Add gallery
	 *
	 */
	public function add_gallery() {
		$this->gallery_page();
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			redirect( 'admin/menu/gallery' );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_gallery' );
			break;
		}
	}

	/**
	 * Update gallery
	 *
	 * @param int $gallery_id
	 *
	 */
	public function update_gallery( $gallery_id ) {
		$this->gallery_page( $gallery_id );
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			$url = $this->input->post( 'previous_url' );
			if( empty( $url ) ) {
				redirect( 'admin/menu/gallery' );
			}
			redirect( $url );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_gallery' );
			break;
		case 'save':
			redirect( 'admin/menu/update_gallery/' .$gallery_id );
			break;
		}
	}
	
	/**
	 * Delete gallery
	 *
	 * @param int $gallery_id
	 *
	 */
	public function delete_gallery( $gallery_id ) {
	
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tags( 'gallery', $gallery_id );

		$cms_category = $this->cms_model->get_cms_category();
		$cms_category->delete_cat_links( 'gallery', $gallery_id );

		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_item_list->delete_item_list_by_table( 'gallery', $gallery_id );

		$cms_gallery = $this->cms_model->get_cms_gallery();
		$cms_gallery->get_gallery_photos( $gallery_id )->delete();
		$cms_gallery->delete_gallery( $gallery_id );
		redirect( 'admin/menu/gallery' );
	}

	/**
	 * Manage gallery
	 *
	 * @param int $gallery_id
	 *
	 */
	public function manage_gallery( $gallery_id ) {
	
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$this->data['gallery'] = $cms_gallery->get_gallery( $gallery_id );
		$this->data['gallery_id'] = $gallery_id;
	
		$this->data['content'] = 'admin/menu/manage_gallery';
		$this->data['title'] = $this->lang->line('menu_manage_gallery', $this->data['language'] )
				.'-' .$this->data['gallery']->get_title();
		$this->lang->load( 'gallery', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Show gallery history
	 *
	 * @param int $gallery_id
	 *
	 */
	public function gallery_history( $gallery_id ) {
		$cms_gallery = $this->cms_model->get_cms_gallery();
		$this->data['gallery'] = $cms_gallery->get_gallery( $gallery_id );
		
		$cms_history = $this->cms_model->get_cms_history();
		$this->data['histories'] = $cms_history->get_histories( 'gallery', $gallery_id );
		
		$this->data['content'] = 'admin/menu/gallery_history';
		$this->data['title'] = $this->lang->line('menu_gallery_history', $this->data['language'] )
				.'-' .$this->data['gallery']->get_title();
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show calendar page
	 *
     * @param int $index
	 */
	public function calendar( $index=0 ) {
	
		// set the number of items per page
		$segment5 = $this->uri->segment(5);
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $segment5 ) ) {
			$cms_signin->set_no_per_page( $segment5 );
		}
		$session_data = $cms_signin->get_session_data();
		$no = $session_data['no_per_page'];
	
		$cms_calendar = $this->cms_model->get_cms_calendar();
		$this->data['calendar_list'] = $cms_calendar->get_calendar_list( 'all', $index, $no );
		$count = $cms_calendar->count_calendar_list();
	
		$this->load->library('pagination');
		$config['base_url'] = site_url( "/admin/menu/calendar" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['uri_segment'] = 4;
		$this->pagination->initialize( $config );
		$this->data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $this->data['calendar_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);

        $this->data['table'] = 'calendar';
		$this->data['content'] = 'admin/menu/calendar';
		$this->data['title'] = $this->lang->line('menu_calendar', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}

	/**
	 * Add calendar
	 *
	 */
	public function add_calendar() {
		$this->calendar_page();
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			redirect( 'admin/menu/calendar' );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_calendar' );
			break;
		}
	}

	/**
	 * Update calendar
	 *
	 * @param int $calendar_id
	 *
	 */
	public function update_calendar( $calendar_id ) {
		$this->calendar_page( $calendar_id );
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			$url = $this->session->flashdata( 'url' );
			if( empty( $url ) ) {
				redirect( 'admin/menu/calendar' );
			}
			redirect( $url );
			break;
		case 'save_new':
			redirect( 'admin/menu/add_calendar' );
			break;
		case 'save':
			redirect( 'admin/menu/update_calendar/' .$calendar_id );
			break;
		}
	}
	
	/**
	 * Delete calendar
	 *
	 * @param int $calendar_id
	 *
	 */
	public function delete_calendar( $calendar_id ) {
	
		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tags( 'calendar', $calendar_id );

		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_item_list->delete_item_list_by_table( 'calendar', $calendar_id );

		$cms_calendar = $this->cms_model->get_cms_calendar();
		$cms_calendar->delete_calendar( $calendar_id );
		redirect( 'admin/menu/calendar' );
	}

	/**
	 * Show calendar history
	 *
	 * @param int $calendar_id
	 *
	 */
	public function calendar_history( $calendar_id ) {
		$cms_calendar = $this->cms_model->get_cms_calendar();
		$this->data['calendar'] = $cms_calendar->get_calendar( $calendar_id );
		
		$cms_history = $this->cms_model->get_cms_history();
		$this->data['histories'] = $cms_history->get_histories( 'calendar', $calendar_id );
		
		$this->data['content'] = 'admin/menu/calendar_history';
		$this->data['title'] = $this->lang->line('menu_calendar_history', $this->data['language'] )
				.'-' .$this->data['calendar']->get_title();
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Show about page
	 *
	 */
	public function about() {
	
		$this->categories( 1 );
	}
	
	/**
	 * Update category info
	 *
	 * @param string $category_id
	 */
	public function update_category( $category_id ) {
	
		$this->category( $category_id );
		
		$title = $this->input->post( 'title' );
		if( !empty( $title ) ) {
			switch( $category_id ) {
			case 1:
				$url = "admin/menu/about";
				break;
			case 2:
				$url = "admin/left_menu/rights";
				break;
			case 3:
				$url = "admin/left_menu/monitoring";
				break;
			case 4:
				$url = "admin/left_menu/waste";
				break;
			case 5:
				$url = "admin/left_menu/mabtapud";
				break;
			case 6:
				$url = "admin/left_menu/paint";
				break;
			case 7:
				$url = "admin/left_menu/data";
				break;
			case 8:
				$url = "admin/left_menu/policy";
				break;
			}
			redirect( $url );
		}
	}
	
	/**
	 * Remove cat link
	 *
	 * @param int $cat_link_id
	 *
	 */
	public function remove_cat_link( $cat_link_id ) {
		
		$cms_category = $this->cms_model->get_cms_category();
		$cat_link = $cms_category->get_cat_link( $cat_link_id );
		
		$category_id = $cat_link->get_category_id();
		$cat_link->delete();

		switch( $category_id ) {
		case 1:
			$url = "admin/menu/about";
			break;
		case 2:
			$url = "admin/left_menu/rights";
			break;
		case 3:
			$url = "admin/left_menu/monitoring";
			break;
		case 4:
			$url = "admin/left_menu/waste";
			break;
		case 5:
			$url = "admin/left_menu/mabtapud";
			break;
		case 6:
			$url = "admin/left_menu/paint";
			break;
		case 7:
			$url = "admin/left_menu/data";
			break;
		case 8:
			$url = "admin/left_menu/policy";
			break;
		}
		redirect( $url );
	}
	
}

/* End of file menu.php */
/* Location: ./application/controllers/admin/menu.php */