<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller {


	public function __construct() {
		parent::__construct();
		
	}
	
	/**
	 * Toggle language
	 *
	 */
	public function toggle() {
		
		$cms_signin = $this->cms_model->get_cms_signin();
		$cms_signin->toggle_lang();
		redirect( '/admin/dashboard' );
	}
	
}

/* End of file language.php */
/* Location: ./application/controllers/admin/language.php */