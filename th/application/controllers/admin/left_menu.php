<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once APPPATH .'controllers/admin/admin_ci.php';

class Left_menu extends Admin_CI {

	public function __construct() {
		parent::__construct();
		
	}
	
	/**
	 * Show rights page
	 *
	 */
	public function rights() {
		$this->categories( 2 );
	}
	
	/**
	 * Show monitoring page
	 *
	 */
	public function monitoring() {
		$this->categories( 3 );
	}
	
	/**
	 * Show sub grant page
	 *
	 */
	public function sub_grant() {
		$this->categories( 14 );
	}
	
	/**
	 * Show waste page
	 *
	 */
	public function waste() {
		$this->categories( 4 );
	}

	/**
	 * Show mabtapud page
	 *
	 */
	public function mabtapud() {
		$this->categories( 5 );
	}

	/**
	 * Show paint page
	 *
	 */
	public function paint() {
		$this->categories( 6 );
	}

	/**
	 * Show data page
	 *
	 */
	public function data() {
		$this->categories( 7 );
	}

	/**
	 * Show policy page
	 *
	 */
	public function policy() {
	
		$this->categories( 8 );
	}

	/**
	 * Show business page
	 *
	 */
	public function business() {
	
		$this->categories( 13 );
	}

	/**
	 * Show multimedia page
	 *
	 * @param int $index
	 */
	public function multimedia( $index=0 ) {
	
		// set the number of items per page
		$segment5 = $this->uri->segment(5);
		$cms_signin = $this->cms_model->get_cms_signin();
		if( !empty( $segment5 ) ) {
			$cms_signin->set_no_per_page( $segment5 );
		}
		$session_data = $cms_signin->get_session_data();
		$no = $session_data['no_per_page'];
	
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$this->data['multimedia_list'] = $cms_multimedia->get_multimedia_list( 'all', $index, $no );
		$count = $cms_multimedia->count_multimedia_list();
	
		$this->load->library('pagination');
		$config['base_url'] = site_url( "/admin/left_menu/multimedia" );
		$config['total_rows'] = $count;
		$config['per_page'] = $no;
		$config['uri_segment'] = 4;
		$this->pagination->initialize( $config );
		$this->data['page'] = array(
			'index1' => $index +1,
			'index2' => $index +sizeof( $this->data['multimedia_list'] ),
			'count' => $count,
			'per_page' => $no,
			'links' => $this->pagination->create_links()
		);
		
		// save current url
		$this->session->set_flashdata( 'url', current_url());
		
		$this->data['table'] = 'multimedia';
		$this->data['content'] = 'admin/left_menu/multimedia';
		$this->data['title'] = $this->lang->line('menu_multimedia', $this->data['language'] );
		$this->load->view( 'admin_template', $this->data );
	}
	
	/**
	 * Add multimedia
	 *
	 */
	public function add_multimedia() {
	
		$this->multimedia_page();
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			redirect( 'admin/left_menu/multimedia' );
			break;
		case 'save_new':
			redirect( 'admin/left_menu/add_multimedia' );
			break;
		}
	}
	
	/**
	 * Update multimedia
	 *
	 * @param int $multimedia_id
	 *
	 */
	public function update_multimedia( $multimedia_id ) {
	
		$this->multimedia_page( $multimedia_id );
		
		$save_type = $this->input->post( 'save_type' );
		switch( $save_type ) {
		case 'save_close':
			$url = $this->input->post( 'previous_url' );
			if( empty( $url ) ) {
				redirect( 'admin/left_menu/multimedia' );
			}
			redirect( $url );
			break;
		case 'save_new':
			redirect( 'admin/left_menu/add_multimedia' );
			break;
		case 'save':
			redirect( 'admin/left_menu/update_multimedia/' .$multimedia_id );
			break;
		}
	}
	
	/**
	 * Delete multimedia
	 *
	 * @param int $multimedia_id
	 *
	 */
	public function delete_multimedia( $multimedia_id ) {

		$cms_tag = $this->cms_model->get_cms_tag();
		$cms_tag->delete_tags( 'multimedia', $multimedia_id );

		$cms_category = $this->cms_model->get_cms_category();
		$cms_category->delete_cat_links( 'multimedia', $multimedia_id );

		$cms_item_list = $this->cms_model->get_cms_item_list();
		$cms_item_list->delete_item_list_by_table( 'multimedia', $multimedia_id );

		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$cms_multimedia->delete_multimedia( $multimedia_id );
		redirect( 'admin/left_menu/multimedia' );
	}

	/**
	 * Show multimedia history
	 *
	 * @param int $multimedia_id
	 *
	 */
	public function multimedia_history( $multimedia_id ) {
		$cms_multimedia = $this->cms_model->get_cms_multimedia();
		$this->data['multimedia'] = $cms_multimedia->get_multimedia( $multimedia_id );
		
		$cms_history = $this->cms_model->get_cms_history();
		$this->data['histories'] = $cms_history->get_histories( 'multimedia', $multimedia_id );
		
		$this->data['content'] = 'admin/left_menu/multimedia_history';
		$this->data['title'] = $this->lang->line('menu_multimedia_history', $this->data['language'] )
				.'-' .$this->data['multimedia']->get_title();
		$this->load->view( 'admin_template', $this->data );
	}
	
}

/* End of file left_menu.php */
/* Location: ./application/controllers/admin/left_menu.php */