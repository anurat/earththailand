<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	private $data;

	public function __construct() {
		parent::__construct();

		$cms_signin = $this->cms_model->get_cms_signin();
		$this->data = $cms_signin->get_session_data();
		if( empty( $this->data['language'] ) ) {
			$this->data['language'] = $this->config->item('language');
		}
		$this->lang->load( 'menu', $this->data['language'] );
		$this->lang->load( 'header', $this->data['language'] );
		
		if( empty( $this->data['userid'] ) ) {
			redirect( '/admin/signin' );
		}
	}
	
	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {

		$this->data['search_type'] = $search_type = $this->input->post( 'search_type' );
		if( !empty( $search_type ) ) {
			$this->data['search_text'] = $search_text = $this->input->post( 'search_text' );
			switch( $search_type ) {
			case 'article':
				$cms_obj = $this->cms_model->get_cms_article();
				break;
			case 'document':
				$cms_obj = $this->cms_model->get_cms_document();
				break;
			case 'gallery':
				$cms_obj = $this->cms_model->get_cms_gallery();
				break;
			case 'pollution':
				$cms_obj = $this->cms_model->get_cms_pollution();
				break;
			case 'calendar':
				$cms_obj = $this->cms_model->get_cms_calendar();
				break;
			case 'multimedia':
				$cms_obj = $this->cms_model->get_cms_multimedia();
				break;
			}

			$this->data['result'] = $cms_obj->search( $search_text );
		}

		$this->data['content'] = 'admin/search';
		$this->lang->load( 'menu', $this->data['language'] );
		$this->lang->load( 'search', $this->data['language'] );
		$this->data['title'] = $this->lang->line('menu_search');
		$this->load->view( 'admin_template', $this->data );
	}
	
}

/* End of file search.php */
/* Location: ./application/controllers/admin/search.php */