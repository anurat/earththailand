<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS tag class
 * 
 * a class for managing tag in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/12/14
 *
 */
class Cms_tag {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Search matching tags
	 *
	 * @param string $s
	 *
	 * @return Tag object array
	 */
	public function search( $s ) {
	
		return TagQuery::create()
				->filterBy_name( "%{$s}%" )
				->orderBy_name()
				->find();
	}
	
	/**
	 * Get a list of tags
	 *
	 * @return Tag object array
	 */
	public function get_tag_list() {
		return TagQuery::create()
				->orderBy_name()
				->find();
	}
	
	/**
	 * Get the number of tags
	 *
	 * @return int
	 */
	public function get_tag_no() {
		return TagQuery::create()->count();
	}

	/**
	 * Get a tag
	 * 
	 * @param int $tag_id
	 *
	 * @return Tag object
	 */
	public function get_tag( $tag_id ) {
		return TagQuery::create()->findPk( $tag_id );
	}
	
	/**
	 * Get tags associated with an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return Tag object array
	 */
	public function get_tags( $table, $table_id ) {
		return TagQuery::create()
				->useTagLinkQuery()
					->filterBy_table( $table )
					->filterBy_table_id( $table_id )
				->endUse()
				->find();
	}
	
	/**
	 * Get tag string with comma
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return string
	 */
	public function get_tag_str( $table, $table_id ) {
		$tags = $this->get_tags( $table, $table_id );
		$tag_str = '';
		foreach( $tags as $tag ) {
			$tag_str .= $tag->get_name() .', ';
		}
		
		if( empty( $tag_str ) ) {
			return '';
		}
		
		return substr( $tag_str, 0, strlen( $tag_str ) -2 );
	}
	
	/**
	 * Update tags associated with an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param string $tag_str
	 *
	 */
	public function update_tags( $table, $table_id, $tag_str ) {
	
		$this->delete_tags( $table, $table_id );
		
		$names = explode( ',', $tag_str );
		foreach( $names as $name ) {
			$name = trim( $name );
			if( empty( $name ) ) {
				continue;
			}
		
			$tag = $this->_find_tag( $name );
			if( empty( $tag ) ) {
				$tag = $this->add_tag( $name );
			}
			$this->_add_tag_link( $tag->get_tag_id(), $table, $table_id );
		}
	}
	
	/**
	 * Get all tag links from tag ID
	 *
	 * @param int $tag_id
	 *
	 * @return TagLink object array
	 */
	public function get_tag_links( $tag_id ) {
		return TagLinkQuery::create()
				->filterBy_tag_id( $tag_id )
				->find();
	}
	
	/**
	 * Get tag link
	 *
	 * @param int $tag_link_id
	 *
	 * @return TagLink object
	 */
	public function get_tag_link( $tag_link_id ) {
		return TagLinkQuery::create()->findPk( $tag_link_id );
	}
	
	/**
	 * Add new tag link
	 *
	 * @param int $tag_id
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return TagLink object
	 */
	private function _add_tag_link( $tag_id, $table, $table_id ) {
	
		$tag_link = new TagLink();
		$tag_link->set_tag_id( $tag_id );
		$tag_link->set_table( $table );
		$tag_link->set_table_id( $table_id );
		$tag_link->save();
		
		return $tag_link;
	}
	
	/**
	 * Find a tag from name
	 *
	 * @param string $name
	 *
	 * return Tag object
	 */
	private function _find_tag( $name ) {
		return TagQuery::create()
				->filterBy_name( $name )
				->findOne();
	}
	
	/**
	 * Delete tag link
	 *
	 * @param int $tag_link_id
	 *
	 */
	public function delete_tag_link( $tag_link_id ) {
		TagLinkQuery::create()->findPk( $tag_link_id )->delete();
	}
	
	/**
	 * Delete tags associated with an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 */
	public function delete_tags( $table, $table_id ) {
		TagLinkQuery::create()
			->filterBy_table( $table )
			->filterBy_table_id( $table_id )
			->delete();
	}
	
	/**
	 * Add new tag
	 *
	 * @param string $name
	 * 
	 * @return Tag object
	 */
	public function add_tag( $name ) {
	
		$tag = new Tag();
		$tag->set_name( $name );
		$tag->save();
		
		return $tag;
	}
	
	/**
	 * Update tag
	 *
	 * @param int $tag_id
	 * @param string $name
	 *
	 * @return Tag object
	 */
	public function update_tag( $tag_id, $name ) {
		
		$tag = TagQuery::create()->findPk( $tag_id );
		$tag->set_name( $name );
		$tag->save();
		
		return $tag;
	}
		
	/**
	 * Delete tag
	 *
	 * @param int $tag_id
	 *
	 */
	public function delete_tag( $tag_id ) {
		TagQuery::create()->findPk( $tag_id )->delete();
	}
}

?>