<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS user class
 * 
 * a class for managing user in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 06/30/14
 *
 */
class Cms_user {

	const ADMIN_ROLE = 'editor_admin';
	const EDITOR_ROLE = 'user_admin';
	
	const RESET_PASSWORD = 'toxicfree';

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of users
	 *
	 * @return User object array
	 */
	public function get_user_list() {
		return UserQuery::create()
                ->orderBy_created_date()
                ->find();
	}
	
	/**
	 * Get a list of online users
	 *
	 * @return string
	 */
	public function get_online_users() {
	
		$users = UserQuery::create()
				->filterBy_last_accessed( array( 'min' => time() -7200 ) )
				->orderBy_last_signedin()
				->find();
				
		$onlines = '';
		foreach( $users as $user ) {
			$onlines .= $user->get_name() .', ';
		}
		
		return $onlines;
	}
	
	/**
	 * save last access by a user
	 *
	 * @param int $user_id
	 *
	 */
	public function log_access( $user_id ) {
		$user = $this->get_user( $user_id );
		$user->set_last_accessed( time() );
		$user->save();
	}
	
	/**
	 * reset user access
	 *
	 * @param int $user_id
	 *
	 */
	public function reset_access( $user_id ) {
		$user = $this->get_user( $user_id );
		$user->set_last_accessed( 0 );
		$user->save();
	}

	/**
	 * Get a user
	 * 
	 * @param int $user_id
	 *
	 * @return User object
	 */
	public function get_user( $user_id ) {
		return UserQuery::create()->findPk( $user_id );
	}
    
    /**
     * Enable user
     *
     * @param int $user_id
     */
    public function enable( $user_id ) {
        $user = $this->get_user( $user_id );
        $user->set_enable( 1 );
        $user->save();
    }
	
    /**
     * Disable user
     *
     * @param int $user_id
     */
    public function disable( $user_id ) {
        $user = $this->get_user( $user_id );
        $user->set_enable( 0 );
        $user->save();
    }
	
	/**
	 * Add new user
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $password
	 * @param string $role
	 * @param int $enable
	 * 
	 * @return User object
	 */
	public function add_user( $name, $email, $password, $role, $enable ) {
	
		$user = new User();
		$user->set_name( $name );
		$user->set_email( $email );
		$user->set_password( md5( $password ) );
		$user->set_role( $role );
		$user->set_enable( $enable );
		$user->set_created_date( time() );
		$user->save();
		
		return $user;
	}
	
	/**
	 * Update user
	 *
	 * @param int $user_id
	 * @param string $name
	 * @param string $email
	 * @param string $role
	 * @param int $enable
	 *
	 * @return User object
	 */
	public function update_user( $user_id, $name, $email, $role, $enable ) {
		
		$user = UserQuery::create()->findPk( $user_id );
		$user->set_name( $name );
		$user->set_email( $email );
		$user->set_role( $role );
		$user->set_enable( $enable );
		$user->save();
		
		return $user;
	}
	
	/**
	 * Reset password
	 *
	 * @param int $user_id
	 */
	public function reset_password( $user_id ) {
	
		$user = UserQuery::create()->findPk( $user_id );
		$user->set_password( md5( self::RESET_PASSWORD ) );
		$user->save();
	}
	
	/**
	 * Update password
	 *
	 * @param int $user_id
	 * @param string $password
	 *
	 * @return User object
	 */
	public function update_password( $user_id, $password ) {
		$user = UserQuery::create()->findPk( $user_id );
		$user->set_password( md5( $password ) );
		$user->save();
		
		return $user;
	}
	
	/**
	 * Update last signedin
	 *
	 * @param int $user_id
	 *
	 * @return User object
	 */
	public function update_last_signedin( $user_id ) {
		$user = UserQuery::create()->findPk( $user_id );
		$user->set_last_signedin( time() );
		$user->save();
		
		return $user;
	}
	
	/**
	 * Delete user
	 *
	 * @param int $user_id
	 *
	 */
	public function delete_user( $user_id ) {
		UserQuery::create()->findPk( $user_id )->delete();
	}
}

?>