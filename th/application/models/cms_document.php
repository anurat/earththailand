<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS document class
 * 
 * a class for managing document in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 06/30/14
 *
 */
class Cms_document {

	private $cms_model;
	
	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * generate where clause
	 *
	 * @param string $query
	 *
	 * @return string
	 */
	private function _generate_query( $query ) {
	
		if( empty( $query ) ) {
			return '0';
		}
		$where = '';
		
		$qs = explode( ' ,', $query );
		foreach( $qs as $q ) {
			$where = "( title LIKE '%{$q}%' OR
					description LIKE '%{$q}%' ) AND ";
		}
		
		$where = substr( $where, 0, strlen( $where ) -4 );
		return $where;
	}
	
	/**
	 * Search from query string
	 *
	 * @param string $query
	 *
	 * @return Document object array
	 */
	public function search( $query ) {
	
		$where = $this->_generate_query( $query );
	
		$where = $this->_generate_query( $query );
	
		$con = Propel::getConnection();
		$sql = "SELECT d.* 
				FROM document d, item_list il
				WHERE il.table = 'document'
				AND d.document_id = il.table_id
				AND {$where}
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return DocumentPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get a list of documents
	 *
	 * @param string $display
	 * @param int $index
	 * @param int $no
	 *
	 * @return Document object array
	 */
	public function get_document_list( $display='all', $index=0, $no=10 ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT d.* 
				FROM document d, item_list il
				WHERE il.table = 'document'
				AND d.document_id = il.table_id
				{$enabled}
				ORDER BY il.order DESC
                LIMIT {$no}
                OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return DocumentPeer::populateObjects( $stmt );
	}
	
	/**
	 * Count the number of documents
	 *
	 * @param string $display
	 *
	 * @return int
	 */
	public function count_document_list( $display='all' ) {
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM document d, item_list il
				WHERE il.table = 'document'
				AND d.document_id = il.table_id
				{$enabled}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
        return $result[0][0];
	}
	
	/**
	 * Get a list of documents with document category only
	 *
	 * @return Document object array
	 */
	public function get_document_cat_list() {
	
		$this->cms_model->get_cms_category();
		$category_id = Cms_Category::DOCUMENT_CAT;
	
		$con = Propel::getConnection();
		$sql = "SELECT DISTINCT d.* 
				FROM document d, cat_link cl
				WHERE cl.table = 'document'
				AND d.document_id = cl.table_id
				AND cl.category_id = '{$category_id}'
				AND d.enable = 1
				ORDER BY d.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return DocumentPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get the number of documents
	 *
	 * @return int
	 */
	public function get_document_no() {
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM document d, item_list il
				WHERE il.table = 'document'
				AND d.document_id = il.table_id";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get a document
	 * 
	 * @param int $document_id
	 *
	 * @return Document object
	 */
	public function get_document( $document_id ) {
		return DocumentQuery::create()->findPk( $document_id );
	}
	
	/**
	 * Get a document
	 *
	 * @param string $title
	 *
	 * @return Document object
	 */
	public function get_document_by_title( $title ) {
		return DocumentQuery::create()
				->filterBy_title( $title )
				->findOne();
	}
	
	/**
	 * Get the maximum number of order
	 *
	 * @return int
	 */
	public function get_max_order() {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( d.order )
				FROM document d";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Get next value of document ID
	 *
	 * @return int
	 */
	public function get_next_id() {
	
		$con = Propel::getConnection();
		$sql = "SELECT AUTO_INCREMENT
				FROM  information_schema.TABLES
				WHERE TABLE_SCHEMA = 'nichemktas_tea'
				AND TABLE_NAME = 'document'";

		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Enable document
	 *
	 * @param int $document_id
	 */
	public function enable( $document_id ) {
		
		$document = $this->get_document( $document_id );
		$document->set_enable( 1 );
		$document->save();
	}
	
	/**
	 * Disable document
	 *
	 * @param int $document_id
	 */
	public function disable( $document_id ) {
		
		$document = $this->get_document( $document_id );
		$document->set_enable( 0 );
		$document->save();
	}
	
	/**
	 * Search documents
	 *
	 * @param string $search
	 *
	 * @return Document object array
	 */
	public function ajax_search( $search ) {
		return DocumentQuery::create()
				->filterBy_title( "%{$search}%" )
				->limit( 10 )
				->find();
	}
	
	/**
	 * Add new document
	 *
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * @param int $document_date
	 * 
	 * @return Document object
	 */
	public function add_document( $title, $video_code, $description, $list_title, $list_desc, $list_img, 
			$document_date ) {
	
		$document = new Document();
		$document->set_title( $title );
		$document->set_video_code( $video_code );
		$document->set_description( $description );
		$document->set_list_title( $list_title );
		$document->set_list_desc( $list_desc );
		$document->set_list_img( $list_img );
		$document->set_document_date( $document_date );
		$document->save();
		
		return $document;
	}
	
	/**
	 * Update document
	 *
	 * @param int $document_id
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * @param int $document_date
	 * 
	 * @return Document object
	 */
	public function update_document( $document_id, $title, $video_code, $description, 
			$list_title, $list_desc, $list_img, $document_date ) {
		
		$document = DocumentQuery::create()->findPk( $document_id );
		$document->set_title( $title );
		$document->set_video_code( $video_code );
		$document->set_description( $description );
		$document->set_list_title( $list_title );
		$document->set_list_desc( $list_desc );
		$document->set_list_img( $list_img );
		$document->set_document_date( $document_date );
		$document->save();
		
		return $document;
	}
	
	/**
	 * Delete document
	 *
	 * @param int $document_id
	 *
	 */
	public function delete_document( $document_id ) {
		DocumentQuery::create()->findPk( $document_id )->delete();
	}
}

?>