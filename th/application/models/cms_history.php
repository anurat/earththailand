<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS history class
 * 
 * a class for managing history in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/11/14
 *
 */
class Cms_history {

	/**
	 * Constructor
	 *
	 */
	function __construct() {
	}
	
	/**
	 * Get a list of historys
	 *
	 * @return History object array
	 */
	public function get_history_list() {
		return HistoryQuery::create()->find();
	}

	/**
	 * Get a history
	 * 
	 * @param int $history_id
	 *
	 * @return History object
	 */
	public function get_history( $history_id ) {
		return HistoryQuery::create()->findPk( $history_id );
	}
	
	/**
	 * Get first history
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return History object
	 */
	public function get_first_history( $table, $table_id ) {
		return HistoryQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->orderBy_changed_date( 'ASC' )
				->findOne();
	}
	
	/**
	 * Get latest history
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return History object
	 */
	public function get_latest_history( $table, $table_id ) {
		return HistoryQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->orderBy_changed_date( 'DESC' )
				->findOne();
	}
	
	/**
	 * Get histories in an item table
	 *
	 * @param string $table
	 * @param int $table_id
	 *
	 * @return History object array
	 */
	public function get_histories( $table, $table_id ) {
	
		return HistoryQuery::create()
				->filterBy_table( $table )
				->filterBy_table_id( $table_id )
				->orderBy_changed_date( 'DESC' )
				->find();
	}
	
	/**
	 * Add new history
	 *
	 * @param string $table
	 * @param int $table_id
	 * @param int $user_id
	 * 
	 * @return History object
	 */
	public function add_history( $table, $table_id, $user_id ) {
	
		$history = new History();
		$history->set_table( $table );
		$history->set_table_id( $table_id );
		$history->set_user_id( $user_id );
		$history->set_changed_date( time() );
		$history->save();
		
		return $history;
	}
	
	/**
	 * Update history
	 *
	 * @param int $history_id
	 * @param string $table
	 * @param int $table_id
	 * @param int $user_id
	 * @param int $changed_date
	 *
	 * @return History object
	 */
	public function update_history( $history_id, $table, $table_id, $user_id, $changed_date ) {
		
		$history = HistoryQuery::create()->findPk( $history_id );
		$history->set_table( $table );
		$history->set_table_id( $table_id );
		$history->set_user_id( $user_id );
		$history->set_changed_date( $changed_date );
		$history->save();
		
		return $history;
	}
		
	/**
	 * Delete history
	 *
	 * @param int $history_id
	 *
	 */
	public function delete_history( $history_id ) {
		HistoryQuery::create()->findPk( $history_id )->delete();
	}
}

?>