<?php


/**
 * CMS model class
 * 
 * a main model class for Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 06/28/14
 *
 */
class Cms_model extends CI_Model {

	private $cms_signin;
	private $cms_user;
	private $cms_article;
	private $cms_history;
	private $cms_tag;
	private $cms_multimedia;
	private $cms_document;
	private $cms_gallery;
	private $cms_photo;
	private $cms_category;
	private $cms_image;
	private $cms_homepage;
	private $cms_news_slideshow;
	private $cms_calendar;
	private $cms_pollution;
	private $cms_item_list;
	
	/**
	 * Constructor
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	
		// propel setup
		$this->propel_setup();
	}

	/**
	 * Set up Propel
	 *
	 */
	private function propel_setup() {
		
		// Set the include_path to include your generated OM 'classes' dir.
		set_include_path( APPPATH .'../propel/build/classes' .PATH_SEPARATOR .get_include_path() );
		
		require_once APPPATH .'../propelorm/runtime/lib/Propel.php';
		Propel::init( APPPATH .'../propel/build/conf/nichemktas_tea-conf.php' );
		
		$con = Propel::getConnection();
		$con->query( "SET character_set_results=utf8" );
		$con->query( "SET character_set_connection=utf8" );
		$con->query( "SET character_set_client=utf8" ); 
	}
	
	/**
	 * Get cms_user
	 * 
	 *
	 * @return Cms_user object
	 */
	public function get_cms_user() {
		
		if( empty( $this->cms_user ) ) {
			include_once "cms_user.php";
			$this->cms_user = new Cms_user( $this );
		}
		
		return $this->cms_user;
	}
	
	/**
	 * Get cms_signin
	 * 
	 *
	 * @return Cms_signin object
	 */
	public function get_cms_signin() {
		
		if( empty( $this->cms_signin ) ) {
			include_once "cms_signin.php";
			$this->cms_signin = new Cms_signin( $this );
		}
		
		return $this->cms_signin;
	}
	
	/**
	 * Get cms_article
	 * 
	 *
	 * @return Cms_article object
	 */
	public function get_cms_article() {
		
		if( empty( $this->cms_article ) ) {
			include_once "cms_article.php";
			$this->cms_article = new Cms_article( $this );
		}
		
		return $this->cms_article;
	}
	
	/**
	 * Get cms_history
	 * 
	 *
	 * @return Cms_history object
	 */
	public function get_cms_history() {
		
		if( empty( $this->cms_history ) ) {
			include_once "cms_history.php";
			$this->cms_history = new Cms_history( $this );
		}
		
		return $this->cms_history;
	}
	
	/**
	 * Get cms_tag
	 * 
	 *
	 * @return Cms_tag object
	 */
	public function get_cms_tag() {
		
		if( empty( $this->cms_tag ) ) {
			include_once "cms_tag.php";
			$this->cms_tag = new Cms_tag( $this );
		}
		
		return $this->cms_tag;
	}
	
	/**
	 * Get cms_multimedia
	 * 
	 *
	 * @return Cms_multimedia object
	 */
	public function get_cms_multimedia() {
		
		if( empty( $this->cms_multimedia ) ) {
			include_once "cms_multimedia.php";
			$this->cms_multimedia = new Cms_multimedia( $this );
		}
		
		return $this->cms_multimedia;
	}
	
	/**
	 * Get cms_document
	 * 
	 *
	 * @return Cms_document object
	 */
	public function get_cms_document() {
		
		if( empty( $this->cms_document ) ) {
			include_once "cms_document.php";
			$this->cms_document = new Cms_document( $this );
		}
		
		return $this->cms_document;
	}
	
	/**
	 * Get cms_gallery
	 * 
	 *
	 * @return Cms_gallery object
	 */
	public function get_cms_gallery() {
		
		if( empty( $this->cms_gallery ) ) {
			include_once "cms_gallery.php";
			$this->cms_gallery = new Cms_gallery( $this );
		}
		
		return $this->cms_gallery;
	}
	
	/**
	 * Get cms_photo
	 * 
	 *
	 * @return Cms_photo object
	 */
	public function get_cms_photo() {
		
		if( empty( $this->cms_photo ) ) {
			include_once "cms_photo.php";
			$this->cms_photo = new Cms_photo( $this );
		}
		
		return $this->cms_photo;
	}
	
	/**
	 * Get cms_category
	 * 
	 *
	 * @return Cms_category object
	 */
	public function get_cms_category() {
		
		if( empty( $this->cms_category ) ) {
			include_once "cms_category.php";
			$this->cms_category = new Cms_category( $this );
		}
		
		return $this->cms_category;
	}
	
	/**
	 * Get cms_image
	 * 
	 *
	 * @return Cms_image object
	 */
	public function get_cms_image() {
		
		if( empty( $this->cms_image ) ) {
			include_once "cms_image.php";
			$this->cms_image = new Cms_image( $this );
		}
		
		return $this->cms_image;
	}
	
	/**
	 * Get cms_homepage
	 * 
	 *
	 * @return Cms_homepage object
	 */
	public function get_cms_homepage() {
		
		if( empty( $this->cms_homepage ) ) {
			include_once "cms_homepage.php";
			$this->cms_homepage = new Cms_homepage( $this );
		}
		
		return $this->cms_homepage;
	}
	
	/**
	 * Get cms_calendar
	 * 
	 *
	 * @return Cms_calendar object
	 */
	public function get_cms_calendar() {
		
		if( empty( $this->cms_calendar ) ) {
			include_once "cms_calendar.php";
			$this->cms_calendar = new Cms_calendar( $this );
		}
		
		return $this->cms_calendar;
	}

	/**
	 * Get cms_pollution
	 * 
	 *
	 * @return Cms_pollution object
	 */
	public function get_cms_pollution() {
		
		if( empty( $this->cms_pollution ) ) {
			include_once "cms_pollution.php";
			$this->cms_pollution = new Cms_pollution( $this );
		}
		
		return $this->cms_pollution;
	}
	
	/**
	 * Get cms_item_list
	 * 
	 *
	 * @return Cms_item_list object
	 */
	public function get_cms_item_list() {
		
		if( empty( $this->cms_item_list ) ) {
			include_once "cms_item_list.php";
			$this->cms_item_list = new Cms_item_list( $this );
		}
		
		return $this->cms_item_list;
	}

}

?>