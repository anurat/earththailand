<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS pollution class
 * 
 * a class for managing pollution mark in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 09/21/14
 *
 */
class Cms_pollution {

	private $cms_model;
	
	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * generate where clause
	 *
	 * @param string $query
	 *
	 * @return string
	 */
	private function _generate_query( $query ) {
	
		if( empty( $query ) ) {
			return '0';
		}
		$where = '';
		
		$qs = explode( ' ,', $query );
		foreach( $qs as $q ) {
			$where = "( title LIKE '%{$q}%' OR
					description LIKE '%{$q}%' ) AND ";
		}
		
		$where = substr( $where, 0, strlen( $where ) -4 );
		return $where;
	}
	
	/**
	 * Search from query string
	 *
	 * @param string $query
	 *
	 * @return Pollution object array
	 */
	public function search( $query ) {

		$where = $this->_generate_query( $query );

		$con = Propel::getConnection();
		$sql = "SELECT p.* 
				FROM pollution p, item_list il
				WHERE il.table = 'pollution'
				AND p.pollution_id = il.table_id
				AND {$where}
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return PollutionPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get a list of pollutions
	 *
	 * @param string $display
	 * @param int $index
	 * @param int $no
	 *
	 * @return Pollution object array
	 */
	public function get_pollution_list( $display='all', $index=0, $no=10 ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT p.* 
				FROM pollution p, item_list il
				WHERE il.table = 'pollution'
				AND p.pollution_id = il.table_id
				{$enabled}
				ORDER BY il.order DESC
                LIMIT {$no}
                OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        return PollutionPeer::populateObjects( $stmt );
	}
    
    /**
     * Get a list of pollutions by type
     *
     * @param int $type
     * @param int $index
     * @param int $no
     *
     * @return Pollution object array
     */
    public function get_pollutions_by_type( $type, $index, $no ) {
        
		$con = Propel::getConnection();
		$sql = "SELECT p.* 
				FROM pollution p, item_list il
				WHERE il.table = 'pollution'
				AND p.pollution_id = il.table_id
				AND il.enable = 1
                AND p.type = '{$type}'
				ORDER BY il.order DESC
                LIMIT {$no}
                OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        return PollutionPeer::populateObjects( $stmt );
    }
	
    /**
     * Count a list of pollutions by type
     *
     * @param int $type
     *
     * @return Pollution object array
     */
    public function count_pollutions_by_type( $type ) {
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*)
				FROM pollution p, item_list il
				WHERE il.table = 'pollution'
				AND p.pollution_id = il.table_id
				AND il.enable = 1
                AND p.type = '{$type}'";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
    }
	
	/**
	 * Count the number of pollutions
	 *
	 * @param string $display
	 *
	 * @return int
	 */
	public function count_pollution_list( $display='all' ) {
		return PollutionQuery::create()
				->_if( $display == 'enabled' )
					->filterBy_enable( 1 )
				->_endif()
				->count();
	}
	
	/**
	 * Get the number of pollutions
	 *
	 * @return int
	 */
	public function get_pollution_no() {
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM pollution p, item_list il
				WHERE il.table = 'pollution'
				AND p.pollution_id = il.table_id";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get an pollution
	 * 
	 * @param int $pollution_id
	 *
	 * @return Pollution object
	 */
	public function get_pollution( $pollution_id ) {
		return PollutionQuery::create()->findPk( $pollution_id );
	}
	
	/**
	 * Get a pollution by title
	 *
	 * @param string $title
	 *
	 * @return Pollution object
	 */
	public function get_pollution_by_title( $title ) {
		return PollutionQuery::create()
				->filterBy_title( $title )
				->findOne();
	}
	
	/**
	 * Get the maximum number of order
	 *
	 * @return int
	 */
	public function get_max_order() {
		$con = Propel::getConnection();
		$sql = "SELECT MAX( p.order )
				FROM pollution p";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Get next value of pollution ID
	 *
	 * @return int
	 */
	public function get_next_id() {
	
		$con = Propel::getConnection();
		$sql = "SELECT AUTO_INCREMENT
				FROM  information_schema.TABLES
				WHERE TABLE_SCHEMA = 'nichemktas_tea'
				AND TABLE_NAME = 'pollution'";

		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Enable pollution
	 *
	 * @param int $pollution_id
	 */
	public function enable( $pollution_id ) {
		
		$pollution = $this->get_pollution( $pollution_id );
		$pollution->set_enable( 1 );
		$pollution->save();
	}
	
	/**
	 * Disable pollution
	 *
	 * @param int $pollution_id
	 */
	public function disable( $pollution_id ) {
		
		$pollution = $this->get_pollution( $pollution_id );
		$pollution->set_enable( 0 );
		$pollution->save();
	}
	
	/**
	 * Search pollutions
	 *
	 * @param string $search
	 *
	 * @return Pollution object array
	 */
	public function ajax_search( $search ) {
		return PollutionQuery::create()
				->filterBy_title( "%{$search}%" )
				->limit( 10 )
				->find();
	}
	
	/**
	 * Add new pollution
	 *
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $list_title
	 * @param string $list_desc
	 * @param int $pollution_date
	 * @param string $type
	 * @param string $mark_img
	 * @param string $x
	 * @param string $y
	 * 
	 * @return Pollution object
	 */
	public function add_pollution( $title, $video_code, $description, $list_title, $list_desc, $pollution_date,
			$type, $mark_img, $x, $y ) {
	
		$pollution = new Pollution();
		$pollution->set_title( $title );
		$pollution->set_video_code( $video_code );
		$pollution->set_description( $description );
		$pollution->set_list_title( $list_title );
		$pollution->set_list_desc( $list_desc );
		$pollution->set_pollution_date( $pollution_date );
		$pollution->set_type( $type );
		$pollution->set_mark_img( $mark_img );
		$pollution->set_x( $x );
		$pollution->set_y( $y );
		$pollution->set_created_date( time() );
		$pollution->save();
		
		return $pollution;
	}
	
	/**
	 * Update pollution
	 *
	 * @param int $pollution_id
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $list_title
	 * @param string $list_desc
	 * @param int $pollution_date
	 * @param string $type
	 * @param string $mark_img
	 * @param string $x
	 * @param string $y
	 * 
	 * @return Pollution object
	 */
	public function update_pollution( $pollution_id, $title, $video_code, $description, $list_title, $list_desc, 
			$pollution_date, $type, $mark_img, $x, $y ) {
		
		$pollution = PollutionQuery::create()->findPk( $pollution_id );
		$pollution->set_title( $title );
		$pollution->set_video_code( $video_code );
		$pollution->set_description( $description );
		$pollution->set_list_title( $list_title );
		$pollution->set_list_desc( $list_desc );
		$pollution->set_pollution_date( $pollution_date );
		$pollution->set_type( $type );
		$pollution->set_mark_img( $mark_img );
		$pollution->set_x( $x );
		$pollution->set_y( $y );
		$pollution->set_created_date( time() );
		$pollution->save();
		
		return $pollution;
	}
	
	/**
	 * Update pollution orders
	 *
	 * @param array $pollution
	 * @param array $order
	 *
	 */
	public function update_orders( $pollution, $order ) {
	
		$max_order = max( $order );
		foreach( $pollution as $i => $pollution_id ) {
			$pollution = $this->get_pollution( $pollution_id );
			$pollution->set_order( $max_order -$i );
			$pollution->save();
		}
	}
		
	/**
	 * Delete pollution
	 *
	 * @param int $pollution_id
	 *
	 */
	public function delete_pollution( $pollution_id ) {
		PollutionQuery::create()->findPk( $pollution_id )->delete();
	}
}

?>