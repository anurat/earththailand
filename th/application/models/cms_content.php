<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS content class
 * 
 * a class for managing content in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 08/11/14
 *
 */
abstract class Cms_content {

	private $cms_model;
	
	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * Get a list of contents
	 *
	 * @param string $display
	 *
	 * @return object array
	 */
	public function get_list( $display="all" );
	
	/**
	 * Get a list of contents with the category only
	 *
	 * @return object array
	 */
	public function get_cat_list();
	
	/**
	 * Get no. of contents
	 *
	 * @return int
	 */
	public function get_no();

	/**
	 * Get a content
	 * 
	 * @param int $id
	 *
	 * @return object
	 */
	public function get_content( $id );
	
	/**
	 * Get the maximum number of order
	 *
	 * @return int
	 */
	public function get_max_order();
	
	/**
	 * Enable content
	 *
	 * @param int $id
	 */
	public function enable( $id );
	
	/**
	 * Disable content
	 *
	 * @param int $id
	 */
	public function disable( $id );
	
	/**
	 * Update content list order
	 *
	 * @param string $order
	 */
	public function update_orders( $order );
		
	/**
	 * Delete content
	 *
	 * @param int $id
	 *
	 */
	public function delete_content( $id );
}

?>