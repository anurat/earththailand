<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CMS gallery class
 * 
 * a class for managing gallery in Thaiecoalert project
 * 
 * @author Anurat Chapanond
 * 
 * @copyright Connectiv Co., Ltd.
 * 
 * @version 1.0
 * 
 * @since 07/17/14
 *
 */
class Cms_gallery {

	private $cms_model;

	/**
	 * Constructor
	 *
	 */
	function __construct( $_cms_model ) {
		$this->cms_model = $_cms_model;
	}
	
	/**
	 * generate where clause
	 *
	 * @param string $query
	 *
	 * @return string
	 */
	private function _generate_query( $query ) {
	
		if( empty( $query ) ) {
			return '0';
		}
		$where = '';
		
		$qs = explode( ' ,', $query );
		foreach( $qs as $q ) {
			$where = "( title LIKE '%{$q}%' OR
					description LIKE '%{$q}%' ) AND ";
		}
		
		$where = substr( $where, 0, strlen( $where ) -4 );
		return $where;
	}
	
	/**
	 * Search from query string
	 *
	 * @param string $query
	 *
	 * @return Gallery object array
	 */
	public function search( $query ) {
	
		$where = $this->_generate_query( $query );
	
		$con = Propel::getConnection();
		$sql = "SELECT g.* 
				FROM gallery g, item_list il
				WHERE il.table = 'gallery'
				AND g.gallery_id = il.table_id
				AND {$where}
				ORDER BY il.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return GalleryPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get a list of galleries
	 *
	 * @param string $display
	 * @param int $index
	 * @param int $no
	 *
	 * @return Gallery object array
	 */
	public function get_gallery_list( $display='all', $index=0, $no=10 ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT g.* 
				FROM gallery g, item_list il
				WHERE il.table = 'gallery'
				AND g.gallery_id = il.table_id
				{$enabled}
				ORDER BY il.order DESC
                LIMIT {$no}
                OFFSET {$index}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return GalleryPeer::populateObjects( $stmt );
	}
	
	/**
	 * Count the number of galleries
	 *
	 * @param string $display
	 *
	 * @return int
	 */
	public function count_gallery_list( $display='all' ) {
        
        $enabled = ( $display == 'enabled' )? 'AND il.enable = 1': '';
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*)
				FROM gallery g, item_list il
				WHERE il.table = 'gallery'
				AND g.gallery_id = il.table_id
				{$enabled}";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}
	
	/**
	 * Get a list of galleries with gallery category only
	 *
	 * @return Gallery object array
	 */
	public function get_gallery_cat_list() {
	
		$this->cms_model->get_cms_category();
		$category_id = Cms_Category::GALLERY_CAT;
	
		$con = Propel::getConnection();
		$sql = "SELECT DISTINCT g.* 
				FROM gallery g, cat_link cl
				WHERE cl.table = 'gallery'
				AND g.gallery_id = cl.table_id
				AND cl.category_id = '{$category_id}'
				AND g.enable = 1
				ORDER BY g.order DESC";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
		return GalleryPeer::populateObjects( $stmt );
	}
	
	/**
	 * Get the number of gallerys
	 *
	 * @return int
	 */
	public function get_gallery_no() {
        
		$con = Propel::getConnection();
		$sql = "SELECT COUNT(*) 
				FROM gallery g, item_list il
				WHERE il.table = 'gallery'
				AND g.gallery_id = il.table_id";
		$stmt = $con->prepare( $sql );
		$stmt->execute();
        $result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get a gallery
	 * 
	 * @param int $gallery_id
	 *
	 * @return Gallery object
	 */
	public function get_gallery( $gallery_id ) {
		return GalleryQuery::create()->findPk( $gallery_id );
	}
	
	/**
	 * Get a gallery
	 *
	 * @param string $title
	 *
	 * @return Gallery object
	 */
	public function get_gallery_by_title( $title ) {
		return GalleryQuery::create()
				->filterBy_title( $title )
				->findOne();
	}
	
	/**
	 * Get next value of gallery ID
	 *
	 * @return int
	 */
	public function get_next_id() {

		$con = Propel::getConnection();
		$sql = "SELECT AUTO_INCREMENT
				FROM  information_schema.TABLES
				WHERE TABLE_SCHEMA = 'nichemktas_tea'
				AND TABLE_NAME = 'gallery'";

		$stmt = $con->prepare( $sql );
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0][0];
	}

	/**
	 * Get gallery photos
	 *
	 * @param int $gallery_id
	 *
	 * @return Image object array
	 */
	public function get_gallery_photos( $gallery_id ) {
	
		return PhotoQuery::create()
				->filterBy_gallery_id( $gallery_id )
				->orderBy_order()
				->find();
	}
	
	/**
	 * Search gallery
	 *
	 * @param string $search
	 *
	 * @return Gallery object array
	 */
	public function ajax_search( $search ) {
		return GalleryQuery::create()
				->filterBy_title( "%{$search}%" )
				->limit( 10 )
				->find();
	}
	
	/**
	 * Add new gallery
	 *
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $attachment
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * @param int $gallery_date
	 * 
	 * @return Gallery object
	 */
	public function add_gallery( $title, $video_code, $description, $attachment,
			$list_title, $list_desc, $list_img, $gallery_date ) {
	
		$gallery = new Gallery();
		$gallery->set_title( $title );
		$gallery->set_video_code( $video_code );
		$gallery->set_description( $description );
		$gallery->set_attachment( $attachment );
		$gallery->set_list_title( $list_title );
		$gallery->set_list_desc( $list_desc );
		$gallery->set_list_img( $list_img );
		$gallery->set_gallery_date( $gallery_date );
		$gallery->set_created_date( time() );
		$gallery->save();
		
		return $gallery;
	}
	
	/**
	 * Update gallery
	 *
	 * @param int $gallery_id
	 * @param string $title
	 * @param string $video_code
	 * @param string $description
	 * @param string $attachment
	 * @param string $list_title
	 * @param string $list_desc
	 * @param string $list_img
	 * @param int $gallery_date
	 * 
	 * @return Gallery object
	 */
	public function update_gallery( $gallery_id, $title, $video_code, $description, $attachment,
			$list_title, $list_desc, $list_img, $gallery_date ) {
		
		$gallery = GalleryQuery::create()->findPk( $gallery_id );
		$gallery->set_title( $title );
		$gallery->set_video_code( $video_code );
		$gallery->set_description( $description );
		$gallery->set_attachment( $attachment );
		$gallery->set_list_title( $list_title );
		$gallery->set_list_desc( $list_desc );
		$gallery->set_list_img( $list_img );
		$gallery->set_gallery_date( $gallery_date );
		$gallery->save();
		
		return $gallery;
	}
	
	/**
	 * Update photo list in database for gallery ID
	 *
	 * @param int $gallery_id
	 *
	 */
	public function update_from_folder( $gallery_id ) {
	
		$path = APPPATH .'../userfiles/Gallery/' .$gallery_id .'/';
		$files = scandir( $path );
		$photos = $this->get_gallery_photos( $gallery_id );
		
		// delete photo from database if files do not exist
		foreach( $photos as $photo ) {
			if( !in_array( $photo->get_path(), $files ) ) {
				$photo->delete();
			}
		}
		
		// update photo from folder
		$cms_photo = $this->cms_model->get_cms_photo();
		foreach( $files as $file ) {
			if( $this->_has_photo( $gallery_id, $file ) ) {
			}
			else {
				if( in_array( $file, array( '.', '..' ) ) ) {
					continue;
				}
				$cms_photo->add_photo( $gallery_id, '', '', $file, time() );
			}
		}
	}
	
	/**
	 * Check whether the folder has old photo
	 *
	 * @param int $gallery_id
	 * @param string $file
	 *
	 * @return boolean
	 */
	private function _has_photo( $gallery_id, $file ) {
		$photos = $this->get_gallery_photos( $gallery_id );
		foreach( $photos as $photo ) {
			if( $photo->get_path() == $file ) {
				return true;
			}
		}
		
		return false;
	}
		
	/**
	 * Delete gallery
	 *
	 * @param int $gallery_id
	 *
	 */
	public function delete_gallery( $gallery_id ) {
		GalleryQuery::create()->findPk( $gallery_id )->delete();
	}
}

?>