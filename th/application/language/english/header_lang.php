<?php

$lang['header_welcome'] = 'Welcome';
$lang['header_signedin'] = 'Signed in at';

$lang['header_save_close'] = 'SAVE & CLOSE';
$lang['header_save_new'] = 'SAVE & NEW';
$lang['header_save'] = 'SAVE';
$lang['header_cancel'] = 'CANCEL';
$lang['header_upload'] = 'UPLOAD';
$lang['header_add'] = 'ADD';
$lang['header_delete'] = 'DELETE';
$lang['header_remove'] = 'REMOVE';
$lang['header_showall'] = 'SHOW ALL';
$lang['header_on'] = 'ON';
$lang['header_off'] = 'OFF';
$lang['header_clear'] = 'clear';
$lang['header_search'] = 'SEARCH';
$lang['header_manage'] = 'MANAGE';
$lang['header_move'] = 'MOVE';
$lang['header_reset'] = 'RESET';

$lang['header_category'] = 'Category';
$lang['header_item'] = 'Item';

/* End of file header_lang.php */
/* Location: ./system/language/english/header_lang.php */