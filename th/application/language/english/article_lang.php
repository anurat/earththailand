<?php

$lang['article_no'] = 'No. of articles';
$lang['article_title'] = 'Title';
$lang['article_image'] = 'Image';
$lang['article_image_folder'] = 'Please upload images into folder';
$lang['article_image_no'] = 'No. of images';
$lang['article_image_upload'] = 'support JPG, GIF, PNG file extension and recommended image size is 800 x 350 pixels and file size less than 2MB';
$lang['article_image_desc'] = 'Image Description';
$lang['article_video_code'] = 'Video Code';
$lang['article_description'] = 'Full Description';
$lang['article_tags'] = 'Tag (please put comma between each keyword)';
$lang['article_attachment'] = 'Attachment (support doc, docx, ppt, pptx, pdf file extension) file size less than 20MB';
$lang['article_list_title'] = 'Item Title';
$lang['article_list_desc'] = 'Item Description';
$lang['article_list_img'] = 'Item Image';
$lang['article_date'] = 'Article Date';
$lang['article_newline'] = 'Please type &lt;br /&gt; to add new line';

/* End of file article_lang.php */
/* Location: ./system/language/english/article_lang.php */