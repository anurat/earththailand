<?php

$lang['pollution_no'] = 'No. of pollutions';
$lang['pollution_title'] = 'Title';
$lang['pollution_video_code'] = 'Video Code';
$lang['pollution_description'] = 'Full Description';
$lang['pollution_tags'] = 'Tag (please put comma between each keyword)';
$lang['pollution_list_title'] = 'Item Title';
$lang['pollution_list_desc'] = 'Item Description';
$lang['pollution_date'] = 'Pollution Date';
$lang['pollution_newline'] = 'Please type &lt;br /&gt; to add new line';

$lang['pollution_marker'] = 'Marker';
$lang['pollution_type'] = 'Type';
$lang['pollution_mark_img'] = 'Marker Image';
$lang['pollution_coordinate'] = 'Coordinate';
$lang['pollution_x'] = 'X';
$lang['pollution_y'] = 'Y';

/* End of file pollution_lang.php */
/* Location: ./system/language/english/pollution_lang.php */