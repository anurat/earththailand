<?php

$lang['calendar_no'] = 'No. of calendar activities';
$lang['calendar_title'] = 'Title';
$lang['calendar_video_code'] = 'Video Code';
$lang['calendar_description'] = 'Full Description';
$lang['calendar_tags'] = 'Tag (please put comma between each keyword)';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['calendar_newline'] = 'Please type &lt;br /&gt; to add new line';

/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */