<?php

$lang['category_update_info'] = 'UPDATE INFO';
$lang['category_item_no'] = 'No. of items';

$lang['category_update_category'] = 'Update Category';
$lang['category_title'] = 'Title';
$lang['category_images'] = 'Images';
$lang['category_image_upload'] = 'support JPG, GIF, PNG file extension and recommended image size is 800 x 350 pixels and file size less than 2MB';
$lang['category_description'] = 'Full Description';
$lang['category_tags'] = 'Tag (please put comma between each keyword)';
$lang['category_date'] = 'Category Date';

/* End of file category_lang.php */
/* Location: ./system/language/english/category_lang.php */