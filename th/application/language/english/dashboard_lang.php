<?php

$lang['dashboard_dashboard'] = 'DASHBOARD';

$lang['dashboard_article_no'] = 'Summary of the number of items';
$lang['dashboard_group'] = 'Group';
$lang['dashboard_no'] = 'No (items)';

$lang['dashboard_quick_icon'] = 'QUICK ICON';
$lang['dashboard_statistics'] = 'Website Statistics';

/* End of file dashboard_lang.php */
/* Location: ./system/language/english/dashboard_lang.php */