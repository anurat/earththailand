<?php

$lang['link_title'] = 'Title';
$lang['link_link'] = 'Link';
$lang['link_thai_link'] = 'Thai Link';
$lang['link_regional_link'] = 'Regional Link';
$lang['link_global_link'] = 'Global Link';
$lang['link_no'] = 'No. of links:';

/* End of file link_lang.php */
/* Location: ./system/language/english/link_lang.php */