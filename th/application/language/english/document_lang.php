<?php

$lang['document_no'] = 'No. of documents';
$lang['document_title'] = 'Title';
$lang['document_image'] = 'Image';
$lang['document_image_folder'] = 'Please upload images into folder';
$lang['document_image_no'] = 'No. of images';
$lang['document_image_upload'] = 'support JPG, GIF, PNG file extension and recommended image size is 800 x 350 pixels and file size less than 2MB';
$lang['document_video_code'] = 'Video Code';
$lang['document_description'] = 'Full Description';
$lang['document_tags'] = 'Tag (please put comma between each keyword)';
$lang['document_list_title'] = 'Item Title';
$lang['document_list_desc'] = 'Item Description';
$lang['document_list_img'] = 'Item Image';
$lang['document_date'] = 'Document Date';
$lang['document_newline'] = 'Please type &lt;br /&gt; to add new line';

/* End of file document_lang.php */
/* Location: ./system/language/english/document_lang.php */