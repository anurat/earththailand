<?php

$lang['header_welcome'] = 'ยินดีต้อนรับ';
$lang['header_signedin'] = 'เข้าสู่ระบบเมื่อ';

$lang['header_save_close'] = 'บันทึกและปิด';
$lang['header_save_new'] = 'บันทึกและสร้างใหม่';
$lang['header_save'] = 'บันทึก';
$lang['header_cancel'] = 'ยกเลิก';
$lang['header_upload'] = 'อัพโหลด';
$lang['header_add'] = 'เพิ่ม';
$lang['header_delete'] = 'ลบ';
$lang['header_remove'] = 'เอาออก';
$lang['header_showall'] = 'แสดงทั้งหมด';
$lang['header_on'] = 'เปิด';
$lang['header_off'] = 'ปิด';
$lang['header_clear'] = 'เคลียร์';
$lang['header_search'] = 'ค้นหา';
$lang['header_manage'] = 'จัดการ';
$lang['header_move'] = 'ย้าย';
$lang['header_reset'] = 'รีเซ็ท';

$lang['header_category'] = 'กลุ่มหน้าเว็บ';
$lang['header_item'] = 'รายการ';

/* End of file header_lang.php */
/* Location: ./system/language/thai/header_lang.php */