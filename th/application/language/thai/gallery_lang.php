<?php

$lang['gallery_title'] = 'หัวข้อ';
$lang['gallery_image_folder'] = 'กรุณาอัพโหลดรูปภาพไปที่โฟลเดอร์';
$lang['gallery_image_no'] = 'จำนวนรูปภาพ';
$lang['gallery_image_upload'] = 'รองรับไฟล์ JPG, GIF, PNG และขนาดภาพที่เหมาะสมคือ 800 x 350 pixels ขนาดไฟล์ไม่เกิน 2MB';
$lang['article_image_desc'] = 'คำอธิบายรูปภาพ';
$lang['gallery_video_code'] = 'คำสั่งวีดีโอ';
$lang['gallery_description'] = 'คำอธิบายฉบับเต็ม';
$lang['gallery_attachment'] = 'เอกสารแนบ (รองรับไฟล์ .doc, .docx, .ppt, .pptx, .pdf) ขนาดไฟล์ไม่เกิน 20MB';
$lang['gallery_tags'] = 'แท็ก (กรุณาใส่เครื่องหมาย , ระหว่างคำ)';
$lang['gallery_list_title'] = 'หัวข้อรายการ';
$lang['gallery_list_desc'] = 'คำอธิบายรายการ';
$lang['gallery_list_img'] = 'รูปภาพรายการ';
$lang['gallery_date'] = 'วันที่ห้องภาพ';
$lang['gallery_newline'] = 'กรุณาพิมพ์ &lt;br /&gt; เพื่อขึ้นบรรทัดใหม่';

$lang['gallery_image_folder'] = 'กรุณาอัพโหลดรูปภาพไปที่โฟลเดอร์';
$lang['gallery_image_no'] = 'จำนวนรูปภาพ';

/* End of file gallery_lang.php */
/* Location: ./system/language/thai/gallery_lang.php */