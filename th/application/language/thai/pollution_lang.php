<?php

$lang['pollution_no'] = 'จำนวนจุดมลพิษ';
$lang['pollution_title'] = 'หัวข้อ';
$lang['pollution_video_code'] = 'คำสั่งวีดีโอ';
$lang['pollution_description'] = 'คำอธิบายฉบับเต็ม';
$lang['pollution_tags'] = 'แท็ก (กรุณาใส่เครื่องหมาย , ระหว่างคำ)';
$lang['pollution_list_title'] = 'หัวข้อรายการ';
$lang['pollution_list_desc'] = 'คำอธิบายรายการ';
$lang['pollution_date'] = 'วันที่มลพิษ';
$lang['pollution_newline'] = 'กรุณาพิมพ์ &lt;br /&gt; เพื่อขึ้นบรรทัดใหม่';

$lang['pollution_marker'] = 'หมุด';
$lang['pollution_type'] = 'ประเภทมลพิษ';
$lang['pollution_mark_img'] = 'รูปหมุด';
$lang['pollution_coordinate'] = 'ตำแหน่ง';
$lang['pollution_x'] = 'X';
$lang['pollution_y'] = 'Y';

/* End of file pollution_lang.php */
/* Location: ./system/language/thai/pollution_lang.php */