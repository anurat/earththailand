<?php

$lang['profiles_name'] = 'ชื่อ';
$lang['profiles_email'] = 'อีเมล';
$lang['profiles_password'] = 'รหัสผ่าน';
$lang['profiles_change_password'] = 'เปลี่ยนรหัสผ่าน';

$lang['profiles_old_password'] = 'รหัสผ่านเก่า';
$lang['profiles_new_password'] = 'รหัสผ่านใหม่';
$lang['profiles_retype_password'] = 'พิมพ์รหัสผ่านอีกครั้ง';

/* End of file profiles_lang.php */
/* Location: ./system/language/thai/profiles_lang.php */