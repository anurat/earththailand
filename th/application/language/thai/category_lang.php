<?php

$lang['category_update_info'] = 'แก้ไขข้อมูล';
$lang['category_item_no'] = 'จำนวนเนื้อหา';

$lang['category_update_category'] = 'แก้ไขหัวข้อ';
$lang['category_title'] = 'หัวข้อ';
$lang['category_images'] = 'รูปภาพ';
$lang['category_image_upload'] = 'รองรับไฟล์ JPG, GIF, PNG และขนาดภาพที่เหมาะสมคือ 800 x 350 pixels ขนาดไฟล์ไม่เกิน 2MB';
$lang['category_description'] = 'คำอธิบายฉบับเต็ม';
$lang['category_tags'] = 'แท็ก (กรุณาใส่เครื่องหมาย , ระหว่างคำ)';
$lang['category_date'] = 'วันที่กลุ่มบทความ';

/* End of file category_lang.php */
/* Location: ./system/language/thai/category_lang.php */