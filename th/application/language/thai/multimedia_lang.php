<?php

$lang['multimedia_title'] = 'หัวข้อ';
$lang['multimedia_description1'] = 'คำอธิบายด้านบน';
$lang['multimedia_video_code'] = 'คำสั่งวีดีโอ';
$lang['multimedia_description2'] = 'คำอธิบายด้านล่าง';
$lang['multimedia_list_title'] = 'หัวข้อในรายการ';
$lang['multimedia_list_desc'] = 'คำอธิบายในรายการ';
$lang['multimedia_list_img'] = 'รูปภาพรายการ';
$lang['multimedia_tags'] = 'แท็ก';
$lang['multimedia_newline'] = 'กรุณาพิมพ์ &lt;br /&gt; เพื่อขึ้นบรรทัดใหม่';

/* End of file multimedia_lang.php */
/* Location: ./system/language/english/multimedia_lang.php */