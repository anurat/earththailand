<div id="article" >
	<div>
		<h1><?php echo $category->get_title(); ?></h1>
		
<?php
if( sizeof( $images ) > 0 ) {
?>
		<script src="<?php echo site_url( "/js/galleria/galleria-1.4.2.min.js" ); ?>" ></script>
		<script>
			Galleria.loadTheme('<?php echo site_url( "/js/galleria/themes/azur/galleria.azur.min.js" ); ?>');
			Galleria.run('.galleria', {
				autoplay: 10000,
				imageCrop: false,
				thumbCrop: false,
				transition: 'slide'
			});
		</script>
		<div class="galleria" id="galleria" >
<?php
	foreach( $images as $image ) {
?>
			<a href="<?php echo site_url( "/userfiles/" .$category_path ."/" .$image->get_path() ); ?>"
					><img src="<?php echo site_url( "/userfiles/_thumbs/" .$category_path ."/" .$image->get_path() ); ?>" 
							data-title="<?php echo $image->get_name(); ?>" 
							data-description="<?php echo $image->get_description(); ?>" alt="" ></a>
<?php
	}
?>
		</div>
<?php
}
?>

		<br />
		<?php echo $category->get_description(); ?>
		<div class="end_desc" >&nbsp;</div>
		
<?php
if( sizeof( $cat_links ) > 0 ) {
?>
		<div class="items" >
<?php
	$cms_article = $this->cms_model->get_cms_article();
	$cms_document = $this->cms_model->get_cms_document();
	$cms_gallery = $this->cms_model->get_cms_gallery();
	$cms_multimedia = $this->cms_model->get_cms_multimedia();
	foreach( $cat_links as $cat_link ) {

		$table = $cat_link->get_table();
		$table_id = $cat_link->get_table_id();
		$obj = null;
		switch( $table ) {
		case 'article':
			$obj = $cms_article->get_article( $table_id );
			break;
		case 'document':
			$obj = $cms_document->get_document( $table_id );
			break;
		case 'gallery':
			$obj = $cms_gallery->get_gallery( $table_id );
			break;
		case 'multimedia':
			$obj = $cms_multimedia->get_multimedia( $table_id );
			break;
		}
?>
			<div>
				<a href="<?php echo site_url( "{$table}/" .$table_id ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$obj->get_list_img() ); ?>" alt="" /></a>
				<h4><a href="<?php echo site_url( "{$table}/" .$table_id ); ?>" 
						target="_blank" ><?php echo $obj->get_list_title(); ?></a></h4>
				<span class="text" ><?php echo $obj->get_list_desc(); ?></span>
				<span class="readmore" ><a href="<?php echo site_url( "{$table}/" .$table_id ); ?>" 
						target="_blank" >อ่านต่อ...</a></span>
			</div>
<?php
	}
?>
		</div>
<?php
}

include "social_media.php";
?>
	</div>
</div>