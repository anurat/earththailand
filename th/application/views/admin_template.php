<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	
	<link href="<?php echo site_url( 'images/favicon.ico' ); ?>" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<link rel="stylesheet" href="<?php echo site_url( 'css/reset.css' ); ?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo site_url( 'css/admin_style.css' ); ?>" type="text/css" />
	
	<script src="<?php echo site_url( 'js/jquery-1.11.1.min.js' ); ?>"></script>
	<script src="<?php echo site_url( 'ckeditor/ckeditor.js' ); ?>"></script>
	<script src="<?php echo site_url( 'ckfinder/ckfinder.js' ); ?>"></script>
</head>
<body>
	<nav><?php include 'admin_menu.php'; ?></nav>
	<div id="page" >
		<header><?php include 'admin_header.php'; ?></header>
		<div id="content" ><?php include "{$content}.php"; ?></div>
		<footer><?php include 'admin_footer.php'; ?></footer>
	</div>
</body>
</html>