<div id="article" >
	<div>
		<h1><?php echo $article->get_title(); ?></h1>
		
<?php
$video_code = $article->get_video_code();
if( !empty( $video_code ) ) {
?>
		<div class="multimedia" >
			<?php echo $video_code; ?>
		</div>
<?php
}
else if( sizeof( $images ) > 0 ) {
?>
		<script src="<?php echo site_url( "/js/galleria/galleria-1.4.2.min.js" ); ?>" ></script>
		<script>
			Galleria.loadTheme('<?php echo site_url( "/js/galleria/themes/azur/galleria.azur.min.js" ); ?>');
			Galleria.run('.galleria', {
				autoplay: 10000,
				imageCrop: false,
				thumbCrop: false,
				transition: 'slide'
			});
		</script>
		<div class="galleria" id="galleria" >
<?php
	$article_id = $article->get_article_id();
	foreach( $images as $image ) {
?>
			<a href="<?php echo site_url( "/userfiles/Articles/" .$article_id ."/" .$image->get_path() ); ?>"
					><img src="<?php echo site_url( "/userfiles/_thumbs/Articles/" .$article_id ."/" .$image->get_path() ); ?>" 
							data-title="<?php echo $image->get_name(); ?>" 
							data-description="<?php echo $image->get_description(); ?>"
							alt="" ></a>
<?php
	}
?>
		</div>
<?php
}
?>
		
		<?php echo $article->get_description(); ?>
		<div class="end_desc" >&nbsp;</div>
		
<?php
$attachment = $article->get_attachment();
if( !empty( $attachment ) ) {
?>
		<?php echo $article->get_attachment(); ?>
		<div class="end_desc" >&nbsp;</div>
<?php
}

include "social_media.php";
?>
	</div>
</div>