<div id="calendar_page" >
	<div>
		<h1>ปฏิทินกิจกรรม</h1>
		<br />
		
		<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
		<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
		<script type="text/javascript" >
		$( function() {
			$("div.calendar_head > div").buttonset();

		});
		</script>
		
		<style>
		button.ui-button {
			width: auto;
		}
		
		div.calendar_head div.left_align {
			float: left;
		}
		div.calendar_head div.right_align {
			float: right;
		}
		div.calendar_head div.center {
			text-align: center;
		}
		div.ui-radio {
			display: inline-block;
		}
		</style>
		
		<div id="calendar" >
			<div class="calendar_head">
				<div class="left_align" >
					<button><</button><button>></button>
				</div>
				<div class="right_align" >
					<input type="radio" id="month_type" name="calendar_type" /><label for="month_type" >Month</label>
					<input type="radio" id="agenda_type" name="calendar_type" /><label for="agenda_type" >Agenda</label>
				</div>
				<div class="center" >
					August 2014
				</div>
			</div>
		</div>
		
		<?php echo $this->calendar->generate(2014, 8); ?>
	</div>
</div>