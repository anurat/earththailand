<div id="article" >
	<div>
		<h1><?php echo $document->get_title(); ?></h1>
		
<?php
$video_code = $document->get_video_code();
if( !empty( $video_code ) ) {
?>
		<div class="multimedia" >
			<?php echo $video_code; ?>
		</div>
<?php
}
else if( sizeof( $images ) > 0 ) {
?>
		<script src="<?php echo site_url( "/js/galleria/galleria-1.4.2.min.js" ); ?>" ></script>
		<script>
			Galleria.loadTheme('<?php echo site_url( "/js/galleria/themes/azur/galleria.azur.min.js" ); ?>');
			Galleria.run('.galleria', {
				autoplay: 10000,
				imageCrop: false,
				thumbCrop: false,
				transition: 'slide'
			});
		</script>
		<div class="galleria" id="galleria" >
<?php
	$document_id = $document->get_document_id();
	foreach( $images as $image ) {
?>
			<a href="<?php echo site_url( "/userfiles/Documents/" .$document_id ."/" .$image->get_path() ); ?>"
					><img src="<?php echo site_url( "/userfiles/_thumbs/Documents/" .$document_id ."/" .$image->get_path() ); ?>" 
							data-title="<?php echo $image->get_name(); ?>" 
							data-description="<?php echo htmlspecialchars( $image->get_description() ); ?>"
							alt="" ></a>
<?php
	}
?>
		</div>
<?php
}
?>
		
		<?php echo $document->get_description(); ?>
		<div class="end_desc" >&nbsp;</div>
		
<?php
include "social_media.php";
?>
	</div>
</div>