<?php
$op_lang = 'english';
if( $language == 'english' ) {
	$op_lang = 'thai';
}
?>
<div class="header_first_row" >
	<span class="language" >
		<a href="<?php echo site_url( 'admin/language/toggle' ); ?>" ><img 
				src="<?php echo site_url( "images/{$op_lang}" ); ?>.jpg" alt="flag" title="Toggle lanuage" /></a>
	</span>
	<span class="power" >
		<a href="<?php echo site_url( 'admin/signin/signout' ); ?>" ><img 
				src="<?php echo site_url( 'images/power.png' ); ?>" alt="Power" title="SIGN OUT" /></a>
	</span>
	<span class="welcome" >
	<?php
	if( !empty( $userid ) ) {
	?>
		<?php echo $this->lang->line( 'header_welcome' ); ?> <?php echo $email_address; ?><br />
		<?php echo $this->lang->line( 'header_signedin' ); ?> <?php echo ( empty( $signedin ) )? '': date( 'j/m/y g:ia', $signedin ); ?>
	<?php
	}
	?>
	</span>
	<span class="url" >
		<a href="http://www.earththailand.org" target="_blank" title="Browse www.EarthThailand.org" >WWW.EARTHTHAILAND.ORG</a>
	</span>
</div>
<div class="header_second_row" >
	online users: <?php echo ( empty( $onlines ) )? '': $onlines; ?>
</div>