<div class="pollution_events" >
	<div class="pollution" >
		<h3><a href="/th/pollutions" target="_blank" >แผนที่มลพิษ</a></h3>
		<a href="/th/pollutions" target="_blank" 
		        ><img src="<?php echo site_url( "images/map.jpg" ); ?>" alt="map" /></a>
	</div>
 
	<div class="events" >
		<h3><a href="/th/events" target="_blank" >ปฏิทินกิจกรรม</a></h3>
		<link rel='stylesheet' type='text/css' href="<?php echo site_url( "js/fullcalendar-2.0.2/fullcalendar.css" ); ?>" />
		<script src="<?php echo site_url( "js/jquery-ui-1.11.0.custom/jquery-ui.min.js" ); ?>"></script>
		<script src="<?php echo site_url( "js/fullcalendar-2.0.2/lib/moment.min.js" ); ?>"></script>
		<script src="<?php echo site_url( "js/fullcalendar-2.0.2/fullcalendar.js" ); ?>"></script>
		<script type="text/javascript" >
		$( function() {

			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next',
					center: 'title',
					right: 'today'
				},
				defaultView: 'month',
				editable: false,
				events: '<?php echo site_url( '/ajax/calendar' ); ?>',
				eventClick: function(e) {
					if(e.url) {
						window.open(e.url, "_blank");
						return false;
					}
				},
				timeFormat: ''
			});

		});
		</script>
		
		<div id="calendar"></div>
	</div>
</div>