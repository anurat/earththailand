<div class="two_columns" >
	<div class="column_one" >
		<h3><a href="/th/documents" target="_blank" >เอกสาร/ข้อมูลเผยแพร่</a></h3>
		<div class="items" >
<?php
foreach( $document_list as $i => $document ) {
	if( $i >= 15 ) {
		break;
	}
?>
			<div>
<?php
	$image = $document->get_image();
	if( !empty( $image ) ) {
?>
				<a href="<?php echo site_url( "document/" .$document->get_table_id() ); ?>" target="_blank"
					><img src="<?php echo site_url( "userfiles/" .$image ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "document/" .$document->get_table_id() ); ?>" target="_blank"
						><?php echo $document->get_title(); ?></a></h4>
				<div class="desc" ><?php echo $document->get_description(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "document/" .$document->get_table_id() ); ?>"
						target="_blank" >อ่านต่อ...</a></span>
			</div>
<?php
}
?>
		</div>

		<span class="moreinfo" ><a href="<?php echo site_url( "documents" ); ?>" >ดูเอกสาร/ข้อมูลเผยแพร่ทั้งหมด</a></span>
	</div>
 
	<div class="column_two" >
		<h3><a href="/th/articles" target="_blank" >ข่าว/บทความ</a></h3>
		<div class="items" >
<?php
foreach( $article_list as $i => $article ) {
	if( $i >= 15 ) {
		break;
	}
?>
			<div>
<?php
	$image = $article->get_image();
	if( !empty( $image ) ) {
?>
				<a href="<?php echo site_url( "article/" .$article->get_table_id() ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$image ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "article/" .$article->get_table_id() ); ?>" target="_blank"
						><?php echo $article->get_title(); ?></a></h4>
				<div class="desc" ><?php echo $article->get_description(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "article/" .$article->get_table_id() ); ?>"
						target="_blank" >อ่านต่อ...</a></span>
			</div>
<?php
}
?>
		</div>

		<span class="moreinfo" ><a href="<?php echo site_url( "articles" ); ?>" target="_blank" >ดูข่าว/บทความทั้งหมด</a></span>
	</div>
</div>