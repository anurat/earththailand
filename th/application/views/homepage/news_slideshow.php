<div class="news_slideshow">

	<div id="owl-news-slideshow" class="owl-carousel owl-theme">
<?php
foreach( $news_slideshow_list as $ns ) {
?>
		<div class="item">
			<div class="slide_left">
				<a href="<?php echo site_url( $ns->get_link_url() ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$ns->get_image() ); ?>" alt="" /></a>
			</div>
			<div class="slide_right">
				<div class="text">
					<h1><a href="<?php echo site_url( $ns->get_link_url() ); ?>" target="_blank"
							><?php echo $ns->get_title(); ?></a></h1>
					<?php echo $ns->get_description(); ?>
				</div>
			</div>
		</div>
	<?php
}
?>
	</div>
	<script type="text/javascript">
	$( function() {
		$("#owl-news-slideshow").owlCarousel({
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay: true,
			stopOnHover: true,
			navigation: true,
			navigationText : ["<",">"]
		});
	});
	</script>

</div>