<form action="<?php echo site_url( 'search' ); ?>" method="post" data-ajax="false" >

<table class="menu_search" >
<tr>
	<td>
		<!-- responsive menu -->
		<select data-role="none" >
			<option value="" selected="selected" >MENU/เมนู</option>
			<option value="<?php echo site_url( '' ); ?>" >หน้าหลัก</option>
			<option value="<?php echo site_url( 'pollutions' ); ?>" >แผนที่มลพิษ</option>
			<option value="<?php echo site_url( 'articles' ); ?>" >ข่าว/บทความ</option>
			<option value="<?php echo site_url( 'documents' ); ?>" >เอกสาร/ข้อมูลเผยแพร่</option>
			<option value="<?php echo site_url( 'gallery_list' ); ?>" >ห้องภาพ</option>
			<option value="<?php echo site_url( 'events' ); ?>" >กิจกรรมและสัมมนา</option>
			<option value="<?php echo site_url( 'about' ); ?>" >เกี่ยวกับมูลนิธิ</option>
			<option value="<?php echo site_url( 'tags' ); ?>" >แท็ก</option>
			<option value="<?php echo site_url( "earth1" ); ?>" >สิทธิการเข้าถึงข้อมูลมลพิษ (PRTR)</option>
			<option value="<?php echo site_url( "earth2" ); ?>" >วิทยาศาสตร์ภาคพลเมือง</option>
			<option value="<?php echo site_url( "earth3" ); ?>" >ขยะอุตสาหกรรมและสารเคมีอันตราย</option>
			<option value="<?php echo site_url( "earth4" ); ?>" >มาบตาพุดศึกษา</option>
			<option value="<?php echo site_url( "earth5" ); ?>" >สารเคมีในวงจรผลิตภัณฑ์</option>
			<option value="<?php echo site_url( "earth7" ); ?>" >ภาคธุรกิจและความรับผิดชอบ</option>
			<option value="<?php echo site_url( "earth6" ); ?>" >ข้อมูลนโยบายและกฎหมาย</option>
			<option value="<?php echo site_url( "multimedia_list" ); ?>" >มัลติมีเดีย/Multimedia</option>
			
			<option value="" disabled="disabled" >เครือข่ายระดับประเทศ</option>
<?php
foreach( $thai_link_list as $link ) {
?>
			<option value="<?php echo $link->get_link_url(); ?>"
					><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
	
			<option value="" disabled="disabled" >เครือข่ายระดับภูมิภาค</option>
<?php
foreach( $regional_link_list as $link ) {
?>
			<option value="<?php echo $link->get_link_url(); ?>"
					><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
			<option value="" disabled="disabled" >เครือข่ายระดับสากล</option>
<?php
foreach( $global_link_list as $link ) {
?>
			<option value="<?php echo $link->get_link_url(); ?>"
					><?php echo $link->get_link_name(); ?></option>
<?php
}
?>
		</select>
	</td>
	<td>
		<input id="text-search" name="text-search" data-role="none" maxlength="20"
			   type="text" size="10" placeholder="ค้นหา" />
	</td>
	<td>
		<input type="submit" data-role="none" value="SEARCH" />
	</td>
</tr>
</table>

</form>
