<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
// init
var site_url = '<?php echo site_url(); ?>';
var table = '<?php echo $table; ?>';
var segment1 = '<?php echo $this->uri->segment(1); ?>';
var segment2 = '<?php echo $this->uri->segment(2); ?>';
var segment3 = '<?php echo $this->uri->segment(3); ?>';
</script>
<script src="<?php echo site_url( 'js/sortable.common.js' ); ?>" ></script>

<?php include APPPATH .'views/admin/dialog.php'; ?>
