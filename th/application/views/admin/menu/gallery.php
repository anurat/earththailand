<?php include APPPATH .'views/admin/jquery_ui_sortable.php'; ?>

<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_gallery'); ?></h1>

<a href="#" class="button" id="add_button" ><?php echo $this->lang->line('header_add'); ?></a>
<span id="message" class="result" ></span>
<br />
<br />

<?php include APPPATH .'views/admin/pagination.php'; ?>

<ul id="sortable" >
<?php
$i = $page['index1'];

$cms_history = $this->cms_model->get_cms_history();
$cms_item_list = $this->cms_model->get_cms_item_list();
foreach( $gallery_list as $gallery ) {
	$gallery_id = $gallery->get_gallery_id();
	$latest = $cms_history->get_latest_history( 'gallery', $gallery_id );
	$first = $cms_history->get_first_history( 'gallery', $gallery_id );
	
	$item_list = $cms_item_list->get_item_list_by_table( 'gallery', $gallery_id );
	$lock = $item_list->get_lock();
	$enable = $item_list->get_enable();
    $order = $item_list->get_order();
?>
	<li class="ui-state-default <?php echo ( $lock )? "ui-state-disabled": ""; ?>" 
            data-table-id="<?php echo $gallery_id; ?>" 
			data-order="<?php echo $order; ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<div class="left_align pad" >
            <?php echo $i; ?><br />
            <input type="checkbox" name="op[]" value="<?php echo $gallery_id; ?>" />
        </div>
		<div class="left_align pad" >
<?php
    if( $role == Cms_user::ADMIN_ROLE ) {
?>
				<input type="checkbox" id="check_lock_<?php echo $gallery_id; ?>" 
                        name="check_lock_<?php echo $gallery_id; ?>"
						<?php echo ( $lock )? "checked=\"checked\"": ""; ?> />
				<label for="check_lock_<?php echo $gallery_id; ?>" class="lock" title="Lock" >L</label>
<?php
    }
?>
        </div>
		<a class="title" href="<?php echo site_url( "admin/menu/update_gallery/" .$gallery_id ); ?>" 
				><?php echo $gallery_id .' ' .$gallery->get_title(); ?></a>
		<div class="right_align pad" >
			<div class="radio" data-gallery-id="<?php echo $gallery_id; ?>" >
				<input type="radio" id="radio_on_<?php echo $gallery_id; ?>" 
                        name="homepage_radio_<?php echo $gallery_id; ?>" 
						data-status="on" <?php echo ( $enable )? "checked=\"checked\"": ""; ?> >
                <label for="radio_on_<?php echo $gallery_id; ?>" class="on" 
                        ><?php echo $this->lang->line('header_on'); ?></label>
				<input type="radio" id="radio_off_<?php echo $gallery_id; ?>" 
                        name="homepage_radio_<?php echo $gallery_id; ?>" 
						data-status="off" <?php echo ( !$enable )? "checked=\"checked\"": ""; ?> >
                <label for="radio_off_<?php echo $gallery_id; ?>" class="off" 
				        ><?php echo $this->lang->line('header_off'); ?></label>
			</div>
			<select name="op" >
				<option value="0" >-</option>
				<option value="delete" ><?php echo $this->lang->line('header_delete'); ?></option>
				<option value="move" ><?php echo $this->lang->line('header_move'); ?></option>
			</select>
			
		</div>
		<div class="right_align pad" >
            <?php echo date( 'j M Y G:i', $first->get_changed_date() ); ?> 
            <?php echo $first->getUser()->get_name(); ?><br />
            <a href="<?php echo site_url( "admin/menu/gallery_history/{$gallery_id}" ); ?>"
                    title="History" 
                ><?php echo date( 'j M Y G:i', $latest->get_changed_date() ); ?> 
                <?php echo $latest->getUser()->get_name(); ?></a>
        </div>
	</li>
<?php
    $i++;
}
?>
</ul>

<?php include APPPATH .'views/admin/pagination.php'; ?>
