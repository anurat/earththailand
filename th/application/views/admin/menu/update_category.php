<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_news_slideshow'); ?>
	> <?php echo $this->lang->line('menu_update_news_slideshow'); ?></h1>

<?php
$title = htmlspecialchars( $obj->get_list_title() );
$description = $obj->get_list_desc();
$image = $obj->get_list_img();
?>

<script type="text/javascript" >
$( function() {
	
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor1 = CKEDITOR.replace( 'editor1', {
		bodyClass: 'item_description',
		height: 100
	});
	
	$('#img_button').click( function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				d = new Date();
				$("#list_img").attr("src", file_url +"?"+d.getTime());
				var n = file_url.indexOf( 'userfiles/' ) +10;
				$("#news_slideshow_img").val( file_url.substring( n ) );
			}
			
		});
	});
	
	$("a[name=save]").click( function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$("a[name=cancel]").click( function(e) {
		e.preventDefault();
		history.back();
	});
});

</script>

<form method="POST" >

	<?php echo $this->lang->line('category_list_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" /><br />
	<?php echo $this->lang->line('article_newline'); ?><br />
	<br />

	<?php echo $this->lang->line('category_list_desc'); ?><br />
	<textarea class="ckeditor" name="editor1" >
		<?php echo $description; ?>
	</textarea>
	<br />
	
	<?php echo $this->lang->line('category_list_img'); ?><br />
	<input type="text" id="category_img" name="category_img" style="width: 400px; " readonly="readonly"
			value="<?php echo $image; ?>" />
	<a href="#" class="button" name="img_button" id="img_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a><br />
	<?php echo $this->lang->line('category_image_upload'); ?><br />
	<img id="list_img" src="<?php echo site_url( 'userfiles/' .$image ); ?>" style="max-width: 200px; max-height: 200px; " alt="" />
	<br />
	<br />
	
	<div class="save_cancel" >
		<a href="#" class="button" name="save" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" name="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
</form>
