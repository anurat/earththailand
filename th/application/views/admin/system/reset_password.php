<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_user'); ?> 
> <?php echo $this->lang->line('menu_reset_password' ); ?></h1>

<h3>The password has been reset to "<?php echo Cms_user::RESET_PASSWORD; ?>".</h3>