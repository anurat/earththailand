<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_profiles'); ?></h1>

<script type="text/javascript" >
$( function() {
    $('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
        e.preventDefault();
        $('form').submit();
    });
    $('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
        e.preventDefault();
        history.back();
    });
});
</script>

<form method="post" >

	<?php echo $this->lang->line('profiles_name'); ?>
	<input type="text" name="name" value="<?php echo $user->get_name(); ?>" style="width: 400px; " />
	<br />
	<br />

	<?php echo $this->lang->line('profiles_email'); ?>
	<input type="text" name="email" value="<?php echo $user->get_email(); ?>" style="width: 400px; " />
	<br />
	<br />

	<?php echo $this->lang->line('profiles_password'); ?>
	<a href="<?php echo site_url( 'admin/system/change_password' ); ?>" class="button" ><?php echo $this->lang->line('profiles_change_password'); ?></a>

	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
</form>