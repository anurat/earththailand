<h1><?php echo $this->lang->line('menu_system'); ?> > <?php echo $this->lang->line('menu_user'); ?></h1>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {

	$("div.radio").buttonset();
    
	$('a#add_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = "<?php echo site_url( "admin/system/add_user" ); ?>";
	});
    
    // set enable status
	$("input[type=radio]").on( 'click', function() {
		var user_id = $(this).closest('tr').data('user-id');
		var status = $(this).data('status');
		
		$.ajax({
			data: { table_id: user_id, status: status },
			type: 'POST',
			url: "<?php echo site_url( "ajax/enable/user" ); ?>"
		})
		.always( function() {
			$('#message').html( 'Enable status updated.' )
					.show()
					.delay( 5000 )
					.fadeOut();
		});
	});
	
});

</script>

<a href="#" class="button" id="add_button" ><?php echo $this->lang->line('header_add'); ?></a>
<span id="message" class="result" ></span><br />
<br />

<table class="user" >
<tr>
	<th></th>
	<th><?php echo $this->lang->line('user_name'); ?></th>
	<th><?php echo $this->lang->line('user_email'); ?></th>
	<th><?php echo $this->lang->line('user_role'); ?></th>
	<th><?php echo $this->lang->line('user_enabled'); ?></th>
	<th><?php echo $this->lang->line('user_last_signedin'); ?></th>
</tr>
<?php
$i = 1;
foreach( $user_list as $user ) {
	$user_id = $user->get_user_id();
	$last_signedin = $user->get_last_signedin();
	$last_signedin = ( !empty( $last_signedin ) )? date( 'j M Y G:i', $user->get_last_signedin() ): '';
    $enable = $user->get_enable();
?>
<tr data-user-id="<?php echo $user_id; ?>" >
	<td><?php echo $i++; ?>
	<td>
		<a href="<?php echo site_url( "admin/system/update_user/" .$user_id ); ?>" ><?php echo $user->get_name(); ?></a>
	</td>
	<td><?php echo $user->get_email(); ?></td>
	<td><?php echo $user->get_role(); ?></td>
	<td>		
        <div class="right_align pad" >
            <div class="radio" >
                <input type="radio" id="radio_on_<?php echo $user_id; ?>" 
                        name="homepage_radio_<?php echo $user_id; ?>" 
                        data-status="on" <?php echo ( $enable )? "checked=\"checked\"": ""; ?> />
                <label for="radio_on_<?php echo $user_id; ?>" class="on" title="ON" 
                        ><?php echo $this->lang->line('header_on'); ?></label>
                <input type="radio" id="radio_off_<?php echo $user_id; ?>" 
                        name="homepage_radio_<?php echo $user_id; ?>" 
                        data-status="off" <?php echo ( !$enable )? "checked=\"checked\"": ""; ?> />
                <label for="radio_off_<?php echo $user_id; ?>" class="off" title="OFF" 
                        ><?php echo $this->lang->line('header_off'); ?></label>
            </div>
        </div>
    </td>
	<td><?php echo $last_signedin; ?></td>
</tr>
<?php
}
?>
</table>
