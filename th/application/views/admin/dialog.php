<div id="move_dialog_form" title="Move <?php echo $table; ?>" >
	<form>
		<fieldset>
			<label>Please input new position</label><br />
			<br />
			<label for="m_new_pos">Position</label><br />
			<input type="text" name="m_new_pos" id="m_new_pos" value="" class="text ui-widget-content ui-corner-all"><br />
			<br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="m_table_id" id="m_table_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>

<div id="move_all_dialog_form" title="Move multiple <?php echo $table; ?>s" >
	<form>
		<fieldset>
			<label>Please input new position</label><br />
			<br />
			<label for="mm_new_pos">Position</label><br />
			<input type="text" name="mm_new_pos" id="mm_new_pos" value="" 
			        class="text ui-widget-content ui-corner-all"><br />
			<br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="mm_table_id" id="mm_table_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>

<div id="delete_dialog_form" title="Delete <?php echo $table; ?>" >
	<form>
		<fieldset>
			<label>Do you want to delete this <?php echo $table; ?>?</label><br />
			<br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="d_table_id" id="d_table_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>

