<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_tags'); ?>
 > <?php echo $this->lang->line('menu_tag_detail'); ?></h1>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {


	$("a.button").on( 'click', function(e) {
		e.preventDefault();
		
		if( $(this).data("button-type") == "delete_tag" ) {
			delete_tag_dialog( <?php echo $tag->get_tag_id(); ?> );
		}
		else if( $(this).data("button-type") == "delete_tag_link" ) {
			var tag_link_id = $(this).closest('li').data("tag-link-id");
			delete_tag_link_dialog( tag_link_id );
		}
	});
});

function delete_tag_dialog( tag_id ) {
	$('<div></div>').appendTo('body')
			.html( 'Do you want to delete this tag?' )
			.dialog({
				modal: true, 
				title: 'Delete tag', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						location.href = "<?php echo site_url( "admin/homepage/remove_tag" ); ?>/" +tag_id;
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}

function delete_tag_link_dialog( tag_link_id ) {

	$('<div></div>').appendTo('body')
			.html( 'Do you want to delete this tag link?' )
			.dialog({
				modal: true, 
				title: 'Delete tag link', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						location.href = "<?php echo site_url( "admin/homepage/remove_tag_link" ); ?>/" +tag_link_id;
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}
</script>


Tag: <b><?php echo $tag->get_name(); ?></b>
<a class="button" href="#" data-button-type="delete_tag" ><?php echo $this->lang->line('header_delete'); ?></a>
<br />
<br />
 

Linked with <?php echo sizeof( $tag_links ); ?> items.<br />
<?php
if( sizeof( $tag_links ) > 0 ) {
?>
<ul class="oneline">
<?php
	$cms_category = $this->cms_model->get_cms_category();
	$cms_article = $this->cms_model->get_cms_article();
	$cms_document = $this->cms_model->get_cms_document();
	$cms_gallery = $this->cms_model->get_cms_gallery();
	$cms_multimedia = $this->cms_model->get_cms_multimedia();
	$cms_pollution = $this->cms_model->get_cms_pollution();
	foreach( $tag_links as $tag_link ) {

		$table = $tag_link->get_table();
		$table_id = $tag_link->get_table_id();
		$obj = null;
		switch( $table ) {
		case 'category':
			$obj = $cms_category->get_category( $table_id );
			break;
		case 'article':
			$obj = $cms_article->get_article( $table_id );
			break;
		case 'document':
			$obj = $cms_document->get_document( $table_id );
			break;
		case 'gallery':
			$obj = $cms_gallery->get_gallery( $table_id );
			break;
		case 'multimedia':
			$obj = $cms_multimedia->get_multimedia( $table_id );
			break;
		case 'pollution':
			$obj = $cms_pollution->get_pollution( $table_id );
			break;
		}
		
		if( $table == 'category' ) {
?>
	<li data-tag-link-id="<?php echo $tag_link->get_tag_link_id(); ?>" >
		<div class="left_align pad">Category</div>
		<a href="<?php echo site_url( "page/category/" .$table_id ); ?>" class="title"
				target="_blank" ><?php echo $obj->get_title(); ?></a>
		<div class="right_align pad" >
			<a class="button" href="#" data-button-type="delete_tag_link" 
					data-tag-link-id="<?php echo $tag_link->get_tag_link_id(); ?>"
					><?php echo $this->lang->line('header_delete'); ?></a>
		</div>
	</li>
<?php
		}
		else {
?>
	<li data-tag-link-id="<?php echo $tag_link->get_tag_link_id(); ?>" >
		<div class="left_align pad"><?php echo ucfirst( $table ); ?></div>
		<a href="<?php echo site_url( "{$table}/" .$table_id ); ?>" class="title" 
				target="_blank" ><?php echo $obj->get_list_title(); ?></a>
		<div class="right_align pad" >
			<a class="button" href="#" data-button-type="delete_tag_link" 
					
					><?php echo $this->lang->line('header_delete'); ?></a>
		</div>
	</li>
<?php
		}
	}
?>
</ul>
<?php
}
?>
