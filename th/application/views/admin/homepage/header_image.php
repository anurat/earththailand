<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_header_image'); ?></h1>

<img id="header_img" src="<?php echo site_url( 'userfiles/Images/homepage/header.jpg' ); ?>" alt="" style="width: 100%" /><br />
<br />
<script type="text/javascript" >
$( function() {
	$('a#upload_button').on( "click", function() {
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function() {
				d = new Date();
				$("#header_img").attr("src", '<?php echo site_url( 'userfiles/Images/homepage/header.jpg' ); ?>'
						+"?"+d.getTime());
			},
			startupPath : "Images:/homepage/"
		});
	});
});
</script>

<a href="#" class="button" id="upload_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a><br />
Please replace an image at <b>Images/homepage/header.jpg</b><br />
รองรับไฟล์ JPG, GIF, PNG และขนาดภาพที่เหมาะสมคือ 1200 x 200 pixels<br />
