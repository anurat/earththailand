<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_news_slideshow'); ?></h1>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {

	$("#sortable").sortable({
		placeholder: "ui-state-highlight",
		axis: "y",
		cursor: "pointer",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_homepage = new Array();
			var a_order = new Array();
			$("#sortable li").each( function(i) {
				a_homepage.push(  $(this).data('homepage-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { homepage: a_homepage, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/homepage_article" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});

	$('a#add_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = "<?php echo site_url( "/admin/homepage/add_news_slideshow" ); ?>";
	});
	$('li a.button').on( 'click', function(e) {
		e.preventDefault();
        var homepage_id = $(this).closest('li').data('homepage-id');
		confirm_dialog( homepage_id );
	});
});

function confirm_dialog( homepage_id ) {
	$('<div></div>').appendTo('body')
			.html( 'Do you want to remove this news slideshow from homepage?' )
			.dialog({
				modal: true, 
				title: 'Remove news slideshow', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						location.href = "<?php echo site_url( "admin/homepage/remove_news_slideshow" ); ?>/" +homepage_id;
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}

</script>

<a href="#" class="button" id="add_button" ><?php echo $this->lang->line('header_add'); ?></a>
<span id="message" class="result" ></span><br />
<br />

<?php echo $this->lang->line('news_slideshow_no'); ?>: <?php echo sizeof( $homepage_list ); ?><br />

<ul id="sortable" >
<?php
foreach( $homepage_list as $homepage ) {
	$homepage_id = $homepage->get_homepage_id();
?>
	<li class="ui-state-default" data-homepage-id="<?php echo $homepage_id; ?>" data-order="<?php echo $homepage->get_order(); ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<a class="title" href="<?php echo site_url( "admin/homepage/update_news_slideshow/" 
                .$homepage_id ); ?>" ><?php echo $homepage->get_title(); ?></a>
		<div class="right_align pad" >
            <a href="#" class="button" ><?php echo $this->lang->line('header_delete'); ?></a>
        </div>
	</li>
<?php
}
?>
</ul>
