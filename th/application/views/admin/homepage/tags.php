<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_tags'); ?></h1>

<input type="text" name="search" placeholder="ค้นหาแท็ก" style="width: 300px; " value="<?php echo $search_text; ?>" />
<a href="#" class="button" ><?php echo $this->lang->line('header_search'); ?></a><br />
<script type="text/javascript" >
$( function() {
	$('a.button').click( function(e) {
		e.preventDefault();
		var url = "<?php echo site_url( "admin/homepage/tags" ); ?>/" +$("input[name=search]").val();
		location.href = url;
	});
});
</script>

<div style="overflow: auto; " >
	<div style="width: 45%; float: left; " >
		<h2><?php echo $this->lang->line('tag_english'); ?></h2>
<?php
foreach( $tags as $tag ) {
	$name = $tag->get_name();
	if( $name[0] > 'z' ) {
		break;
	}
?>
		<a href="<?php echo site_url( "/admin/homepage/tag/" .$tag->get_tag_id() ); ?>" class="tag"
				><?php echo $tag->get_name(); ?></a><br />
<?php
}
?>
	</div>
	<div style=" width: 45%; float: left; " >
		<h2><?php echo $this->lang->line('tag_thai'); ?></h2>
<?php
foreach( $tags as $tag ) {
	$name = $tag->get_name();
	if( $name[0] <= 'z' ) {
		continue;
	}
?>
		<a href="<?php echo site_url( "/admin/homepage/tag/" .$tag->get_tag_id() ); ?>" class="tag"
				><?php echo $tag->get_name(); ?></a><br />
<?php
}
?>
	</div>
</div>