<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_news_slideshow'); ?>
	> <?php echo $this->lang->line('menu_update_news_slideshow'); ?></h1>

<?php
$title = htmlspecialchars( $news_slideshow->get_title() );
$description = $news_slideshow->get_description();
$image = $news_slideshow->get_image();
?>

<script type="text/javascript" >
$( function() {
	
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor1 = CKEDITOR.replace( 'editor1', {
		bodyClass: 'article_description',
		height: 100
	});
	
	$('a#img_button').click( function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				d = new Date();
				$("#list_img").attr("src", file_url +"?"+d.getTime());
				var n = file_url.indexOf( 'userfiles/' ) +10;
				$("#news_slideshow_img").val( file_url.substring( n ) );
			}
			
		});
	});
	
	$('div.save_cancel a:nth-child(1)').on( 'click', function(e) {
		e.preventDefault();
		$('form').submit();
	});
	$('div.save_cancel a:nth-child(2)').on( 'click', function(e) {
		e.preventDefault();
		history.back();
	});
});

</script>

<form method="POST" >

	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
	<?php echo $this->lang->line('article_list_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" /><br />
	<?php echo $this->lang->line('article_newline'); ?><br />
	<br />

	<?php echo $this->lang->line('article_list_desc'); ?><br />
	<textarea class="ckeditor" name="editor1" >
		<?php echo $description; ?>
	</textarea>
	<br />
	
	<?php echo $this->lang->line('article_list_img'); ?><br />
	<input type="text" id="news_slideshow_img" name="news_slideshow_img" style="width: 400px; " readonly="readonly"
			value="<?php echo $image; ?>" />
	<a href="#" class="button" id="img_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a><br />
	<?php echo $this->lang->line('article_image_upload'); ?><br />
	<img id="list_img" src="<?php echo site_url( 'userfiles/' .$image ); ?>" style="max-width: 200px; max-height: 200px; " alt="" />
	<br />
	<br />
	
	<div class="save_cancel" >
		<a href="#" class="button" ><?php echo $this->lang->line('header_save'); ?></a>
		<a href="#" class="button" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
</form>
