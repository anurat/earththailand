<h1><?php echo $this->lang->line('menu_homepage'); ?> > <?php echo $this->lang->line('menu_link'); ?></h1>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {	

	$("#sortable, #sortable2, #sortable3").sortable({
		placeholder: "ui-state-highlight",
		axis: "y",
		cursor: "pointer",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_homepage = new Array();
			var a_order = new Array();
			$(this).find("li").each( function(i) {
				a_homepage.push(  $(this).data('homepage-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { homepage: a_homepage, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/link" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});

	$('a#thai_add_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = "<?php echo site_url( "/admin/homepage/add_link/thai" ); ?>";
	});
	$('a#regional_add_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = "<?php echo site_url( "/admin/homepage/add_link/regional" ); ?>";
	});
	$('a#global_add_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = "<?php echo site_url( "/admin/homepage/add_link/global" ); ?>";
	});
	$('li a.button').on( 'click', function(e) {
		e.preventDefault();
        var homepage_id = $(this).closest('li').data('homepage-id');
		delete_dialog( homepage_id );
	});
});

function delete_dialog( homepage_id ) {
	$('<div></div>').appendTo('body')
			.html( 'Do you want to remove this external link from homepage?' )
			.dialog({
				modal: true, 
				title: 'Remove external link', 
				zIndex: 10000, 
				autoOpen: true,
				width: 'auto', 
				resizable: false,
				buttons: {
					Yes: function () {
						$(this).dialog("close");
						location.href = "<?php echo site_url( "admin/homepage/remove_link" ); ?>/" +homepage_id;
					},
					No: function () {
						$(this).dialog("close");
					}
				},
				close: function (event, ui) {
					$(this).remove();
				}
			});
}

</script>

<h2><?php echo $this->lang->line('link_thai_link'); ?></h2>
<a href="#" class="button" id="thai_add_button" ><?php echo $this->lang->line('header_add'); ?></a> 
<span id="message" class="result" ></span><br />
<br />

<?php echo $this->lang->line('link_no'); ?> <?php echo sizeof( $thai_link_list ); ?><br />

<ul id="sortable" >
<?php
foreach( $thai_link_list as $link ) {
	$homepage_id = $link->get_homepage_id();
?>
	<li class="ui-state-default" data-homepage-id="<?php echo $homepage_id; ?>" 
	        data-order="<?php echo $link->get_order(); ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<a class="title" href="<?php echo site_url( "/admin/homepage/update_link/thai/" 
                .$homepage_id ); ?>" ><?php echo $link->get_link_name(); ?><br />
				<?php echo $link->get_link_url(); ?></a>
		<div class="right_align pad" >
		    <a href="#" class="button"
		            ><?php echo $this->lang->line('header_delete'); ?></a>
        </div>
	</li>
<?php
}
?>
</ul>
<br />

<h2><?php echo $this->lang->line('link_regional_link'); ?></h2>
<a href="#" class="button" id="regional_add_button" ><?php echo $this->lang->line('header_add'); ?></a><br />
<br />
<?php echo $this->lang->line('link_no'); ?> <?php echo sizeof( $regional_link_list ); ?><br />

<ul id="sortable2" >
<?php
foreach( $regional_link_list as $link ) {
	$homepage_id = $link->get_homepage_id();
?>
	<li class="ui-state-default" data-homepage-id="<?php echo $homepage_id; ?>" 
	        data-order="<?php echo $link->get_order(); ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<a class="title" href="<?php echo site_url( "/admin/homepage/update_link/regional/" 
                .$homepage_id ); ?>" ><?php echo $link->get_link_name(); ?><br />
				<?php echo $link->get_link_url(); ?></a>
		<div class="right_align pad" >
            <a href="#" class="button"
                    ><?php echo $this->lang->line('header_delete'); ?></a>
        </div>
	</li>
<?php
}
?>
</ul>


<h2><?php echo $this->lang->line('link_global_link'); ?></h2>
<a href="#" class="button" id="global_add_button" ><?php echo $this->lang->line('header_add'); ?></a><br />
<br />
<?php echo $this->lang->line('link_no'); ?> <?php echo sizeof( $global_link_list ); ?><br />

<ul id="sortable3" >
<?php
foreach( $global_link_list as $link ) {
	$homepage_id = $link->get_homepage_id();
?>
	<li class="ui-state-default" data-homepage-id="<?php echo $homepage_id; ?>" 
	        data-order="<?php echo $link->get_order(); ?>" >
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<a class="title" href="<?php echo site_url( "/admin/homepage/update_link/global/" 
                .$homepage_id ); ?>" ><?php echo $link->get_link_name(); ?><br />
				<?php echo $link->get_link_url(); ?></a>
		<div class="right_align pad" >
            <a href="#" class="button"
                    ><?php echo $this->lang->line('header_delete'); ?></a>
        </div>
	</li>
<?php
}
?>
</ul>
