<?php
$search_text = ( empty( $search_text ) )? '': $search_text;
?>
<h1><?php echo $this->lang->line( 'menu_search' ); ?></h1>
	
<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
	$( function() {
		$("select").selectmenu();

		$('a[name=search_button]').on('click', function(e) {
			e.preventDefault();
			$('form[name=search_form]').submit();
		});
	});
</script>

<style>
	div#content span.ui-selectmenu-text {
		font-size: 120%;
	}
</style>
<form method="post" name="search_form">
	<?php echo $this->lang->line( 'search_type' ); ?>
	<select name="search_type" style="width: 200px; ">
		<option value="article" <?php echo ( $search_type == 'article' )? 'selected="selected"': ''; ?>
				><?php echo $this->lang->line('menu_articles'); ?></option>
		<option value="document" <?php echo ( $search_type == 'document' )? 'selected="selected"': ''; ?>
				><?php echo $this->lang->line('menu_documents'); ?></option>
		<option value="gallery" <?php echo ( $search_type == 'gallery' )? 'selected="selected"': ''; ?>
				><?php echo $this->lang->line('menu_gallery'); ?></option>
		<option value="multimedia" <?php echo ( $search_type == 'multimedia' )? 'selected="selected"': ''; ?>
				><?php echo $this->lang->line('menu_multimedia'); ?></option>
		<option value="pollution" <?php echo ( $search_type == 'pollution' )? 'selected="selected"': ''; ?>
				><?php echo $this->lang->line('menu_pollution_map'); ?></option>
		<option value="calendar" <?php echo ( $search_type == 'calendar' )? 'selected="selected"': ''; ?>
				><?php echo $this->lang->line('menu_calendar'); ?></option>
	</select>
	<br />

	<?php echo $this->lang->line( 'menu_search' ); ?>
	<input type="text" name="search_text" style="width: 200px; " placeholder="<?php echo $this->lang->line('search_text'); ?>"
			value="<?php echo $search_text; ?>" />
	<a href="#" class="button" name="search_button" ><?php echo $this->lang->line('menu_search'); ?></a>
</form>

<?php
if( !empty( $search_text ) ) {
?>
<h2><?php echo $this->lang->line('search_result'); ?></h2>
No. of matches: <?php echo sizeof( $result ); ?><br />
	
<ul>
<?php
	foreach( $result as $obj ) {
		$id = 0;
		switch( $search_type ) {
		case 'article': $id = $obj->get_article_id(); break;
		case 'document': $id = $obj->get_document_id(); break;
		case 'gallery': $id = $obj->get_gallery_id(); break;
		case 'multimedia': $id = $obj->get_multimedia_id(); break;
		case 'pollution': $id = $obj->get_pollution_id(); break;
		case 'calendar': $id = $obj->get_calendar_id(); break;
		}

		$menu = 'menu';
		if( $search_type == 'multimedia' ) {
			$menu = 'left_menu';
		}
?>
	<li><a href="<?php echo site_url( "admin/{$menu}/update_{$search_type}/{$id}" ); ?>" target="_blank"
			><?php echo $id; ?> <?php echo $obj->get_title(); ?></a></li>
<?php
	}
}
?>
</ul>
