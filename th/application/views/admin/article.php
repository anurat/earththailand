<?php
if( empty( $article_id ) ) {
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_articles'); ?>
 > <?php echo $this->lang->line('menu_add_article'); ?></h1>
<?php
}
else {
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_articles'); ?>
 > <?php echo $this->lang->line('menu_update_article'); ?></h1>
<?php
}

$cms_article = $this->cms_model->get_cms_article();

$current_id = ( empty( $article_id ) )? $cms_article->get_next_id(): $article->get_article_id();
$title = ( empty( $article_id ) )? '': htmlspecialchars( $article->get_title() );
$video_code = ( empty( $article_id ) )? '': $article->get_video_code();
$description = ( empty( $article_id ) )? '': $article->get_description();
$attachment = ( empty( $article_id ) )? '': $article->get_attachment();
$list_title = ( empty( $article_id ) )? '': htmlspecialchars( $article->get_list_title() );
$list_desc = ( empty( $article_id ) )? '': $article->get_list_desc();
$list_img = ( empty( $article_id ) )? '': $article->get_list_img();
$article_date = ( empty( $article_id ) )? time(): $article->get_article_date();
?>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >
$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor1 = CKEDITOR.replace( 'editor1', {
		bodyClass: 'item_description',
		height: 100
	});
	var editor2 = CKEDITOR.replace( 'editor2', {
		bodyClass: 'article_description',
		height: 1000
	});
	var editor3 = CKEDITOR.replace( 'editor3', {
		bodyClass: 'article_description',
		height: 100
	});

	load_images( 'article', <?php echo $current_id; ?>, '<?php echo site_url(); ?>' );
	
	$('a#upload_button').on( 'click', function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				load_images( 'article', <?php echo $current_id; ?>, '<?php echo site_url(); ?>' );
			},
			startupPath : "Articles:/<?php echo $current_id; ?>/"
		});
	});
	
	$( "#sortable_box" ).sortable({
		placeholder: "ui-state-highlight",
		containment: "div#content",
		delay: "150",
		scroll: true,
		scrollSensitivity: 100,
		update: function( event, ui ) {

			var a_image = new Array();
			var a_order = new Array();
			$("#sortable_box li").each( function(i) {
				a_image.push(  $(this).data('image-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { image: a_image, order: a_order },
				type: 'POST',
				url: "<?php echo site_url( "ajax/sortable/image" ); ?>"
			})
			.always( function() {
				$('#message').html( 'Reordering complete.' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});
	
	var dialog = $( "#dialog_form" ).dialog({
		autoOpen: false,
		height: 250,
		width: 350,
		modal: true,
		buttons: {
			Update: function() {
			
				$.ajax({
					data: { 
						image_id: $("#image_id").val(), 
						name: $("#name").val(), 
						description: $("#description").val()
					},
					type: 'POST',
					url: "<?php echo site_url( "ajax/image/update" ); ?>"
				})
				.always( function( data ) {
				
					load_images( 'article', <?php echo $current_id; ?>, '<?php echo site_url(); ?>' );
					$('#message').html( 'Image info updated.' )
							.show()
							.delay( 5000 )
							.fadeOut();
				});
				dialog.dialog( "close" );
			},
			Cancel: function() {
				dialog.dialog( "close" );
			}
		},
	});
	
	$('#sortable_box').on( 'click', 'span.box', function() {
	
		var $li = $(this).parent();
		$("#image_id").val( $li.data("image-id") );
		$("#name").val( $li.find("span.name").html() );
		$("#description").val( $li.find("span.description").html() );
		dialog.dialog( "open" );
	});
	
	$('#list_img_button').on( 'click', function(e) {
		e.preventDefault();
		CKFinder.popup({
			basePath: '/th/ckfinder/',
			selectActionFunction: function( file_url ) {
				d = new Date();
				$("#article_list_img").attr("src", file_url +"?"+d.getTime());
				var n = file_url.indexOf( 'userfiles/' ) +10;
				$("#list_img").val( file_url.substring( n ) );
			},
			startupPath : "Articles:/item/"
		});
	});
	
	$('a#clear_list_image').on( 'click', function( e ) {
		e.preventDefault();
		$("#article_list_img").attr("src", "" );
		$("#list_img").val("");
	});
		
	$("#article_date").datepicker({ 
		dateFormat: "d MM yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "2005:2020"
	});
	
	$("#checkbox_box").buttonset();
	
	$('div.save_cancel a.button').on( 'click', function(e) {
		e.preventDefault();
		var button_type = $(this).data('button-type');
		switch( button_type ) {
		case 'cancel':
			history.back();
			break;
		default:
			$('form[name=article_form] input[name=save_type]').val( button_type );
			$('form[name=article_form]').submit();
			break;
		}
	});
});

function load_images( type, id, base_url ) {
	$.getJSON( base_url +'ajax/gallery_image/' +type +'/' +id, function( images ) {
		
		var html = '';
		$.each( images, function( i, image ) {
			html += "<li class=\"ui-state-default\" data-image-id=\"" +image._image_id +"\" data-order=\"" +image._order +"\" title=\"" +image._path +"\" >\n"
					+"	<span class=\"ui-icon ui-icon-arrow-4\"></span>\n"
					+"	<div>\n"
					+"		<img src=\"" +base_url +"userfiles/_thumbs/Articles/" +id +"/" +image._path +"\" alt=\"\" />\n"
					+"	</div>\n"
					+"	<span class=\"box name\" title=\"" +image._name +"\" >" +image._name +"</span>\n"
					+"	<span class=\"box description\" title=\"" +image._description +"\" >" +image._description +"</span>\n"
					+"</li>\n";
		});
		
		$("#sortable_box").html( html );
		$("#image_no").html( images.length );
	});
}

</script>

<form method="post" name="article_form">
	<div class="save_cancel" >
		<input type="hidden" name="previous_url" value="<?php echo $previous_url; ?>" />
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $article_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
	<?php echo $this->lang->line('article_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" />
	<br />
	<br />
	
	<?php echo $this->lang->line('article_video_code'); ?><br />
	<textarea name="video_code" ><?php echo $video_code; ?></textarea>
	<br />
	<br />

	<?php echo $this->lang->line('article_image'); ?><br />
	<a href="#" class="button" id="upload_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
	<span id="message" class="result" ></span><br />
	<?php echo $this->lang->line('article_image_folder'); ?> <b>Articles/<?php echo $current_id; ?></b> <br />

	<?php echo $this->lang->line('article_image_no'); ?>: <span id="image_no" ></span>
	<ul id="sortable_box" ></ul>
	<br />
	
	<?php echo $this->lang->line('article_description'); ?><br />
	<textarea class="ckeditor" name="editor2" >
		<?php echo $description; ?>
	</textarea>
	<br />

	<?php echo $this->lang->line('article_attachment'); ?><br />
	<textarea class="ckeditor" name="editor3" >
		<?php echo $attachment; ?>
	</textarea>
	<br />

	<?php echo $this->lang->line('article_tags'); ?><br />
	<input type="text" name="tags" value="<?php echo $tags; ?>" /><br />
	<br />
	
	<hr />
	<h2 id="list_item" ><?php echo $this->lang->line('header_item'); ?></h2>
	
	<?php echo $this->lang->line('article_list_title'); ?><br />
	<input type="text" class="title" name="list_title" value="<?php echo $list_title; ?>" /><br />
	<?php echo $this->lang->line('article_newline'); ?><br />
	<br />

	<?php echo $this->lang->line('article_list_desc'); ?><br />
	<textarea class="ckeditor" name="editor1" >
		<?php echo $list_desc; ?>
	</textarea>
	<br />
	
	<?php echo $this->lang->line('article_list_img'); ?><br />
	<input type="text" id="list_img" name="list_img" style="width: 400px; " readonly="readonly"
			value="<?php echo $list_img; ?>" />
	<a href="#" class="button" id="list_img_button" ><?php echo $this->lang->line( 'header_upload' ); ?></a>
	<a href="#" id="clear_list_image" ><?php echo $this->lang->line('header_clear'); ?></a><br />
	<?php echo $this->lang->line('article_image_upload'); ?><br />
	<img id="article_list_img" src="<?php echo site_url( 'userfiles/' .$list_img ); ?>" style="max-width: 200px; max-height: 200px; " alt="" />
	<br />
	<br />
	
	<?php echo $this->lang->line('article_date'); ?><br />
	<input type="text" id="article_date" name="article_date" style="width: 400px; "
			value="<?php echo date( 'j F Y', $article_date ); ?>" /><br />
	<br />
			
	<?php echo $this->lang->line('header_category'); ?><br />
	<div id="checkbox_box">
<?php
$cms_category = $this->cms_model->get_cms_category();
foreach( $category_list as $category ) {
	$category_id = $category->get_category_id();
	$has_cat_link = $cms_category->has_cat_link( $category_id, $cat_links );
?>
		<input type="checkbox" id="category[<?php echo $category_id; ?>]" name="category[<?php echo $category_id; ?>]" 
				<?php echo ( empty( $article_id ) && $category_id == Cms_category::ARTICLE_CAT )? "checked=\"checked\"": ""; ?>
				<?php echo ( !empty( $article_id ) && $has_cat_link )? "checked=\"checked\"": ""; ?>
				><label for="category[<?php echo $category_id; ?>]"><?php echo $category->get_title(); ?></label>
<?php
}
?>
	</div>

	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $article_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
    <input type="hidden" name="save_type" value=""/>	
</form>
<br />

<div id="dialog_form" title="Update photo info" >
	<form>
		<fieldset>
			<label for="name">Name</label><br />
			<input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all"><br />
			<br />
			<label for="description">Description</label><br />
			<textarea name="description" id="description" class="textarea ui-widget-content ui-corner-all"></textarea><br />
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="hidden" name="image_id" id="image_id" value="" />
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>
