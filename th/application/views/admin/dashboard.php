<h1><img src="/th/images/home.png" alt="home" /><?php echo $this->lang->line( 'dashboard_dashboard' ); ?></h1>

<div class="box" style="width: 350px; " >
	<h2><?php echo $this->lang->line( 'dashboard_article_no' ); ?></h2>
	<table style="width: 80%; ">
	<tr>
		<th style="width: 75%;" ><?php echo $this->lang->line( 'dashboard_group' ); ?></th>
		<th><?php echo $this->lang->line( 'dashboard_no' ); ?></th>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_top_articles' ); ?></td>
		<td class="no" ><?php echo $article_no; ?></td>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_top_documents' ); ?></td>
		<td class="no" ><?php echo $document_no; ?></td>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_gallery' ); ?></td>
		<td class="no" ><?php echo $gallery_no; ?></td>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_multimedia' ); ?></td>
		<td class="no" ><?php echo $multimedia_no; ?></td>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_pollution_map' ); ?></td>
		<td class="no" ><?php echo $pollution_no; ?></td>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_calendar' ); ?></td>
		<td class="no" ><?php echo $calendar_no; ?></td>
	</tr>
	<tr>
		<td><?php echo $this->lang->line( 'menu_tags' ); ?></td>
		<td class="no" ><?php echo $tag_no; ?></td>
	</tr>
	</table>
</div>

<div class="box noborder" style="width: 380px;" >
	<h2 class="red" ><?php echo $this->lang->line( 'dashboard_quick_icon' ); ?></h2>
	<a href="<?php echo site_url( "admin/menu/articles" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/news-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_top_articles' ); ?></a>
	<a href="<?php echo site_url( "admin/menu/documents" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/document-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_short_documents' ); ?></a>
	<a href="<?php echo site_url( "admin/menu/about" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/business_contact-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_short_about' ); ?></a>
	<a href="<?php echo site_url( "admin/menu/pollution_map" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/map_marker-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_pollution_map' ); ?></a>
	<a href="<?php echo site_url( "admin/menu/calendar" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/tear_off_calendar-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_calendar' ); ?></a>
	<a href="<?php echo site_url( "admin/homepage/tags" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/tag-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_tags' ); ?></a>
	<a href="<?php echo site_url( "admin/menu/gallery" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/picture-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_gallery' ); ?></a>
	<a href="<?php echo site_url( "admin/left_menu/multimedia" ); ?>" class="icon" 
			><img src="<?php echo site_url( "images/youtube-50.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_multimedia' ); ?></a>
	<a href="<?php echo site_url( "admin/search" ); ?>" class="icon"
			><img src="<?php echo site_url( "images/search-48.png" ); ?>" alt="" /><br />
			<?php echo $this->lang->line( 'menu_search' ); ?></a>
</div>

<div class="box" style="width: 700px; clear: both; " >
	<h2><?php echo $this->lang->line( 'dashboard_statistics' ); ?></h2>
	<div id="widgetIframe"><iframe width="100%" height="350" src="http://earththailand.org/th/piwik/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=range&date=last30&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
</div>