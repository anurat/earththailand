<?php
if( empty( $calendar_id ) ) {
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_calendar'); ?>
 > <?php echo $this->lang->line('menu_add_calendar'); ?></h1>
<?php
}
else {
?>
<h1><?php echo $this->lang->line('menu_topmenu'); ?> > <?php echo $this->lang->line('menu_calendar'); ?>
 > <?php echo $this->lang->line('menu_update_calendar'); ?></h1>
<?php
}

$cms_calendar = $this->cms_model->get_cms_calendar();

$current_id = ( empty( $calendar_id ) )? $cms_calendar->get_next_id(): $calendar->get_calendar_id();
$title = ( empty( $calendar_id ) )? '': htmlspecialchars( $calendar->get_title() );
$video_code = ( empty( $calendar_id ) )? '': $calendar->get_video_code();
$description = ( empty( $calendar_id ) )? '': $calendar->get_description();
$start_date = ( empty( $calendar_id ) )? time(): $calendar->get_start_date();
$end_date = ( empty( $calendar_id ) )? time(): $calendar->get_end_date();
?>

<link rel="stylesheet" href="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.css' ); ?>" type="text/css" />
<script src="<?php echo site_url( 'js/jquery-ui-1.11.0.custom/jquery-ui.min.js' ); ?>" ></script>
<script type="text/javascript" >

$( function() {
	CKFinder.setupCKEditor( null, '/th/ckfinder/' );
	var editor2 = CKEDITOR.replace( 'editor2', {
		bodyClass: 'article_description',
		height: 1000
	});

	$("#start_date, #end_date").datepicker({ 
		dateFormat: "d MM yy",
		changeMonth: true,
		changeYear: true,
		yearRange: "2014:2020"
	});
	
	$('div.save_cancel a.button').on( 'click', function(e) {
		e.preventDefault();
        var button_type = $(this).data('button-type');
        switch( button_type ) {
        case 'cancel':
            history.back();
            break;
        default:
            $('form[name=calendar_form] input[name=save_type]').val( button_type );
            $('form[name=calendar_form]').submit();
            break;
        }
	});
});
</script>

<form method="post" name="calendar_form">
	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $calendar_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	
	<?php echo $this->lang->line('calendar_title'); ?><br />
	<input type="text" class="title" name="title" value="<?php echo $title; ?>" />
	<br />
	<br />
	
	<?php echo $this->lang->line('calendar_video_code'); ?><br />
	<textarea name="video_code" ><?php echo $video_code; ?></textarea>
	<br />
	<br />

	<?php echo $this->lang->line('calendar_description'); ?><br />
	<textarea class="ckeditor" name="editor2" >
		<?php echo $description; ?>
	</textarea>
	<br />

	<?php echo $this->lang->line('calendar_tags'); ?><br />
	<input type="text" name="tags" value="<?php echo $tags; ?>" /><br />
	<br />
	
	<?php echo $this->lang->line('start_date'); ?><br />
	<input type="text" id="start_date" name="start_date" style="width: 400px; "
			value="<?php echo date( 'j F Y', $start_date ); ?>" /><br />
	<br />

	<?php echo $this->lang->line('end_date'); ?><br />
	<input type="text" id="end_date" name="end_date" style="width: 400px; "
			value="<?php echo date( 'j F Y', $end_date ); ?>" /><br />
	<br />
	<br />
	
	<div class="save_cancel" >
		<a href="#" class="button" data-button-type="save_close" ><?php echo $this->lang->line('header_save_close'); ?></a>
		<a href="#" class="button" data-button-type="save_new" ><?php echo $this->lang->line('header_save_new'); ?></a>
<?php
if( !empty( $calendar_id ) ) {
?>
		<a href="#" class="button" data-button-type="save" ><?php echo $this->lang->line('header_save'); ?></a>
<?php
}
?>
		<a href="#" class="button" data-button-type="cancel" ><?php echo $this->lang->line('header_cancel'); ?></a>
	</div>
	<br />
    <input type="hidden" name="save_type" value=""/>	
</form>
