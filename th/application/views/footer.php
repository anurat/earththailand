มูลนิธิบูรณะนิเวศ<br />
211/2 ซ.งามวงศ์วาน 31 ถ.งามวงศ์วาน อ.เมือง นนทบุรี 11000<br />
โทรศัพท์: 02 952 5061 โทรสาร: 02 952 5062<br />
Email: <a href="mailto:earth@earththailand.org" target="_blank">earth@earththailand.org</a><br />
Web Editor: <a href="mailto:webeditor@earththailand.org" target="_blank">webeditor@earththailand.org</a><br />
<br />
Ecological Alert and Recovery-Thailand (EARTH)<br />
211/2 Soi Ngamwongwan31, Ngamwongwan Rd., Muang, Nonthaburi 11000, THAILAND<br />
Tel: +66 2 952 5061 Fax: +66 2 952 5062<br />
Email: <a href="mailto:earth@earththailand.org" target="_blank">earth@earththailand.org</a><br />
Web Editor: <a href="mailto:webeditor@earththailand.org" target="_blank">webeditor@earththailand.org</a><br />

<div style="text-align:center">&nbsp;</div>
<div style="text-align:center">&nbsp;</div>

โครงการวิทยาศาสตร์ภาคพลเมืองในการเฝ้าระวังมลพิษอุตสาหกรรมเพื่อสุขภาพและสิ่งแวดล้อม<br />
(Increasing Transparency in Industrial Pollution Management through Citizen Science)

<div style="text-align:center">&nbsp;</div>

<img alt="" src="/th/userfiles/Images/1%20logo%20EU%20Flag.png" />&nbsp; 
<img alt="" src="/th/userfiles/Images/RGB01_ThaiHealth%20Logo_ThEng_full%20colour.png" /></div>

<div style="text-align:center">&nbsp;</div>

<p>&nbsp;</p>
