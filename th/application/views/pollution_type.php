<div id="article" >
	<div class="pollution_type">
		<h1>พื้นที่มลพิษ</h1>
		<br />
		<table style="width: 100%" >
			<tr>
				<td style="width: 50%; text-align: center; " >
					<a href="<?php echo site_url( "pollution_list/1" ); ?>" >
						<img src="<?php echo site_url( "images/north.jpg" ); ?>" style="width: 350px;" />
					</a>
					<br />
					<h3>พื้นที่ภาคเหนือ</h3><br />
					<br />
				</td>
				<td style="width: 50%; text-align: center; " >
					<a href="<?php echo site_url( "pollution_list/2" ); ?>" >
						<img src="<?php echo site_url( "images/east.jpg" ); ?>" style="width: 350px;" />
					</a>
					<br />
					<h3>พื้นที่ภาคตะวันออก</h3><br />
					<br />
				</td>
			</tr>
			<tr>
				<td style="width: 50%; text-align: center; " >
					<a href="<?php echo site_url( "pollution_list/3" ); ?>" >
						<img src="<?php echo site_url( "images/northeast.jpg" ); ?>" style="width: 350px;" />
					</a>
					<br />
					<h3>พื้นที่ภาคตะวันออกเฉียงเหนือ</h3><br />
					<br />
				</td>
				<td style="width: 50%; text-align: center; " >
					<a href="<?php echo site_url( "pollution_list/4" ); ?>" >
						<img src="<?php echo site_url( "images/west.jpg" ); ?>" style="width: 350px;" />
					</a>
					<br />
					<h3>พื้นที่ภาคตะวันตก</h3><br />
					<br />
				</td>
			</tr>
			<tr>
				<td style="width: 50%; text-align: center; " >
					<a href="<?php echo site_url( "pollution_list/5" ); ?>" >
						<img src="<?php echo site_url( "images/central.jpg" ); ?>" style="width: 350px;" />
					</a>
					<br />
					<h3>พื้นที่ภาคกลาง</h3><br />
					<br />
				</td>
				<td style="width: 50%; text-align: center; " >
					<a href="<?php echo site_url( "pollution_list/6" ); ?>" >
						<img src="<?php echo site_url( "images/south.jpg" ); ?>" style="width: 350px;" />
					</a>
					<br />
					<h3>พื้นที่ภาคใต้</h3><br />
					<br />
				</td>
			</tr>
		</table>
		<br />
		<br />
		<br />
		<div class="end_desc" >&nbsp;</div>
		
<?php
include "social_media.php";
?>
	</div>
</div>