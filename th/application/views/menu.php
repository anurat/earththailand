<nav class="nav1600" >
	<ul>
		<li><a href="<?php echo site_url( '' ); ?>" target="_blank" >หน้าหลัก</a></li>
		<li><a href="<?php echo site_url( 'pollutions' ); ?>" target="_blank" >แผนที่มลพิษ</a></li>
		<li><a href="<?php echo site_url( 'articles' ); ?>" target="_blank" >ข่าว/บทความ</a></li>
		<li><a href="<?php echo site_url( 'documents' ); ?>" target="_blank" >เอกสาร/ข้อมูลเผยแพร่</a></li>
		<li><a href="<?php echo site_url( 'gallery_list' ); ?>" target="_blank" >ห้องภาพ</a></li>
		<li><a href="<?php echo site_url( 'events' ); ?>" target="_blank" >กิจกรรมและสัมมนา</a></li>
		<li><a href="<?php echo site_url( 'about' ); ?>" target="_blank" >เกี่ยวกับมูลนิธิ</a></li>
		<li><a href="<?php echo site_url( 'tags' ); ?>" target="_blank" >แท็ก</a></li>
		<li><a href="/en" target="_blank" >English</a></li>
		<li>
			<form action="<?php echo site_url( 'search' ); ?>" method="post" data-ajax="false" >
				<input id="text-search" name="text-search" maxlength="20" type="text" size="10" placeholder="ค้นหา" />
			</form>
		</li>
	</ul>
</nav>

<nav class="nav1200" >
	<ul>
		<li><a href="<?php echo site_url( '' ); ?>" target="_blank" >หน้าหลัก</a></li>
		<li><a href="<?php echo site_url( 'articles' ); ?>" target="_blank" >ข่าว/บทความ</a></li>
		<li><a href="<?php echo site_url( 'documents' ); ?>" target="_blank" >เอกสาร/ข้อมูลเผยแพร่</a></li>
		<li><a href="<?php echo site_url( 'gallery_list' ); ?>" target="_blank" >ห้องภาพ</a></li>
		<li><a href="<?php echo site_url( 'about' ); ?>" target="_blank" >เกี่ยวกับมูลนิธิ</a></li>
		<li><a href="/en" target="_blank" >English</a></li>
	</ul>
	
	<?php include "select_menu.php"; ?>
</nav>

<nav class="nav800" >
	<?php include "select_menu.php"; ?>
</nav>
