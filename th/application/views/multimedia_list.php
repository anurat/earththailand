<div id="article" >
	<div>
		<h1>มัลติมีเดีย/Multimedia</h1>
		<br />

		<div class="items" >
<?php
foreach( $multimedia_list as $multimedia ) {
?>
			<div>
				<a href="<?php echo site_url( "multimedia/" .$multimedia->get_multimedia_id() ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$multimedia->get_list_img() ); ?>" alt="" /></a>
				<h4><a href="<?php echo site_url( "multimedia/" .$multimedia->get_multimedia_id() ); ?>" 
						target="_blank" ><?php echo $multimedia->get_list_title(); ?></a></h4>
				<div class="text" ><?php echo $multimedia->get_list_desc(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "multimedia/" .$multimedia->get_multimedia_id() ); ?>" 
						target="_blank" >อ่านต่อ...</a></span>
			</div>
<?php
}
?>
		</div>

		<div class="pagination" >
			<?php echo $page['links']; ?>
			<span class="summary" >
				แสดงรายการ <?php echo $page['index1']; ?> - <?php echo $page['index2']; ?><br />
				จากทั้งหมด <?php echo $page['count']; ?> รายการ
			</span>
		</div>

<?php
include "social_media.php";
?>
	</div>
</div>