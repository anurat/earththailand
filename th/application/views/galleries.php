<div id="article" >
	<div>
		<h1>ห้องภาพ</h1>
		<br />

		<div class="items" >
<?php
foreach( $gallery_list as $gallery ) {
	$gallery_id = $gallery->get_gallery_id();
	$list_img = $gallery->get_list_img();
?>
			<div>
<?php
	if( !empty( $list_img ) ) {
?>
				<a href="<?php echo site_url( "gallery/" .$gallery_id ); ?>" target="_blank"
						><img src="<?php echo site_url( "userfiles/" .$list_img ); ?>" alt="" /></a>
<?php
	}
?>
				<h4><a href="<?php echo site_url( "gallery/" .$gallery_id ); ?>" 
						target="_blank" ><?php echo $gallery->get_list_title(); ?></a></h4>
				<div class="text" ><?php echo $gallery->get_list_desc(); ?></div>
				<span class="readmore" ><a href="<?php echo site_url( "gallery/" .$gallery_id ); ?>" 
						target="_blank" >อ่านต่อ...</a></span>
			</div>
<?php
}
?>
		</div>

		<div class="pagination" >
			<?php echo $page['links']; ?>
			<span class="summary" >
				แสดงรายการ <?php echo $page['index1']; ?> - <?php echo $page['index2']; ?><br />
				จากทั้งหมด <?php echo $page['count']; ?> รายการ
			</span>
		</div>
		
<?php
include "social_media.php";
?>
	</div>
</div>