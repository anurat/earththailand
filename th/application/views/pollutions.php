<div id="article" >
	<div>
		<h1>แผนที่มลพิษ</h1>
		
		<script type="text/javascript" src="<?php echo site_url( "js/mapsvg/js/raphael.js" ); ?>" ></script>
		<script type="text/javascript" src="<?php echo site_url( "js/mapsvg/js/jquery.mousewheel.js" ); ?>" ></script>
		<script type="text/javascript" src="<?php echo site_url( "js/mapsvg/js/mapsvg.min.js" ); ?>" ></script>
		
		<div id="thai_map" style="width: 100%;" ></div>
		
		<script type="text/javascript">
		$( function() {
			var currentLocation = [0,0];

			$("#thai_map").mapSvg({
				source: "<?php echo site_url( "images/thai_map.svg" ); ?>",
				
				loadingText : 'กำลังเปิดแผนที่...',         // Text showing while map is loading


				width:         1050,                    // width of the map container
				height:        800,                     // height of the map container
				
				//viewBox:       [0,0,800,800],       // viewing window (crop) - [x,y, width, height] - use it for  if you want to show portion of map as initial position.
														// Please note that you have to use same ratio as width / height.


				zoom: true,                             // Enable zooming (by mouswheel)
				zoomLimit: [0,7],                       // Set zoom lower / upper limits (number or zoom "steps", "0" is inital scale.
														// You can set [-1,5] to let a user zoom 1 step out from 1:1 scale.
				zoomDelta: 1.5,                         // Defines how much a map will zoom in with each "zoom step". Set values from 1 and up.
                                        // Lower values = more smooth zooming.
                                        // If you set zoomDelata = 2 then map will zoom x2 with each turn of mousewheel.
				zoomButtons: {'show': true,             // true / false = show or hide zoom buttons (dafault is "true", also buttons can be visible only if zoom is turned ON)
							  'location': 'left'},      // 'left' / 'right'
							  
				pan: true,                              // Enable panning (clik & hold left mouse button, then scroll)
				panLimit: true,                         // Enable / disable panning outside of map's boundaries
				panBackground: false,                   // Enable / disable panning of background image together with map
				
				responsive: true,                        // Set fluid size, automatically adjusting to small screens
														// Don't forget to set width and/or height, because these will be used as limiting 'max-width' and 'max-height'.

				tooltipsMode: 'names',                 // If you want to show tooltips, define this parameter.
														// There're 3 variants:
														// 1) false     (default - don't show tooltips)
														// 2) 'names'   (show country names)
														// 3) 'custom'  (show custom text defined in each region's parameters as shown below)
														// 4) 'combined'  (show custom tip, if no custom tip is set then show name)

				onClick: function(event,map){
					if( this.mapsvg_type == 'mark' ) {
<?php
foreach( $pollution_list as $pollution ) { 
	$title = $pollution->get_list_title();
	$desc = str_replace( array( "\r", "\n" ), "", $pollution->get_list_desc() );
	$x = $pollution->get_x();
	$y = $pollution->get_y();
?>
						if( $(this.node).attr("x") == <?php echo ( empty( $x ) )? '0': $x; ?> &&
								$(this.node).attr("y") == <?php echo ( empty( $y ) )? '0': $y; ?> ) {
							
							var title = '<?php echo $title; ?>';
							var desc = '<?php echo $desc; ?>';
							var readmore = '<a class="readmore" href="<?php echo site_url( "pollution/" .$pollution->get_pollution_id() ); ?>" target="_blank" >อ่านต่อ...</a>';
							
							map.showPopover( event, "<b>" +title +"</b><br />" +desc +readmore );
						}
<?php
}
?>
					}
				},
				marks: [
<?php
foreach( $pollution_list as $pollution ) { 
?>
					{ xy: [<?php echo $pollution->get_x(); ?>, <?php echo $pollution->get_y(); ?>], 
					   tooltip: '<?php echo $pollution->get_title(); ?>',
					   attrs: {
							src:  '<?php echo site_url( "userfiles/" .$pollution->get_mark_img() ); ?>',
						}
					},
<?php
}
?>
				],
			});
		});
		</script>
		
<?php
include "social_media.php";
?>
	</div>
</div>