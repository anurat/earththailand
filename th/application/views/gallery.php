<div id="article" >
	<div style="overflow: auto;" >
		<h1><?php echo $gallery->get_title(); ?></h1>

<?php
$video_code = $gallery->get_video_code();
if( !empty( $video_code ) ) {
?>
		<div class="multimedia" >
			<?php echo $video_code; ?>
		</div>
<?php
}
else {
?>
		<script src="<?php echo site_url( "/js/galleria/galleria-1.4.2.min.js" ); ?>" ></script>
		<script>
			Galleria.loadTheme('<?php echo site_url( "/js/galleria/themes/azur/galleria.azur.min.js" ); ?>');
			Galleria.run('.galleria', {
				autoplay: 10000,
				imageCrop: false,
				thumbCrop: false,
				transition: 'slide'
			});
		</script>
		<div class="galleria" id="galleria" >
<?php
	$gallery_id = $gallery->get_gallery_id();
	foreach( $images as $image ) {
?>
			<a href="<?php echo site_url( "/userfiles/Gallery/{$gallery_id}/" .$image->get_path() ); ?>"
					><img src="<?php echo site_url( "/userfiles/_thumbs/Gallery/{$gallery_id}/" .$image->get_path() ); ?>"
							data-title="<?php echo $image->get_name(); ?>"
							data-description="<?php echo $image->get_description(); ?>"
							alt="" /></a>
<?php
	}
?>
		</div>
<?php
}
?>

		<?php echo $gallery->get_description(); ?>
		<div class="end_desc" >&nbsp;</div>

<?php
$attachment = $gallery->get_attachment();
if( !empty( $attachment ) ) {
?>
			<?php echo $attachment; ?>
			<div class="end_desc" >&nbsp;</div>
<?php
}
?>

		<?php include "social_media.php"; ?>
	</div>
</div>
