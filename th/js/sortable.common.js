// common functions for sortable list
$(function () {
    
    // init sortable list
	$("#sortable").sortable({
		placeholder: "ui-state-highlight",
		axis: "y",
		cursor: "pointer",
		delay: "150",
		items: "li:not(.ui-state-disabled)",
		update: function( event, ui ) {

			var a_table = new Array();
			var a_order = new Array();
			$("#sortable li").each( function(i) {
				a_table.push(  $(this).data('table-id') );
				a_order.push( $(this).data('order') );
			});
			
			$.ajax({
				data: { a_table: a_table, a_order: a_order },
				type: 'POST',
                url: site_url +'ajax/sortable/' +table
			})
			.always( function() {
				$('#message').html( 'Reordering complete' )
						.show()
						.delay( 5000 )
						.fadeOut();
			});
		}
	});
    $("#sortable").disableSelection();

    // init items
	$("input[name^=check_]").button();
	$("div.radio").buttonset();
    $("select").selectmenu();
    $("#sortable li.ui-state-disabled").each( function() {
        lock_li( $(this) );
    });

    // add new item
	$('a#add_button').on( 'click', function(e) {
		e.preventDefault();
		location.href = site_url +segment1 +'/' +segment2 +'/' +'add_' +table;;
	});
	
    // set homepage status
	$('input[name^="check_homepage_"]').on( 'click', function() {
		var $li = $(this).closest('li');
        var table_id = $li.data('table-id');
		var status = ( $(this).prop("checked") )? 'on': 'off';
		
		$.ajax({
			data: { table_id: table_id, status: status },
			type: 'POST',
			url: site_url +"ajax/homepage/" +table
		})
		.always( function() {
			$('#message').html( 'Homepage status updated.' )
					.show()
					.delay( 5000 )
					.fadeOut();
		});
	});
    
    // set lock status
	$('input[name^="check_lock_"]').on( 'click', function() {
		var $li = $(this).closest('li');
		var table_id = $li.data('table-id');
		var status = ( $(this).prop("checked") )? 'on': 'off';
		
		$.ajax({
			data: { table_id: table_id, status: status },
			type: 'POST',
			url: site_url +"ajax/lock/" +table
		})
		.always( function() {
			$('#message').html( 'Lock status updated.' )
					.show()
					.delay( 5000 )
					.fadeOut();
			
			( status == 'on' )? lock_li( $li ): unlock_li( $li );
		});
	});
    
    // set enable status
	$("input[type=radio]").on( 'click', function() {
        var $li = $(this).closest('li');
		var table_id = $li.data('table-id');
		var status = $(this).data('status');
		
		$.ajax({
			data: { table_id: table_id, status: status },
			type: 'POST',
			url: site_url +"ajax/enable/" +table
		})
		.always( function() {
			$('#message').html( 'Enable status updated.' )
					.show()
					.delay( 5000 )
					.fadeOut();
		});
	});
	
    // run single item operations
	$('select[name=op]').on( 'selectmenuchange', function() {
		var table_id = $(this).closest('li').data('table-id');
		if( $(this).val() == 'move' ) {
			$("#m_table_id").val( table_id );
			move_dialog.dialog( "open" );
		}
		else if( $(this).val() == 'delete' ) {
			$("#d_table_id").val( table_id );
			delete_dialog.dialog( "open" );
		}
		else if( $(this).val() == 'manage' ) {
            location.href = site_url +segment1 +'/' +segment2 +'/manage_' +table +'/' +table_id
		}
	});
    
    // all ops
    $('div.all_op a:nth-child(1)').on( 'click', function(e) {
        e.preventDefault();
        $('input[name^=op]').prop( 'checked', true );
    });
    $('div.all_op a:nth-child(2)').on( 'click', function(e) {
        e.preventDefault();
        $('input[name^=op]').prop( 'checked', false );
    });
    
    $('select[name=all_op]').on( 'selectmenuchange', function() {
		if( $(this).val() == 'move' ) {
            
            var mm_table_id = new Array();
            $('input[name="op[]"]:checked').each( function() {
                mm_table_id.push( $(this).val() );
            });
            $("#mm_table_id").val( mm_table_id );
			move_all_dialog.dialog( "open" );
		}
    });
	
    // move dialog
	var move_dialog = $("#move_dialog_form").dialog({
		autoOpen: false,
		width: 'auto',
		height: 'auto',
		modal: true,
		buttons: {
			Move: function() {
			
				$.ajax({
					data: { 
						table_id: $("#m_table_id").val(),
						new_pos: $("#m_new_pos").val(), 
					},
					type: 'POST',
                    url: site_url +"ajax/move/" +table
				})
				.always( function( data ) {
					location.reload();
				});
				move_dialog.dialog( "close" );
			},
			Cancel: function() {
				move_dialog.dialog( "close" );
			}
		},
	});
    
    // move all dialog
	var move_all_dialog = $("#move_all_dialog_form").dialog({
		autoOpen: false,
		width: 'auto',
		height: 'auto',
		modal: true,
		buttons: {
			Move: function() {
			
				$.ajax({
					data: { 
						mm_table_id: $("#mm_table_id").val(),
						new_pos: $("#mm_new_pos").val(), 
					},
					type: 'POST',
                    url: site_url +"ajax/move_all/" +table
				})
				.always( function( data ) {
                    $('input[name^=op]').prop( 'checked', false );
                    $('select').selectmenu( 'option', 'disabled', false );
					location.reload();
				});
				move_all_dialog.dialog( "close" );
			},
			Cancel: function() {
				move_all_dialog.dialog( "close" );
			}
		},
	});
    
    // delete dialog
    var delete_dialog = $("#delete_dialog_form").dialog({
        autoOpen: false,
		width: 'auto',
		modal: true,
		buttons: {
			Yes: function() {
			
				$.ajax({
					type: 'GET',
                    url: site_url +segment1 +'/' +segment2 +'/delete_' +table 
                        +'/' +$("#d_table_id").val()
				})
				.always( function( data ) {
					location.reload();
				});
				delete_dialog.dialog( "close" );
			},
			No: function() {
				delete_dialog.dialog( "close" );
			}
		},
   });

    // pagination: no. of items per page
	$("select[name=per_page]").on( 'selectmenuchange', function() {
		var url = site_url +segment1 +'/' +segment2 +'/' +segment3 +'/0/' +$(this).val();
		location.href = url;
	});
});

// prevent click on the link
function disable_click() {
	return false;
}

// lock a sortable item
function lock_li( $li ) {
    $li.addClass( 'ui-state-disabled' );
    $li.find('input[name^=check_homepage]').button( 'option', 'disabled', true );
    $li.find('select').selectmenu( 'option', 'disabled', true );
    $li.find("div.radio").buttonset( 'option', 'disabled', true );

    $li.on('click', 'a', disable_click);
}

// unlock a sortable item
function unlock_li( $li ) {
    $li.removeClass( 'ui-state-disabled' );
    $li.find('input[name^=check_homepage]').button( 'option', 'disabled', false );
    $li.find('select').selectmenu( 'option', 'disabled', false );
    $li.find("div.radio").buttonset( 'option', 'disabled', false );

    $li.off('click', 'a', disable_click);
}
