var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('esass', function() {
	return gulp.src('en/css/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('en/css'));
});

gulp.task('tsass', function() {
	return gulp.src('th/css/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('th/css'));
});

gulp.task('watch', function(){
	//gulp.watch('{en,th}/css/*.scss', ['esass','tsass']);
	gulp.watch('en/css/*.scss', ['esass']);
	gulp.watch('th/css/*.scss', ['tsass']);
	// Other watchers
});

